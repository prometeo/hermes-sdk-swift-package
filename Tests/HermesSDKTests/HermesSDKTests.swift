    import XCTest
    @testable import HermesSDK

    final class HermesSDKTests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            XCTAssertEqual(HermesSDK().text, "Hello, World!")
        }
    }
