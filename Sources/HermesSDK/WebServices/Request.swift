//
//  Taiger.swift
//  ExtractSDK
//
//  Created by Juan Luis Jimenez Garcia on 20/03/2020.
//  Copyright © 2020 B1 apps. All rights reserved.
//

import Foundation

class Request : NSObject{
    
    private let endpoint : String!
    private let method : String!
    
    init(endpoint: String, method: String) {
        self.endpoint = endpoint
        self.method = method
    }
    
    func send(headers: [String : String]?, body: Data?, completion: @escaping (_ result: String, _ statusCode: Int, _ error: Error?) -> Void){
                
        guard let urlEnconded = self.endpoint.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            completion("", -1000, nil)
            return
        }
        
        guard let url = URL(string: urlEnconded) else{
            completion("", -1000, nil)
            return
        }
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = self.method
        request.timeoutInterval = 120
        
        if let headers = headers{
            for key in headers.keys{
                if let value = headers[key]{
                    request.addValue(value, forHTTPHeaderField: key)
                }
            }
        }
        
        if let body = body{
            request.httpBody = body
        }

        let task = session.dataTask(with: request) { (data, response, error) in
            
            var statusCode = -1000
            var result = ""
            
            if let httpResponse = response as? HTTPURLResponse {
                
                statusCode = httpResponse.statusCode
            }
            
            if let data = data{
                result = String(data: data, encoding: .utf8) ?? ""
            }
            
            completion(result, statusCode, error)
        }
        task.resume()
    }
    
    func download(headers: [String : String]?, body: Data?, completion: @escaping (_ result: Data?, _ mimeType: String?, _ statusCode: Int, _ error: Error?) -> Void){
                
        guard let url = URL(string: self.endpoint) else{
            completion(nil, nil, -1000, nil)
            return
        }
        
        let session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        
        var request = URLRequest(url: url)
        request.httpMethod = self.method
        request.timeoutInterval = 120
        
        if let headers = headers{
            for key in headers.keys{
                if let value = headers[key]{
                    request.addValue(value, forHTTPHeaderField: key)
                }
            }
        }
        
        if let body = body{
            request.httpBody = body
        }

        let task = session.dataTask(with: request) { (data, response, error) in
            
            var statusCode = -1000
            
            if let httpResponse = response as? HTTPURLResponse {
                
                statusCode = httpResponse.statusCode
            }
            
            completion(data, response?.mimeType, statusCode, error)
        }
        
        task.resume()
    }
}

extension Request : URLSessionDelegate {
    

    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        let urlCredential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
        
        completionHandler(.useCredential, urlCredential)
    }
}
