//
//  Formateador.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 29/11/2019.
//  Copyright © 2019 B1 apps. All rights reserved.
//

import UIKit

class Formatter {
    

    static func removeHtml(string: String) -> String{
        
        var result = ""
        
        let aux = string.replacingOccurrences(of: "&#039;", with: "'").replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        
        result = aux.replacingOccurrences(of: "&aacute;", with: "á")
        result = result.replacingOccurrences(of: "&eacute;", with: "é")
        result = result.replacingOccurrences(of: "&iacute;", with: "í")
        result = result.replacingOccurrences(of: "&oacute;", with: "ó")
        result = result.replacingOccurrences(of: "&uacute;", with: "ú")
        
        result = result.replacingOccurrences(of: "&Aacute;", with: "Á")
        result = result.replacingOccurrences(of: "&Eacute;", with: "É")
        result = result.replacingOccurrences(of: "&Iacute;", with: "Í")
        result = result.replacingOccurrences(of: "&Oacute;", with: "Ó")
        result = result.replacingOccurrences(of: "&Uacute;", with: "Ú")
        
        result = result.replacingOccurrences(of: "&ntilde;", with: "ñ")
        result = result.replacingOccurrences(of: "&Ntilde;", with: "Ñ")
        result = result.replacingOccurrences(of: "&amp;", with: "&")
        result = result.replacingOccurrences(of: "&nbsp;", with: " ")
        
        result = result.replacingOccurrences(of: "&lt;", with: "<")
        result = result.replacingOccurrences(of: "&gt;", with: ">")
        result = result.replacingOccurrences(of: "&quot;", with: "\"")
        
        result = result.replacingOccurrences(of: "&[^;]+;", with: "", options: .regularExpression, range: nil)
        
        return result
    }
    
    static func convertHtml(string: String, font: UIFont, color: UIColor) -> NSAttributedString? {
        
    
        let hexColor = color.toHex()
        let boldFont = font.bold()
        
        let screenSize = UIScreen.main.bounds
        
        let screenWidth = screenSize.width - 40
        
        let stringCSS = "<!DOCTYPE html><html><head> <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/icon?family=Material+Icons\"><style>body{font-family: '\(font.fontName)'; font-size:\(font.pointSize)px; color: \(hexColor);} strong{font-family: '\(boldFont.fontName)'; font-size:\(boldFont.pointSize)px; color: \(hexColor);} img{ max-width: \(screenWidth) !important; width: auto !important; height: auto !important;} </style></head><body>\(string)</body></html>"
        
        guard let data = stringCSS.data(using: .utf8) else {
            return nil
        }
        
        do {
            
            let aux = try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
                        
            return aux
        }
        catch {
            print(error)
            return nil
        }
        
    }
    
    static func stringToDate(string: String, format: String) -> Date?{
    
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: LOCALE)
        dateFormatter.dateFormat = format
        
        let date = dateFormatter.date(from: string)
        
        return date
    }
    
    static func dateToString(date: Date?, format: String) -> String{
        
        guard let date = date else {return "-"}
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: LOCALE)
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: date)
        
    }
    
    static func validateEmail(email: String) -> Bool{
        
        let emailRegEx = "(?:[a-zA-Z0-9!#$%\\&‘*+/=?\\^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}" +
                "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
                "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-" +
                "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5" +
                "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
                "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
                "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
            
            let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        
            return emailTest.evaluate(with: email)
    }
    
    static func randomString() -> String{
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<10).map{ _ in letters.randomElement()! })
    }
}


