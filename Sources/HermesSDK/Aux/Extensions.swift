//
//  Aux.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 24/08/2020.
//  Copyright © 2020 B1 apps. All rights reserved.
//

import UIKit

extension Dictionary{
    
    func toJson() -> String{
        var result = ""
        
        do{
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: [.prettyPrinted])
            
            if let json = String(data: jsonData, encoding: .utf8){
                result = json
            }
        }
        catch let error{
            print(error.localizedDescription)
        }
        
        return result
    }
}

extension UIFont {
    private func withTraits(traits:UIFontDescriptor.SymbolicTraits) -> UIFont {
        let descriptor = fontDescriptor.withSymbolicTraits(traits)
        return UIFont(descriptor: descriptor!, size: 0) //size 0 means keep the size as it is
    }

    func bold() -> UIFont {
        return withTraits(traits: .traitBold)
    }

    func italic() -> UIFont {
        return withTraits(traits: .traitItalic)
    }
}

extension UIColor {

    func toHex() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return String(format:"#%06x", rgb)
    }
}

extension NSMutableAttributedString {
    
    var fontSize:CGFloat { return 17 }
    var boldFont:UIFont { return UIFont.boldSystemFont(ofSize: fontSize) }
    var normalFont:UIFont { return UIFont.systemFont(ofSize: fontSize)}

    func bold(_ value:String) -> NSMutableAttributedString {

        let attributes:[NSAttributedString.Key : Any] = [
            .font : boldFont
        ]

        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }

    func normal(_ value:String) -> NSMutableAttributedString {

        let attributes:[NSAttributedString.Key : Any] = [
            .font : normalFont,
        ]

        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func orangeHighlight(_ value:String) -> NSMutableAttributedString {

        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.orange
        ]

        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }

    func blackHighlight(_ value:String) -> NSMutableAttributedString {

        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.black

        ]

        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }

    func underlined(_ value:String) -> NSMutableAttributedString {

        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .underlineStyle : NSUnderlineStyle.single.rawValue

        ]

        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
}

extension UIImage {
    
    func compress() -> Data?{
        
        var imageData = self.jpegData(compressionQuality: 1.0)
        
        var quality = 0.9
        
        while imageData != nil && imageData!.count > 500000 && quality >= 0.0 {
            imageData = self.jpegData(compressionQuality: CGFloat(quality))
            
            quality -= 0.1
        }
                
        return imageData
    }
}

extension String {
    var unescaped: String {
        let result = self.replacingOccurrences(of: "\\", with: "")
        
        return result
    }
    
    public func getAcronyms() -> String
    {
        let components = self.components(separatedBy: " ")
        
        var words = [String]()
        
        for component in components{
            if component.count > 0{
                words.append(component)
            }
        }
        
        if words.count > 0{
            let acronyms = words.map({ String($0.first!) }).joined(separator: "").prefix(3)
            return String(acronyms)
        }
        
        return ""
    }
}

extension Date {
    var startOfDay: Date? {
        return Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: self)
    }
    
    var endOfDay : Date? {
        return Calendar.current.date(bySettingHour: 23, minute: 59, second: 0, of: self)
    }
}
