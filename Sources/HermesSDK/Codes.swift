//
//  Codes.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 30/10/2020.
//

import Foundation

///Response code
public enum ResponseCode : Int {
    case success = 200
    case created = 201
    case noDataAvailable = 204
    case badRequest = 400
    case unauthorized = 401
    case forbidden = 403
    case notFound = 404
    case internalError = 500
    case notImplemented = 501
    case badGateway = 502
    case serviceUnavailable = 503
    case gateWayTimeout = 504
    case unknown = -1000
}

public enum LoginResult : Int {
    case correct = 0
    case incorrect = 1
    case needsValidation = 2
    case error = 3
    
}

public enum RequestResult : Int {
    case correct = 0
    case error = 1
}

public enum UpdateResult : Int {
    case correct = 0
    case error = 1
    case missingUserToken = 2
}

public enum DownloadResult : Int {
    case correct = 0
    case error = 1
    case missingUserToken = 2
}

public enum SendResult : Int {
    case correct = 0
    case error = 1
}

public enum SubscribeResult : Int {
    case correct = 0
    case incorrect = 1
    case alreadySubscribe = 2
    
}

public enum AssociateResult : Int {
    case correct = 0
    case incorrect = 1
    case needsValidation = 2
    
}
