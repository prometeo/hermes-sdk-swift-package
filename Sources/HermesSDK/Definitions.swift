//
//  Definitions.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 30/10/2020.
//

import Foundation

var BASE_URL = "http://hermes.dev.prometeoinnovations.com/app_dev.php/"
var APP_TOKEN = ""
var ENTITY_ID = ""
var ENTITY_TOKEN = ""
var APP_ID = ""
var LOCALE = "es"

let MENU_ENDPOINT = BASE_URL + "api/v2/app/menu"
let MENU_BADGES_ENDPOINT = BASE_URL + "api/v2/notificaciones/all"
let LOGIN_ENDPOINT = BASE_URL + "api/v4/usuario-movil/login"
let REGISTER_DEVICE_ENDPOINT = BASE_URL + "api/v4/usuario-movil/dispositivo-movil?$group"
let UNREGISTER_DEVICE_ENDPOINT = BASE_URL + "api/v2/login/dispositivo/desasociar"
let RESET_PASSWORD_ENDPOINT = BASE_URL + "api/v2/pc/resetpassword"
let CHANGE_PASSWORD_ENDPOINT = BASE_URL + "api/v4/usuario-movil/"
let REGISTER_ENDPOINT = BASE_URL + "api/v4/usuario-movil"
let CREATE_AND_ASSOCIATE_CONTENT_OWNER_ENDPOINT = BASE_URL + "api/v4/usuario-movil/propietario-contenido?$group"
//let CONTENT_OWNERS_ENDPOINT = BASE_URL + "api/v4/propietariocontenido/?$nocache"
let CONTENT_OWNERS_ENDPOINT = BASE_URL + "api/v2/pc/info"
let ALL_CONTENT_OWNERS_ENDPOINT = BASE_URL + "api/v4/propietariocontenido/?$nocache"
let ENTITIES_ENDPOINT = BASE_URL + "api/v2/pc/info/entidades"
let MESSAGES_ENDPOINT = BASE_URL + "api/v4/mensajelibre/?$nocache"
let CHANGE_MESSAGE_STATE_ENDPOINT = BASE_URL + "api/v4/mensajelibre/"
let EVENTS_ENDPOINT = BASE_URL + "api/v4/evento/?$nocache"
let CREATE_EVENT_ENDPOINT = BASE_URL + "api/v4/evento"
let SUBSCRIBE_TO_EVENT_ENDPOINT = BASE_URL + "api/v4/evento/$0?$group"
let NEWS_CATEGORIES_ENDPOINT = BASE_URL + "api/v2/noticias/getcategorias"
let NEWS_ENDPOINT = BASE_URL + "api/v4/noticia"
let CREATE_NEWS_ENDPOINT = BASE_URL + "api/v4/noticia"
let FILES_ENDPOINT = BASE_URL + "api/v2/ficheros/getficheros"
let GET_FILE_ENDPOINT = BASE_URL + "api/v2/ficheros/getfichero"
let INCIDENCES_ENDPOINT = BASE_URL + "api/v4/incidencia/?$nocache"
//let INCIDENCES_ENDPOINT = BASE_URL + "api/v2/incidencias/getincidencias"
let INCIDENCES_PROFILES_ENDPOINT = BASE_URL + "api/v2/incidencias/getperfiles"
//let CREATE_INCIDENCE_ENDPOINT = BASE_URL + "api/v2/incidencias/add"
let CREATE_INCIDENCE_ENDPOINT = BASE_URL + "api/v4/incidencia?$group"
let APPOINTMENTS_ENDPOINT = BASE_URL + "api/v4/cita/?$nocache"
let CHANGE_APPOINTMENT_STATE_ENDPOINT = BASE_URL + "app_dev.php/api/v4/cita/"
let CHANGE_APPOINTMENT_RESPONSE_ENDPOINT = BASE_URL + "app_dev.php/api/v4/cita/"
let CHANGE_APPOINTMENT_REMINDER_ENDPOINT = BASE_URL + "api/v2/cita/$0/aviso/$1"
let DIRECTORY_ENDPOINT = BASE_URL + "api/v2/directorio/getdirectorio"
let DIRECTORY_CATEGORIES_ENDPOINT = BASE_URL + "api/v4/categoriapoi/?$nocache"
let FORMS_ENDPOINT = BASE_URL + "api/v4/formulario/?$nocache"
let FORM_ENDPOINT = BASE_URL + "api/v4/formulario/"
let FORM_RESPONSES_ENDPOINT = BASE_URL + "api/v4/respuestas-formulario"
let FORM_RESPONSES_RECEIVED_ENDPOINT = BASE_URL + "api/v4/respuestas-formulario-asignadas"
let FORM_RESPONSES_RECEIVED_PENDING_ENDPOINT = BASE_URL + "api/v4/respuestas-formulario-aprobar"
let SEND_FORM_ENDPOINT = BASE_URL + "api/v2/formularios/store/"
let CHANGE_FORM_RESPONSE_ENDPOINT = BASE_URL + "api/v4/respuestas-formulario/$0?$group"
let WEATHER_ENDPOINT = "https://opendata.aemet.es/opendata/api/prediccion/especifica/municipio/diaria/{$0}?api_key=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJkc2FudGFtYXJpYUBiZW9uZWFwcHMuY29tIiwianRpIjoiMTM4ZDUxNTMtNjkyNC00MmNkLTlhOGUtYmM0ODYxMWM0OTU3IiwiaXNzIjoiQUVNRVQiLCJpYXQiOjE1NjIxNDY2NTUsInVzZXJJZCI6IjEzOGQ1MTUzLTY5MjQtNDJjZC05YThlLWJjNDg2MTFjNDk1NyIsInJvbGUiOiIifQ.XGGfw0p6nCGQ2kPIEFaYZhikHQ4IFNz5pyThjnkvKtA"
let ASSOCIATE_CONTENT_OWNER_ENDPOINT = BASE_URL + "api/v2/pc/$0/asociar"
let VALIDATE_CONTENT_OWNER_ENDPOINT = BASE_URL + "api/v2/pc/$0/validarws"
let DEASSOCIATE_CONTENT_OWNER_ENDPOINT = BASE_URL + "app_dev.php/api/v2/pc/$0/desasociar"
let ASK_SMS_CODE_ENDPOINT = BASE_URL + "api/v4/validar/$0/sms"
let SEND_PUSH_ENDPOINT = BASE_URL + "api/v2/push/envio/select"
let CHATS_ENDPOINT = BASE_URL + "api/v4/chat"
let CREATE_CHAT_ENDPOINT = BASE_URL + "api/v4/chat"
let CHAT_MESSAGES_ENDPOINT = BASE_URL + "api/v4/mensaje?chat="
let CHAT_CONTENT_OWNERS_ENDPOINT = BASE_URL + "api/v4/propietariocontenido"
let CHAT_GROUPS_ENDPOINT = BASE_URL + "api/v4/grupo"
let CHAT_ORGANS_ENDPOINT = BASE_URL + "api/v4/organigrama"
let CHAT_ENTITY_ENDPOINT = BASE_URL + "api/v4/entidad?id="
let SEND_CHAT_MESSAGE_ENDPOINT = BASE_URL + "api/v4/mensaje"
let CMS_ENDPOINT = BASE_URL + "api/v4/cms/?$nocache"
let SPLASH_ENDPOINT = BASE_URL + "api/v4/splash/?$nocache"
let ORGANIZATION_ENDPOINT = BASE_URL + "api/v4/organigrama/?$nocache"
let ORGANIZATION_OWNERS_ENDPOINT = BASE_URL + "api/v4/propietariocontenido"
let RESOURCES_ENDPOINT = BASE_URL + "api/v4/recurso/?$nocache"
let RESOURCE_TIMETABLE_ENDPOINT = BASE_URL + "api/v4/recurso/$0/horario/?$nocache"
let RESERVE_TYPES_ENDPOINT = BASE_URL + "api/v4/tiporeserva/?$nocache"
let SEND_RESERVE_ENDPOINT = BASE_URL + "api/v4/reserva"
let RESERVES_ENDPOINT = BASE_URL + "api/v4/reserva/?$nocache"
let RESERVED_TIMES_ENDPOINT = BASE_URL + "api/v4/horario?$nocache"
let CREATE_RESERVE_TIMETABLE = BASE_URL + "api/v4/horario"
let TEXTS_ENDPOINT = BASE_URL + "api/v4/texto/?$nocache"
let CREATE_CONTENT_OWNER = BASE_URL + "api/v4/propietariocontenido/"
let SEARCH_CONTENT_OWNER_BY_DNI = BASE_URL + "api/v4/propietariocontenido/?$nocache&dni="
let SEARCH_CONTENT_OWNER_BY_TOKEN = BASE_URL + "api/v4/propietariocontenido/?$nocache&token_paciente="
let GET_CONTENT_OWNER_ENDPOINT = BASE_URL + "api/v4/propietariocontenido/?id="
let HOME_ENDPOINT = BASE_URL + "api/v4/home/?$nocache"
let DOCUMENTATION_ENDPOINT = BASE_URL + "api/v4/gestor-documento/?$nocache"
let DOWNLOAD_DOCUMENT_ENDPOINT = BASE_URL + "app_dev.php/api/v4/file/download/"
let ACTIVATE_NEWS_CATEGORY_ENDPOINT = BASE_URL + "api/v2/pc/asociarcategoria"
let DEACTIVATE_NEWS_CATEGORY_ENDPOINT = BASE_URL + "api/v2/pc/desasociarcategoria"
let ALERTS_ENDPOINT = BASE_URL + "api/v4/alerta/?$nocache"
let VACATIONS_ENDPOINT = BASE_URL + "app_dev.php/api/v4/vacacion"
let PROJECTS_ENDPOINT = BASE_URL + "api/v4/proyecto"
let TASKS_ENDPOINT = BASE_URL + "api/v4/maquinaria"
let IMPUTATIONS_ENDPOINT = BASE_URL + "api/v4/imputacion_proyecto"
let PROGRAMS_ENDPOINT = BASE_URL + "api/v4/programa"
let EXPENSES_ENDPOINT = BASE_URL + "app_dev.php/api/v4/gasto"
let OFFERS_ENDPOINT = BASE_URL + "api/v4/evento/?$nocache"
let CREATE_OFFER_ENDPOINT = BASE_URL + "app_dev.php/api/v4/evento"
let EDIT_OFFER_ENDPOINT = BASE_URL + "app_dev.php/api/v4/evento/"
let SUBSCRIBE_TO_OFFERS_ENDPOINT = BASE_URL + "api/v4/respuestas-formulario?$group"
let REQUEST_TOKEN_BY_NIF = BASE_URL + "api/v4/propietariocontenido/sendToken/"
let UPDATE_SUBSCRIPTION_TO_OFFERS_ENDPOINT = BASE_URL + "api/v4/respuestas-formulario/$1?$group"
let GET_OFFER_ENDPOINT = BASE_URL + "api/v4/evento/$0?$nocache"
let SAVE_CV_ENDPOINT = BASE_URL + "api/v4/propietariocontenido/"
let SAVE_CONTENT_OWNER_EXTRA_DATA_ENDPOINT = BASE_URL + "api/v4/propietariocontenido/"
let DISABLE_USER_ENDPOINT = BASE_URL + "api/v4/usuario-movil/$0/disable"
let DELETE_USER_ENDPOINT = BASE_URL + "api/v4/usuario-movil/"

public let ROL_DEMANDANTE_EMPLEO = "ROLE_DEMANDANTE_EMPLEO"
public let ROL_EMPRESA_EMPLEADORA = "ROLE_EMPRESA_EMPLEADORA"
