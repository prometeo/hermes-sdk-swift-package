//
//  FormModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 12/11/2020.
//

import Foundation
import RealmSwift

class FormModel: Object {
    
    @objc dynamic var id : Int = 0
    @objc dynamic var titulo : String = ""
    @objc dynamic var descripcion : String = ""
    @objc dynamic var fechaAlta : String = ""
    @objc dynamic var fechaBaja : String = ""
    
    let campos = List<FormFieldModel>()
    let destinatariosFase1 = List<FormRecipientModel>()
    let destinatariosFase2 = List<FormRecipientModel>()
    let destinatariosFase3 = List<FormRecipientModel>()
    
    let categorias = List<Int>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.titulo = info["titulo"] as? String ?? ""
        self.descripcion = info["descripcion"] as? String ?? ""
        self.fechaAlta = info["fecha_alta"] as? String ?? ""
        self.fechaBaja = info["fecha_baja"] as? String ?? ""
        
        if var jsonformulario = info["inputs"] as? String{
            
            jsonformulario = jsonformulario.removingPercentEncoding!.replacingOccurrences(of: "+", with: " ")
            
            if let camposFormulario = try? JSONSerialization.jsonObject(with: (jsonformulario.data(using: .utf8))!, options:[]) as? [String:Any]{
                
                let keys = camposFormulario.keys.sorted{Int($0) ?? 0 < Int($1) ?? 0}
                
                for key in keys{
                    
                    guard let campo = camposFormulario[key] as? [String : Any] else {continue}
                    
                    let formFieldModel = FormFieldModel(info: campo)
                    
                    if formFieldModel.tipo != "file" {
                        self.campos.append(formFieldModel)
                    }
                }
                
                
            }
        }
                
        if let destinatarios1 = info["destinatariosFase1"] as? [[String : Any]]{
            for destinatario in destinatarios1{
                self.destinatariosFase1.append(FormRecipientModel(info: destinatario))
            }
        }
        
        if let destinatarios2 = info["destinatariosFase2"] as? [[String : Any]]{
            for destinatario in destinatarios2{
                self.destinatariosFase2.append(FormRecipientModel(info: destinatario))
            }
        }
        
        if let destinatarios3 = info["destinatariosFase3"] as? [[String : Any]]{
            for destinatario in destinatarios3{
                self.destinatariosFase3.append(FormRecipientModel(info: destinatario))
            }
        }
        
        if let categorias = info["categorias"] as? [[String : Any]]{
            for categoria in categorias {
                if let id = categoria["id"] as? Int{
                    self.categorias.append(id)
                }
            }
        }
 
    }
    
    
}
