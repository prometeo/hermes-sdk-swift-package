//
//  FormFieldModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 12/11/2020.
//

import Foundation
import RealmSwift

class FormFieldModel: Object {
    
    @objc dynamic var id : String = ""
    @objc dynamic var grupo : String = ""
    @objc dynamic var nombre : String = ""
    @objc dynamic var tipo : String = ""
    @objc dynamic var requerido : String = ""
    @objc dynamic var regexp : String = ""
    @objc dynamic var valor : String = ""
 
    let opciones = List<String>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = Formatter.randomString()
        self.grupo = info["grupo"] as? String ?? ""
        self.nombre = info["nombre"] as? String ?? ""
        self.tipo = info["tipo"] as? String ?? ""
        self.requerido = info["requerido"] as? String ?? ""
        self.regexp = info["regexp"] as? String ?? ""
        self.valor = info["valor"] as? String ?? ""
        
        if self.nombre == ""{
            self.nombre = info["name"] as? String ?? ""
        }
        
        if self.tipo == "combo"{
            self.opciones.append(objectsIn: self.valor.components(separatedBy: ","))
            
            if self.opciones.count > 1{
                self.valor = ""
            }
        }
                
        if let valorBool = info["valor"] as? Bool{
            if valorBool == true{
                self.valor = "true"
            }
            else{
                self.valor = "false"
            }
        }
    }
    
    
}
