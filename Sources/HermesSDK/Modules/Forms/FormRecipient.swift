//
//  FormRecipient.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 12/11/2020.
//

import Foundation
import UIKit

public class FormRecipient : HermesObject {
    
    
    internal init(model: FormRecipientModel) {
        self.id = model.id
        self.nombre = model.nombre
        self.aprobador = model.aprobador
    }
    
    public var id : Int
    public var nombre : String
    public var aprobador : Bool
    
}
