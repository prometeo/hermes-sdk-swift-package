//
//  FormResponse.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 12/11/2020.
//

import Foundation

public protocol FormResponse : HermesObject {
    
    var id : Int { get }
    var titulo : String { get }
    var idFormulario : Int { get }
    var idPropietarioContenido : Int { get }
    var nombrePropietarioContenido : String { get }
    var imagenPropietarioContenido : String { get }
    var nombreEntidad : String { get }
    var imagenEntidad : String { get }
    var fecha : String { get }
    func fecha(formato: String) -> String
    var idAprobador1 : Int { get }
    var nombreAprobador1 : String { get }
    var estadoAprobacion1 : Int { get }
    var idAprobador2 : Int { get }
    var nombreAprobador2 : String { get }
    var estadoAprobacion2 : Int { get }
    var idAprobador3 : Int { get }
    var nombreAprobador3 : String { get }
    var estadoAprobacion3 : Int { get }
    var estadoFinal : Int { get }
    var campos : [FormField] { get }
    var leida : Bool { get }
    var observaciones : String { get }
    func marcarLeida()
    var fechaEstado1 : String { get }
    var fechaEstado2 : String { get }
    var fechaEstado3 : String { get }
    func fechaEstado1(formato: String) -> String
    func fechaEstado2(formato: String) -> String
    func fechaEstado3(formato: String) -> String
    var comentario1 : String { get }
    var comentario2 : String { get }
    var comentario3 : String { get }
    var pendiente : Bool { get }
}
