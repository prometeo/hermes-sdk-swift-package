//
//  FormField.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 12/11/2020.
//

import Foundation
import UIKit

public class FormField : HermesObject {
        
    internal init(model: FormFieldModel) {
        self.grupo = model.grupo
        self.nombre = model.nombre
        self.tipo = FormFieldType(rawValue: model.tipo) ?? .text
        self.regexp = model.regexp
        self.valor = model.valor
        self.opciones = Array(model.opciones)
        
        if model.requerido == "true"{
            self.requerido = true
        }
        else{
            self.requerido = false
        }
    }
    
    public var grupo : String
    public var nombre : String
    public var tipo : FormFieldType
    public var requerido : Bool
    public var regexp : String
    public var valor : String
    public var opciones : [String]

}

public enum FormFieldType : String {
    case text = "text"
    case date = "date"
    case time = "time"
    case file = "file"
    case image = "image"
    case combo = "combo"
    case checkbox = "checkbox"
    case firma = "firma"
}
