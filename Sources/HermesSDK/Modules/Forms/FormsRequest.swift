//
//  FormsRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 12/11/2020.
//

import Foundation
import UIKit

class FormsRequest{
    
    func download(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = FORMS_ENDPOINT + "?$locale=" + LOCALE
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            
            if result.contains("OBJECT_NOT_EXIST"){
                completion(results, .correct)
                
                return
            }
            
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
    
    func getForm(id: Int, appToken: String, userToken: String, completion: (@escaping([String : Any], RequestResult) -> ())){
        
        let url = FORM_ENDPOINT + String(id)
                        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [String : Any]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            

        })
        
    }
    
    func send(appToken: String, userToken: String, idForm: String, idContentOwner: String, fields: [[String : Any]], idRecipient1: String?, idRecipient2: String?, idRecipient3: String?, completion: (@escaping(RequestResult) -> ())){
        
        let url = SEND_FORM_ENDPOINT + appToken
        
        let request = Request(endpoint: url, method: "POST")
        
        var body = Data()
        
        let boundary = "------VohpleBoundary4sadfasdfeafe4w5w45435hwttre"
        let contentType = "multipart/form-data; boundary=" + boundary
        let separator = "--" + boundary + "\r\n"
        let newLine = "\r\n"
        
        let paramNameUserToken = "Content-Disposition: form-data; name=\"userToken\"\r\n\r\n"
        let paramNameIdForm = "Content-Disposition: form-data; name=\"idFormulario\"\r\n\r\n"
        let paramNameContentOwner = "Content-Disposition: form-data; name=\"idPaciente\"\r\n\r\n"
        let paramNameJson = "Content-Disposition: form-data; name=\"jsonFormulario\"\r\n\r\n"
        let paramNameRecipient1 = "Content-Disposition: form-data; name=\"id_aprobador1\"\r\n\r\n"
        let paramNameRecipient2 = "Content-Disposition: form-data; name=\"id_aprobador2\"\r\n\r\n"
        let paramNameRecipient3 = "Content-Disposition: form-data; name=\"id_aprobador3\"\r\n\r\n"
        

        body.append(separator.data(using: .utf8)!)
        body.append(paramNameUserToken.data(using: .utf8)!)
        body.append(userToken.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameIdForm.data(using: .utf8)!)
        body.append(idForm.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameContentOwner.data(using: .utf8)!)
        body.append(idContentOwner.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        if idRecipient1 != nil{
            body.append(separator.data(using: .utf8)!)
            body.append(paramNameRecipient1.data(using: .utf8)!)
            body.append(idRecipient1!.data(using: .utf8)!)
            body.append(newLine.data(using: .utf8)!)
        }
        
        if idRecipient2 != nil{
            body.append(separator.data(using: .utf8)!)
            body.append(paramNameRecipient2.data(using: .utf8)!)
            body.append(idRecipient2!.data(using: .utf8)!)
            body.append(newLine.data(using: .utf8)!)
        }
        
        if idRecipient3 != nil{
            body.append(separator.data(using: .utf8)!)
            body.append(paramNameRecipient3.data(using: .utf8)!)
            body.append(idRecipient3!.data(using: .utf8)!)
            body.append(newLine.data(using: .utf8)!)
        }
        
        let fieldsDic = ["campos" : fields]
        
        guard let fieldsJson = try? JSONSerialization.data(withJSONObject: fieldsDic, options: .prettyPrinted) else {
            completion(.error)
            return
        }
        
        guard let fieldsString = String(data: fieldsJson, encoding: String.Encoding.utf8) else {
            completion(.error)
            return
        }
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameJson.data(using: .utf8)!)
        body.append(fieldsString.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        var images = [[String : Any]]()
        
        for field in fields{
            
            if let type = field["tipo"] as? String, let fieldName = field["nombre"] as? String, let value = field["valor"] as? String{
                
                if type == "image" || type == "firma"{
                    
                    var imageInfo = [String : Any]()
                    
                    let imageName = value
                    
                    if let image = self.getImage(name: imageName){
                        imageInfo["nombreCampo"] = fieldName
                        imageInfo["nombreImagen"] = imageName
                        imageInfo["imagen"] = image
                        
                        images.append(imageInfo)
                    }
                    else{
                        print("ERROR READING IMAGE")
                    }
                }
            }
        }
        
        let typeImage = "Content-Type:image/jpeg\r\n\r\n"
        
        if images.count > 0 {
            
            for i in 0..<images.count{
                
                let imageInfo = images[i]
                
                if let fieldName = imageInfo["nombreCampo"] as? String, let fileName = imageInfo["nombreImagen"] as? String, let image = imageInfo["imagen"] as? UIImage{
                    
                    guard let imageData = image.compress() else {continue}
                                        
                    body.append(separator.data(using: .utf8)!)
                    
                    let imageField = "Content-Disposition: form-data; name=\"" + fieldName + "\"; filename=\"" + fileName + "\"\r\n"
                                
                    body.append(imageField.data(using: .utf8)!)
                    body.append(typeImage.data(using: .utf8)!)
                    body.append(imageData)
                    body.append(newLine.data(using: .utf8)!)
                }
                
            }
        }
        
        let end = "--" + boundary + "--\r\n"
                
        body.append(end.data(using: .utf8)!)
        
        let headers : [String : String] = ["Content-Type" : contentType]
        
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            completion(.correct)
            return
        })
    }
        
    private func getImage(name: String) -> UIImage?{
        
        let documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        
        let loadPath = documentsPath.appendingPathComponent(name)
        
        do{
            let data = try Data.init(contentsOf: loadPath)
            
            let image = UIImage(data: data)
            
            return image
        }
        catch let error as NSError {
            print(error.localizedDescription)
            
            return nil
        }
    }
}
