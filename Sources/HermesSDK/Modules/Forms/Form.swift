//
//  Form.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 12/11/2020.
//

import Foundation
import UIKit

public class Form : HermesObject {
        
    internal init(model: FormModel) {
        self.id = model.id
        self.titulo = model.titulo
        self.descripcion = model.descripcion
        self.campos = model.campos.map{FormField(model: $0)}
        self.destinatariosFase1 = model.destinatariosFase1.map{FormRecipient(model: $0)}
        self.destinatariosFase2 = model.destinatariosFase2.map{FormRecipient(model: $0)}
        self.destinatariosFase3 = model.destinatariosFase3.map{FormRecipient(model: $0)}
        self.categorias = Array(model.categorias)
        
        if let fechaAltaDate = Formatter.stringToDate(string: model.fechaAlta, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            self.fechaAlta = fechaAltaDate
        }
        else{
            self.fechaAlta = Date()
        }
        
        if let fechaBajaDate = Formatter.stringToDate(string: model.fechaBaja, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            self.fechaBaja = fechaBajaDate
        }
        else{
            self.fechaBaja = Date()
        }
    }
    
    public var id : Int
    public var titulo : String
    public var descripcion : String
    public var fechaAlta : Date
    public var fechaBaja : Date
    public var campos : [FormField]
    public var destinatariosFase1 : [FormRecipient]
    public var destinatariosFase2 : [FormRecipient]
    public var destinatariosFase3 : [FormRecipient]
    public var categorias : [Int]
    
    public var descripcionHtml : NSAttributedString {
        return Formatter.convertHtml(string:self.descripcion, font: UIFont(name: "Helvetica Neue", size: 18.0)!, color: UIColor.darkGray) ?? NSAttributedString(string: "")
    }
    
}
