//
//  HermesForms.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 12/11/2020.
//

import Foundation

public class HermesForms: HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        let forms = DataBaseManager.shared.getObjects(type: FormModel.self).map{Form(model: $0)}
        
        return forms
    }
    
    public func getContentOwners() -> [ContentOwner] {
        
        let contentOwners = DataBaseManager.shared.getObjects(type: ContentOwnerModel.self).map{ContentOwner(model: $0)}
        
        return contentOwners
    }
    
    private var pendingForms = 0
    
    public func update(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = FormsRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    self.pendingForms = results.count
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: FormModel.self)
                        
                        var forms = [FormModel]()
                        
                        let currentDate = Date()
                        
                        for element in results{
                            
                            let form = FormModel(info: element)
                                                            
                            if let startDate = Formatter.stringToDate(string: form.fechaAlta, format: "yyyy-MM-dd'T'HH:mm:sszzzz"), let endDate = Formatter.stringToDate(string: form.fechaBaja, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){

                                if currentDate >= startDate && currentDate <= endDate{
                                    forms.append(form)
                                }
                                
                            }
                            else if let endDate = Formatter.stringToDate(string: form.fechaBaja, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){

                                if currentDate <= endDate{
                                    forms.append(form)
                                }
                                
                            }
                            else{
                                forms.append(form)
                            }
                        }
                        
                        DataBaseManager.shared.save(objects: forms)
                        
                        if completion != nil{
                            completion!(.correct)
                        }
                        

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
            
        })
 
    
    }
    
    public func send(form: Form, contentOwner: String, fields: [[String : Any]], idRecipient1: String?, idRecipient2: String?, idRecipient3: String?, completion: ((SendResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.error)
                }
                
            }
            return
        }
                
        let request = FormsRequest()
        
        request.send(appToken: self.appToken, userToken: userToken, idForm: String(form.id), idContentOwner: contentOwner, fields: fields, idRecipient1: idRecipient1, idRecipient2: idRecipient2, idRecipient3: idRecipient3,  completion: {(requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    let sentFormsModule = HermesSentForms()
                    
                    sentFormsModule.update(completion: {(updateResult) -> () in
                        
                        DispatchQueue.main.async {
                            
                            if completion != nil{
                                completion!(.correct)
                            }

                        }
                        
                    })

                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
}
