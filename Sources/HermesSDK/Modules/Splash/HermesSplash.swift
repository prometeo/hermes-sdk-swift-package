//
//  HermesSplash.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 6/4/21.
//

import Foundation

public class HermesSplash: HermesModule {
    
    private var appToken : String!
    private var entityToken : String!
    
    public init() {

        self.appToken = APP_TOKEN
        self.entityToken = ENTITY_TOKEN
        
        
    }
    
    public func get() -> [HermesObject] {
        
        let splashes = DataBaseManager.shared.getObjects(type: SplashModel.self).map{Splash(model: $0)}
        
        return splashes
    }
    
    public func update(completion: ((UpdateResult) -> ())?){
        
        let request = SplashRequest()
        
        request.download(appToken: self.appToken, entityToken: self.entityToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: SplashModel.self)
                        
                        var splashes = [SplashModel]()
                        
                        for element in results{
                            let splash = SplashModel(info: element)
                            
                            splashes.append(splash)
                        }
                        
                        DataBaseManager.shared.save(objects: splashes)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
}
