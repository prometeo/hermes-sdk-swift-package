//
//  Splash.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 6/4/21.
//

import Foundation
import UIKit

public class Splash : HermesObject {
        
    internal init(model: SplashModel) {
        self.id = model.id
        self.titulo = model.titulo
        self.imagen = model.imagen
        self.duracion = model.duracion
        
        if let fechaInicioDate = Formatter.stringToDate(string: model.fechaInicio, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            self.fechaInicio = fechaInicioDate
        }
        else{
            self.fechaInicio = Date()
        }
        
        if let fechaFinDate = Formatter.stringToDate(string: model.fechaFin, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            self.fechaFin = fechaFinDate
        }
        else{
            self.fechaFin = Date()
        }
    }
    
    public var id : Int
    public var titulo : String
    public var imagen : String
    public var duracion : Int
    public var fechaInicio : Date
    public var fechaFin : Date
    
}
