//
//  SplashModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 6/4/21.
//

import Foundation
import RealmSwift

class SplashModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var titulo : String = ""
    @objc dynamic var imagen : String = ""
    @objc dynamic var duracion : Int = 2000 //ms
    @objc dynamic var fechaInicio : String = ""
    @objc dynamic var fechaFin : String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.titulo = info["titulo"] as? String ?? ""
        self.duracion = info["duracion"] as? Int ?? 2000
        self.fechaInicio = info["fecha_inicio"] as? String ?? ""
        self.fechaFin = info["fecha_fin"] as? String ?? ""
        
        if let imagen = info["image"] as? String{
            if imagen.hasPrefix("http") == false{
                self.imagen = (BASE_URL + imagen).replacingOccurrences(of: "//", with: "/")
            }
            else{
                self.imagen = imagen
            }
        }
    }
}
