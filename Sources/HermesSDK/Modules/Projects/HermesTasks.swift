//
//  HermesTasks.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 16/2/23.
//

import Foundation

public class HermesTasks : HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    
    public init() {
        self.userToken = nil
        
        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        return DataBaseManager.shared.getObjects(type: TaskModel.self).map{Task(model: $0)}
    }
    
    public func update(completion: ((UpdateResult) -> ())?) {
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = TasksRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: TaskModel.self)
                        
                        var taskItems = [TaskModel]()
                        
                        for element in results{
                            let taskItem = TaskModel(info: element)
                            
                            taskItems.append(taskItem)
                        }
                        
                        DataBaseManager.shared.save(objects: taskItems)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
    }
    
    
}
