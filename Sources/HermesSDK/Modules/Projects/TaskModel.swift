//
//  TaskModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 16/2/23.
//

import Foundation
import RealmSwift

class TaskModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var nombre : String = ""
    @objc dynamic var descripcion : String = ""
    @objc dynamic var codigo : String = ""
    @objc dynamic var tipoId : Int = 0
    @objc dynamic var tipoNombre : String = ""
    @objc dynamic var tipoDescripcion : String = ""

    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.nombre = info["nombre"] as? String ?? ""
        self.descripcion = info["descripcion"] as? String ?? ""
        self.codigo = info["codigo"] as? String ?? ""
        
        if let tipo_maquinaria = info["tipo_maquinaria"] as? [String : Any]{
            self.tipoId = tipo_maquinaria["id"] as? Int ?? 0
            self.tipoNombre = tipo_maquinaria["nombre"] as? String ?? ""
            self.tipoDescripcion = tipo_maquinaria["descripcion"] as? String ?? ""
        }

    }

}
