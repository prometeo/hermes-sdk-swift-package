//
//  ImputationModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 16/2/23.
//

import Foundation
import RealmSwift

class ImputationModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var fechaHoraInicio : String = ""
    @objc dynamic var horaFin : String = ""
    @objc dynamic var duracion : Int = 0
    @objc dynamic var idProyecto : Int = 0
    @objc dynamic var idTarea : Int = 0
    @objc dynamic var comentarios : String = ""

    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.fechaHoraInicio = info["fecha_hora_inicio"] as? String ?? ""
        self.horaFin = info["hora_fin"] as? String ?? ""
        self.duracion = info["duracion"] as? Int ?? 0
        self.comentarios = info["comentarios"] as? String ?? ""
        
        if let proyecto = info["proyecto"] as? [String : Any]{
            self.idProyecto = proyecto["id"] as? Int ?? 0
        }
        
        if let maquinaria = info["maquinaria"] as? [String : Any]{
            self.idTarea = maquinaria["id"] as? Int ?? 0
        }

    }

}
