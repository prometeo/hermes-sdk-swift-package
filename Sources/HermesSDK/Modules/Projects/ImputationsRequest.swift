//
//  ImputationsRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 16/2/23.
//

import Foundation

class ImputationsRequest{
    
    func download(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = IMPUTATIONS_ENDPOINT + "?$locale=" + LOCALE
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
    
    func downloadByContentOwner(appToken: String, userToken: String, contentOwnerId: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = IMPUTATIONS_ENDPOINT + "?propietario=" + contentOwnerId
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            
            if result.contains("OBJECT_NOT_EXIST"){
                completion(results, .correct)
                
                return
            }
            
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
    
    func create(appToken: String, userToken: String, contentOwnerId: String, dateStartTime: String, endTime: String, duration: Int, comments: String, projectId: Int, taskId: Int, completion: (@escaping([String : Any], RequestResult) -> ())){
        
        let url = IMPUTATIONS_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "fecha_hora_inicio", value: dateStartTime))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "hora_fin", value: endTime))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "duracion", value: String(duration)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "comentarios", value: comments))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "proyecto[id]", value: String(projectId)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "maquinaria[id]", value: String(taskId)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "propietario[id]", value: contentOwnerId))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [String : Any]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
    
}
