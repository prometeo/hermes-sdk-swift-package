//
//  Imputation.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 16/2/23.
//

import Foundation

public class Imputation : HermesObject {
    
    public init() {
        self.id = 0
        self.fechaHoraInicio = ""
        self.horaFin = ""
        self.duracion = 0
        self.idProyecto = 0
        self.idTarea = 0
        self.comentarios = ""
        self.proyecto = Project()
        self.tarea = Task()
    }
    
    internal init(model: ImputationModel) {
        self.id = model.id
        self.fechaHoraInicio = model.fechaHoraInicio
        self.horaFin = model.horaFin
        self.duracion = model.duracion
        self.idProyecto = model.idProyecto
        self.idTarea = model.idTarea
        self.comentarios = model.comentarios
        self.proyecto = Project()
        self.tarea = Task()
        
        if let project = DataBaseManager.shared.getObjects(type: ProjectModel.self).filter({$0.id == model.idProyecto}).first{
            self.proyecto = Project(model: project)
        }
        
        if let task = DataBaseManager.shared.getObjects(type: TaskModel.self).filter({$0.id == model.idTarea}).first{
            self.tarea = Task(model: task)
        }

    }

    public var id : Int
    public var fechaHoraInicio : String
    public var horaFin : String
    public var duracion : Int
    public var idProyecto : Int
    public var idTarea : Int
    public var comentarios : String
    public var proyecto : Project
    public var tarea : Task

    public func fechaHoraInicio(formato: String) -> String {
        var resultado = ""
    
        if let fechaDate = Formatter.stringToDate(string: self.fechaHoraInicio, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
    
        return resultado
    }
    
    public func horaFin(formato: String) -> String {
        var resultado = ""
    
        if let fechaDate = Formatter.stringToDate(string: self.horaFin, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
    
        return resultado
    }
    

}
