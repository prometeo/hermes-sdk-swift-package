//
//  HermesImputations.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 16/2/23.
//

import Foundation

public class HermesImputations : HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    
    public init() {
        self.userToken = nil
        
        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        return DataBaseManager.shared.getObjects(type: ImputationModel.self).map{Imputation(model: $0)}
    }
    
    public func update(completion: ((UpdateResult) -> ())?) {
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = ImputationsRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: ImputationModel.self)
                        
                        var imputationItems = [ImputationModel]()
                        
                        for element in results{
                            let imputationItem = ImputationModel(info: element)
                            
                            imputationItems.append(imputationItem)
                        }
                        
                        DataBaseManager.shared.save(objects: imputationItems)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
    }
    
    public func updateByContentOwner(contentOwnerId: Int, completion: ((UpdateResult) -> ())?) {
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = ImputationsRequest()
        
        request.downloadByContentOwner(appToken: self.appToken, userToken: userToken, contentOwnerId: String(contentOwnerId), completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: ImputationModel.self)
                        
                        var imputationItems = [ImputationModel]()
                        
                        for element in results{
                            let imputationItem = ImputationModel(info: element)
                            
                            imputationItems.append(imputationItem)
                        }
                        
                        DataBaseManager.shared.save(objects: imputationItems)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
    }
    
    public func saveImputation(contentOwner: ContentOwner, date: Date, startTime: Date, endTime: Date, duration: Int, comments: String, projectId: Int, taskId: Int, completion: ((UpdateResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let dateString = Formatter.dateToString(date: date, format: "dd/MM/yyyy")
        let startTimeString = Formatter.dateToString(date: startTime, format: "HH:mm")
        
        let dateStart = Formatter.stringToDate(string: dateString + " " + startTimeString, format: "dd/MM/yyyy HH:mm")
        
        let dateStartTimeString = Formatter.dateToString(date: dateStart, format: "yyyy-MM-dd'T'HH:mm:ssZ").replacingOccurrences(of: "+", with: "%2B")
        let endTimeString = Formatter.dateToString(date: endTime, format: "yyyy-MM-dd'T'HH:mm:ssZ").replacingOccurrences(of: "+", with: "%2B")
        

        
        let request = ImputationsRequest()
        
        request.create(appToken: self.appToken, userToken: userToken, contentOwnerId: String(contentOwner.id), dateStartTime: dateStartTimeString, endTime: endTimeString, duration: duration, comments: comments, projectId: projectId, taskId: taskId, completion: {(data, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    if completion != nil{
                        completion!(.correct)
                    }
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
        
        
        
    }
    
}
