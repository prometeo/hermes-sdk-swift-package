//
//  Task.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 16/2/23.
//

import Foundation

public class Task : HermesObject {
    
    public init() {
        self.id = 0
        self.nombre = ""
        self.descripcion = ""
        self.codigo = ""
        self.tipoId = 0
        self.tipoNombre = ""
        self.tipoDescripcion = ""

    }
    
    internal init(model: TaskModel) {
        self.id = model.id
        self.nombre = model.nombre
        self.descripcion = model.descripcion
        self.codigo = model.codigo
        self.tipoId = model.tipoId
        self.tipoNombre = model.tipoNombre
        self.tipoDescripcion = model.tipoDescripcion

    }

    public var id : Int
    public var nombre : String
    public var descripcion : String
    public var codigo : String
    public var tipoId : Int
    public var tipoNombre : String
    public var tipoDescripcion : String

    

}
