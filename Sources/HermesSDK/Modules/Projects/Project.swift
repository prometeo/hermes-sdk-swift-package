//
//  Project.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 13/1/23.
//

import Foundation

public class Project : HermesObject {
    
    public init() {
        self.id = 0
        self.nombre = ""
        self.fechaInicio = ""
        self.fechaFin = ""
        self.descripcion = ""
        self.codigo = ""
        self.tipoProyectoId = 0
        self.tipoProyecto = ""
        self.cliente = ""
        self.clienteCodigo = ""
    }
    
    internal init(model: ProjectModel) {
        self.id = model.id
        self.nombre = model.nombre
        self.fechaInicio = model.fechaInicio
        self.fechaFin = model.fechaFin
        self.descripcion = model.descripcion
        self.codigo = model.codigo
        self.tipoProyectoId = model.tipoProyectoId
        self.tipoProyecto = model.tipoProyecto
        self.cliente = model.cliente
        self.clienteCodigo = model.clienteCodigo
    }

    public var id : Int
    public var nombre : String
    public var fechaInicio : String
    public var fechaFin : String
    public var descripcion : String
    public var codigo : String
    public var tipoProyectoId : Int
    public var tipoProyecto : String
    public var cliente : String
    public var clienteCodigo : String

    public func fechaInicio(formato: String) -> String {
        var resultado = ""
    
        if let fechaDate = Formatter.stringToDate(string: self.fechaInicio, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
    
        return resultado
    }
    
    public func fechaFin(formato: String) -> String {
        var resultado = ""
    
        if let fechaDate = Formatter.stringToDate(string: self.fechaFin, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
    
        return resultado
    }

}
