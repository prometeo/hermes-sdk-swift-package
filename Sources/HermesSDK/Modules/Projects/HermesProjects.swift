//
//  HermesProjects.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 13/1/23.
//

import Foundation

public class HermesProjects : HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    
    public init() {
        self.userToken = nil
        
        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        return DataBaseManager.shared.getObjects(type: ProjectModel.self).map{Project(model: $0)}
    }
    
    public func update(completion: ((UpdateResult) -> ())?) {
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = ProjectsRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: ProjectModel.self)
                        
                        var projectItems = [ProjectModel]()
                        
                        for element in results{
                            let projectItem = ProjectModel(info: element)
                            
                            projectItems.append(projectItem)
                        }
                        
                        DataBaseManager.shared.save(objects: projectItems)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
    }
    
    
}
