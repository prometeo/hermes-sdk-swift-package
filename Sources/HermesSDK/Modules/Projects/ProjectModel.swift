//
//  ProjectModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 13/1/23.
//

import Foundation
import RealmSwift

class ProjectModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var nombre : String = ""
    @objc dynamic var fechaInicio : String = ""
    @objc dynamic var fechaFin : String = ""
    @objc dynamic var descripcion : String = ""
    @objc dynamic var codigo : String = ""
    @objc dynamic var tipoProyectoId : Int = 0
    @objc dynamic var tipoProyecto : String = ""
    @objc dynamic var cliente : String = ""
    @objc dynamic var clienteCodigo : String = ""

    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.nombre = info["nombre"] as? String ?? ""
        self.fechaInicio = info["fecha_inicio"] as? String ?? ""
        self.fechaFin = info["fecha_fin"] as? String ?? ""
        self.descripcion = info["descripcion"] as? String ?? ""
        self.codigo = info["codigo"] as? String ?? ""
        
        if let tipo_proyecto = info["tipo_proyecto"] as? [String : Any]{
            self.tipoProyectoId = tipo_proyecto["id"] as? Int ?? 0
            self.tipoProyecto = tipo_proyecto["nombre"] as? String ?? ""
        }
        
        if let cliente = info["cliente"] as? [String : Any]{
            self.cliente = cliente["denominacion"] as? String ?? ""
            self.clienteCodigo = cliente["codigo"] as? String ?? ""
        }

    }
    
    
    
}


