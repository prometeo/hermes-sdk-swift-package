//
//  HermesConfigManager.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 04/11/2020.
//

import Foundation

public class HermesConfigManager {
    
    public static let shared = HermesConfigManager()
    
    public func configHermesWith(serverUrl: String, appToken: String, entityId: String, entityToken: String, appId: String, language: String = "es"){
        BASE_URL = serverUrl
        APP_TOKEN = appToken
        ENTITY_ID = entityId
        ENTITY_TOKEN = entityToken
        LOCALE = language
        APP_ID = appId
    }
    
    public func changeLanguage(language: String){
        LOCALE = language
    }
    
}
