//
//  DirectoryItemModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 12/11/2020.
//

import Foundation
import RealmSwift

class DirectoryItemModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var nombre : String = ""
    @objc dynamic var categoriPrincipal : String = ""
    @objc dynamic var categoriaSecundaria : String = ""
    @objc dynamic var codigo : String = ""
    @objc dynamic var direccion : String = ""
    @objc dynamic var correo : String = ""
    @objc dynamic var telefono : String = ""
    @objc dynamic var descripcion : String = ""
    @objc dynamic var tipo : String = ""
    @objc dynamic var tematica : String = ""
    @objc dynamic var horario : String = ""
    @objc dynamic var latitud : String = ""
    @objc dynamic var longitud : String = ""
    @objc dynamic var orden : Int  = 0
    
    let imagenes = List<String>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.nombre = info["nombre"] as? String ?? ""
        self.categoriPrincipal = info["cat_principal"] as? String ?? ""
        self.categoriaSecundaria = info["cat_secundaria"] as? String ?? ""
        self.codigo = info["codigo"] as? String ?? ""
        self.direccion = info["direccion"] as? String ?? ""
        self.correo = info["email"] as? String ?? ""
        self.telefono = info["telefono"] as? String ?? ""
        self.descripcion = info["descripcion"] as? String ?? ""
        self.tipo = info["tipo_servicio"] as? String ?? ""
        self.tematica = info["tematica"] as? String ?? ""
        self.horario = info["horario"] as? String ?? ""
        self.latitud = info["latitud"] as? String ?? ""
        self.longitud = info["longitud"] as? String ?? ""
        self.orden = info["orden"] as? Int ?? 0
        
        if let imagenesDirectorio = info["imagenes"] as? String{
            self.imagenes.append(objectsIn: imagenesDirectorio.components(separatedBy: ",").map{
                if $0.hasPrefix("http") == false{
                    return (BASE_URL + "uploads/" + $0).replacingOccurrences(of: "//", with: "/")
                }
                
                return $0
            })
            
        }
        
        if let imagenesDirectorio = info["imagenes"] as? [String]{
            self.imagenes.append(objectsIn: imagenesDirectorio.map{
                if $0.hasPrefix("http") == false{
                    return (BASE_URL + "uploads/" + $0).replacingOccurrences(of: "uploads//uploads", with: "uploads").replacingOccurrences(of: "//", with: "/")
                }
                
                return $0
            })
            
        }
    }
    
    
}
