//
//  DirectoryCategoryModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 9/12/21.
//

import Foundation
import RealmSwift

class DirectoryCategoryModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var nombre : String = ""
    @objc dynamic var alias : String = ""
    @objc dynamic var imagen : String = ""
    @objc dynamic var orden : Int  = 0
    @objc dynamic var esRuta : Bool  = false
    @objc dynamic var descripcion : String = ""
    @objc dynamic var html : String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.nombre = info["nombre"] as? String ?? ""
        self.alias = info["alias"] as? String ?? ""
        self.orden = info["orden"] as? Int ?? 0
        self.esRuta = info["is_ruta"] as? Bool ?? false
        self.descripcion = info["descripcion"] as? String ?? ""
        self.html = info["html"] as? String ?? ""
        
        if let image = info["image"] as? String{
            if image.hasPrefix("http") == false{
                self.imagen = (BASE_URL + "uploads/" + image).replacingOccurrences(of: "uploads//uploads", with: "uploads").replacingOccurrences(of: "//", with: "/")
            }
            else{
                self.imagen = image
            }
        }
    }
    
    
}
