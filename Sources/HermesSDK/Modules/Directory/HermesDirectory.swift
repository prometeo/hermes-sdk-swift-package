//
//  HermesDirectory.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 12/11/2020.
//

import Foundation

public class HermesDirectory: HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    private var filterCategory : String?
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func filterBy(_ category : String) -> HermesDirectory {
        
        self.filterCategory = category
        
        return self
    }
    
    public func get() -> [HermesObject] {
        let directoryItems = DataBaseManager.shared.getObjects(type: DirectoryItemModel.self).map{DirectoryItem(model: $0)}.sorted{$0.orden < $1.orden}
        
        if self.filterCategory != nil {
            return directoryItems.filter{$0.categoriPrincipal == self.filterCategory! || $0.categoriaSecundaria == self.filterCategory}
        }
        else{
            return directoryItems
        }
        
    }
    
    public func update(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = DirectoryRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: DirectoryItemModel.self)
                        
                        var directoryItems = [DirectoryItemModel]()
                        
                        for element in results{
                            let directoryItem = DirectoryItemModel(info: element)
                            
                            directoryItems.append(directoryItem)
                        }
                        
                        DataBaseManager.shared.save(objects: directoryItems)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
    public func getCategories() -> [HermesObject] {
        let directoryCategories = DataBaseManager.shared.getObjects(type: DirectoryCategoryModel.self).map{DirectoryCategory(model: $0)}.sorted{$0.orden < $1.orden}
        
        return directoryCategories
        
    }
    
    public func updateCategories(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = DirectoryRequest()
        
        request.downloadCategories(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: DirectoryCategoryModel.self)
                        
                        var directoryCategories = [DirectoryCategoryModel]()
                        
                        for element in results{
                            let directoryCategory = DirectoryCategoryModel(info: element)
                            
                            directoryCategories.append(directoryCategory)
                        }
                        
                        DataBaseManager.shared.save(objects: directoryCategories)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
}
