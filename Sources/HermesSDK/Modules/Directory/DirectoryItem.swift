//
//  DirectoryItem.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 12/11/2020.
//

import Foundation
import UIKit

public class DirectoryItem : HermesObject {
        
    internal init(model: DirectoryItemModel) {
        self.id = model.id
        self.nombre = model.nombre
        self.categoriPrincipal = model.categoriPrincipal
        self.categoriaSecundaria = model.categoriaSecundaria
        self.codigo = model.codigo
        self.direccion = model.direccion
        self.correo = model.correo
        self.telefono = model.telefono
        self.descripcion = model.descripcion
        self.tipo = model.tipo
        self.tematica = model.tematica
        self.horario = model.horario
        self.latitud = (model.latitud as NSString).doubleValue
        self.longitud = (model.longitud as NSString).doubleValue
        self.imagenes = Array(model.imagenes)
        self.orden = model.orden
    }
    
    public var id : Int
    public var nombre : String
    public var categoriPrincipal : String
    public var categoriaSecundaria : String
    public var codigo : String
    public var direccion : String
    public var correo : String
    public var telefono : String
    public var descripcion : String
    public var tipo : String
    public var tematica : String
    public var horario : String
    public var latitud : Double
    public var longitud : Double
    public var imagenes : [String]
    public var orden : Int
}
