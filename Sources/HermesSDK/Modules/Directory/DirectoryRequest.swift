//
//  DirectoryRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 12/11/2020.
//

import Foundation

class DirectoryRequest{
    
    func download(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = DIRECTORY_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded"]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "appToken", value: appToken))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "userToken", value: userToken))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            guard let str = jsonData["str"] as? String else {completion(results, .error); return}
            
            if str == "OK"{
                
                guard let info = jsonData["directorio"] as? [[String : Any]] else {completion(results, .error); return}
                
                results = info
                
                completion(results, .correct)
                return
            }
            else{
                completion(results, .error)
                return
            }
            

        })
        
    }
    
    func downloadCategories(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = DIRECTORY_CATEGORIES_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
        
}
