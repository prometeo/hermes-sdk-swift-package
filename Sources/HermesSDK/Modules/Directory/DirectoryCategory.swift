//
//  DirectoryCategory.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 9/12/21.
//

import Foundation
import UIKit

public class DirectoryCategory : Hashable, HermesObject {
    
    public static func == (lhs: DirectoryCategory, rhs: DirectoryCategory) -> Bool {

        return lhs.id == rhs.id
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.id)
    }
        
    internal init(model: DirectoryCategoryModel) {
        self.id = model.id
        self.nombre = model.nombre
        self.alias = model.alias
        self.orden = model.orden
        self.esRuta = model.esRuta
        self.imagen = model.imagen
        self.descripcion = model.descripcion
        self.html = model.html
    }
    
    public func descripcionHtml(color: UIColor) -> NSAttributedString {
        return Formatter.convertHtml(string: self.html, font: UIFont(name: "Helvetica Neue", size: 18.0)!, color: color) ?? NSAttributedString(string: "")
    }
    
    public var id : Int
    public var nombre : String
    public var alias : String
    public var orden : Int
    public var esRuta : Bool
    public var imagen : String
    public var descripcion : String
    public var html : String

}
