//
//  HermesCMS.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 22/3/21.
//

import Foundation

public class HermesCMS: HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        let user = users.first
        
        let cmsItems = DataBaseManager.shared.getObjects(type: CMSItemModel.self).filter{
            if $0.esPublico == false{
                if user?.anonimo == true {
                    return false
                }
                else{
                    return true
                }
            }
            
            return true
            
        }.sorted{$0.orden < $1.orden}.map{CMSItem(model: $0)}
        
        return cmsItems
    }
    
    public func update(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = CMSRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: CMSItemModel.self)
                        
                        var cmsItems = [CMSItemModel]()
                        
                        for element in results{
                            let cmsItem = CMSItemModel(info: element)
                            
                            cmsItems.append(cmsItem)
                        }
                        
                        DataBaseManager.shared.save(objects: cmsItems)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
}
