//
//  CMSCategory.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 22/3/21.
//

import Foundation
import UIKit

public class CMSCategory : HermesObject {
        
    internal init(model: CMSCategoryModel) {
        self.id = model.id
        self.nombre = model.nombre
        self.alias = model.alias
        self.subscribible = model.subscribible
    }
    
    public var id : Int
    public var nombre : String
    public var alias : String
    public var subscribible : Bool
    
}
