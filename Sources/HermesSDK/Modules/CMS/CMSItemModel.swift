//
//  CMSItemModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 22/3/21.
//

import Foundation
import RealmSwift

class CMSItemModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var orden : Int  = 0
    @objc dynamic var titulo : String = ""
    @objc dynamic var etiqueta : String = ""
    @objc dynamic var contenido : String = ""
    @objc dynamic var url : String = ""
    @objc dynamic var activo : Bool = false
    @objc dynamic var esPublico : Bool = false
    @objc dynamic var esMenu : Bool = false
    @objc dynamic var color : String = ""
    @objc dynamic var iconClass : String = ""
    @objc dynamic var views : Int  = 0
    @objc dynamic var parent : Int = -1
    @objc dynamic var slug : String = ""
    
    let categorias = List<CMSCategoryModel>()
    let children = List<Int>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.orden = info["orden"] as? Int ?? 0
        self.titulo = info["titulo"] as? String ?? ""
        self.etiqueta = info["etiqueta"] as? String ?? ""
        self.contenido = info["contenido"] as? String ?? ""
        self.url = info["url"] as? String ?? ""
        self.activo = info["activo"] as? Bool ?? false
        self.esPublico = info["is_publico"] as? Bool ?? false
        self.esMenu = info["es_menu"] as? Bool ?? false
        self.color = info["color"] as? String ?? ""
        self.iconClass = info["icon_class"] as? String ?? ""
        self.views = info["views"] as? Int ?? 0
        
        if let categorias = info["categorias"] as? [[String :Any]]{
            for categoria in categorias{
                let categoriaCMS = CMSCategoryModel(info: categoria)
                self.categorias.append(categoriaCMS)
            }
        }
                
        if let children = info["children"] as? [[String :Any]]{
            for child in children{
                if let childId = child["id"] as? Int{
                    self.children.append(childId)
                }
            }
        }
        
        if let parent = info["parent"] as? [String :Any]{
            self.parent = parent["id"] as? Int ?? -1
        }
        
        self.slug = info["slug"] as? String ?? ""
    }
    
    
}
