//
//  CMSCategoryModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 22/3/21.
//

import Foundation
import RealmSwift

class CMSCategoryModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var nombre : String = ""
    @objc dynamic var alias : String = ""
    @objc dynamic var subscribible : Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.nombre = info["nombre"] as? String ?? ""
        self.alias = info["alias"] as? String ?? ""
        self.subscribible = info["subscribible"] as? Bool ?? false
    }
    
    
}
