//
//  CMSItem.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 22/3/21.
//

import Foundation
import UIKit
import RealmSwift

public class CMSItem : HermesObject {
        
    internal init(model: CMSItemModel) {
        self.id = model.id
        self.orden = model.orden
        self.titulo = model.titulo
        self.etiqueta = model.etiqueta
        self.contenido = model.contenido
        self.url = model.url
        self.activo = model.activo
        self.esPublico = model.esPublico
        self.esMenu = model.esMenu
        self.color = model.color
        self.iconClass = model.iconClass
        self.views = model.views
        self.categorias = model.categorias.map{CMSCategory(model: $0)}
        self.childrenIds = model.children
        self.parent = model.parent
        self.slug = model.slug
    }
    
    public var id : Int
    public var orden : Int
    public var titulo : String
    public var etiqueta : String
    public var contenido : String
    public var url : String
    public var activo : Bool
    public var esPublico : Bool
    public var esMenu : Bool
    public var color : String
    public var iconClass : String
    public var views : Int
    public var categorias : [CMSCategory]
    public var parent : Int
    public var childrenIds : List<Int>
    public var slug : String
    
    public var children : [CMSItem] {
        
        var children = DataBaseManager.shared.filter(type: CMSItemModel.self, identifiers: self.childrenIds)
        
        if children.count == 0{
            children = DataBaseManager.shared.getObjects(type: CMSItemModel.self).filter{$0.parent == self.id}
        }
        
        return children.map{CMSItem(model: $0)}.sorted{$0.orden < $1.orden}
    }
    
    
}
