//
//  OfferSuscriber.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 5/7/23.
//

import Foundation
import UIKit

public class OfferSubscription : HermesObject {
        
    internal init(model: OfferSubscriptionModel) {
        self.id = model.id
        self.idPropietario = model.idPropietario
        self.nombre = model.nombre
        self.apellidos = model.apellidos
        self.comentarioPrivado = model.comentarioPrivado
        self.descartar = model.descartar
        self.destacado = model.destacado
    }
    
    public var id : Int
    public var idPropietario : Int
    public var nombre : String
    public var apellidos : String
    public var destacado : Bool
    public var descartar : Bool
    public var comentarioPrivado : String
}
