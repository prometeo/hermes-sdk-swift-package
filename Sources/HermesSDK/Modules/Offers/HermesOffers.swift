//
//  HermesEvents.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 09/11/2020.
//

import Foundation

public class HermesOffers: HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        let offers = DataBaseManager.shared.getObjects(type: OfferModel.self).sorted{$0.fechaInicio > $1.fechaInicio}.map{Offer(model: $0)}
        
        return offers
    }
    
    public func update(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = OffersRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        let oldEvents = DataBaseManager.shared.getObjects(type: OfferModel.self)
                        
                        for oldEvent in oldEvents{
                            if oldEvent.local == false{
                                DataBaseManager.shared.delete(object: oldEvent)
                            }
                        }
                                                
                        var offers = [OfferModel]()
                        
                        for element in results{
                            let offer = OfferModel(info: element)
                            
                            offers.append(offer)
                        }
                        
                        DataBaseManager.shared.save(objects: offers)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
    public func subscribeTo(offer: Offer, contentOwnerId : String, completion: ((SubscribeResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.incorrect)
                }
                
            }
            return
        }
                
        let request = OffersRequest()
        
        request.subscribe(appToken: self.appToken, userToken: userToken, contentOwnerId: contentOwnerId, offerId: String(offer.id), formularioId: String(offer.idFormularioInscripcion), completion: {(requestResult) -> () in
            
            DispatchQueue.main.async {
                if completion != nil{
                    completion!(requestResult)
                }
            }
        })
        
    }
    
    public func create(title: String, startDate: Date, endDate: Date, jsonData: String, formId: String, contentOwnerId: String, completion: ((UpdateResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.error)
                }
                
            }
            return
        }
                
        let startDateString = Formatter.dateToString(date: startDate, format: "yyyy-MM-dd'T'HH:mm:ssZ").replacingOccurrences(of: "+", with: "%2B")
        let endDateString = Formatter.dateToString(date: endDate, format: "yyyy-MM-dd'T'HH:mm:ssZ").replacingOccurrences(of: "+", with: "%2B")
        
        let request = OffersRequest()
        
        request.create(appToken: self.appToken, userToken: userToken, title: title, startDate: startDateString, endDate: endDateString, jsonData: jsonData, formId: formId, contentOwnerId: contentOwnerId, completion: {(offer, requestResult) -> () in
            
            DispatchQueue.main.async {
                
                if requestResult == .correct{
                    if completion != nil{
                        completion!(.correct)
                    }
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
                
            }
        })
        
    }
    
    public func edit(id: Int, title: String, startDate: Date, endDate: Date, jsonData: String, formId: String, contentOwnerId: String, completion: ((UpdateResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.error)
                }
                
            }
            return
        }
                
        let startDateString = Formatter.dateToString(date: startDate, format: "yyyy-MM-dd'T'HH:mm:ssZ").replacingOccurrences(of: "+", with: "%2B")
        let endDateString = Formatter.dateToString(date: endDate, format: "yyyy-MM-dd'T'HH:mm:ssZ").replacingOccurrences(of: "+", with: "%2B")
        
        let request = OffersRequest()
        
        request.edit(appToken: self.appToken, userToken: userToken, id: id, title: title, startDate: startDateString, endDate: endDateString, jsonData: jsonData, formId: formId, contentOwnerId: contentOwnerId, completion: {(offer, requestResult) -> () in
            
            DispatchQueue.main.async {
                
                if requestResult == .correct{
                    if completion != nil{
                        completion!(.correct)
                    }
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
                
            }
        })
        
    }
    
    public func changeState(id: Int, active: Bool, completion: ((UpdateResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.error)
                }
                
            }
            return
        }
                

        let request = OffersRequest()
        
        request.changeState(appToken: self.appToken, userToken: userToken, id: id, active: active, completion: {(offer, requestResult) -> () in
            
            DispatchQueue.main.async {
                
                if requestResult == .correct{
                    if completion != nil{
                        completion!(.correct)
                    }
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
                
            }
        })
        
    }
    
    public func updateSubscription(id : String, comment: String, accepted: Bool, rejected: Bool, completion: ((SubscribeResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.incorrect)
                }
                
            }
            return
        }
                
        let request = OffersRequest()
        
        request.updateSubscription(appToken: self.appToken, userToken: userToken, id: id, comment: comment, accepted: accepted, rejected: rejected, completion: {(requestResult) -> () in
            
            DispatchQueue.main.async {
                if completion != nil{
                    completion!(requestResult)
                }
            }
        })
        
    }
    
    public func getOffer(id: Int, completion: ((Offer?, RequestResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(nil, .error)
                }
                
            }
            return
        }
                
        let request = OffersRequest()
        request.getOffer(appToken: self.appToken, userToken: userToken, id: id, completion: {(offerInfo, requestResult) in
            
            DispatchQueue.main.async {
                if requestResult == .correct {
                    
                    let offerModel = OfferModel(info: offerInfo)
                    let offer = Offer(model: offerModel)
                    
                    if completion != nil{
                        completion!(offer, .correct)
                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(nil, .error)
                    }
                }
            }
        })
    }
}
