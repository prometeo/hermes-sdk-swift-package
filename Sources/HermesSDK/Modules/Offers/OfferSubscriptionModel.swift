//
//  OfferSuscriberModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 5/7/23.
//

import Foundation
import RealmSwift

class OfferSubscriptionModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var titulo : String = ""
    @objc dynamic var idPropietario : Int  = 0
    @objc dynamic var nombre : String = ""
    @objc dynamic var apellidos : String = ""
    @objc dynamic var comentarioPrivado : String = ""
    @objc dynamic var destacado : Bool = false
    @objc dynamic var descartar : Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.titulo = info["titulo"] as? String ?? ""
        self.comentarioPrivado = info["comentario_privado"] as? String ?? ""
        self.destacado = info["destacado"] as? Bool ?? false
        self.descartar = info["descartar"] as? Bool ?? false
        
        if let paciente = info["paciente"] as? [String : Any]{
            self.idPropietario = paciente["id"] as? Int ?? 0
            self.nombre = paciente["nombre"] as? String ?? ""
            self.apellidos = paciente["apellidos"] as? String ?? ""
        }
        
        
    }
    
    
}
