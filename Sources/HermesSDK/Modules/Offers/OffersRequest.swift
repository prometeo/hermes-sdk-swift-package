//
//  OffersRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 09/11/2020.
//

import Foundation

class OffersRequest{
    
    func download(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = OFFERS_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            
            if result.contains("OBJECT_NOT_EXIST"){
                completion(results, .correct)
                
                return
            }
            
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
        })
        
    }
    
    func subscribe(appToken: String, userToken: String, contentOwnerId: String, offerId: String, formularioId: String, completion: (@escaping(SubscribeResult) -> ())){
        
        let url = SUBSCRIBE_TO_OFFERS_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "paciente[id]", value: contentOwnerId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "formulario[id]", value: formularioId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "jsonformulario", value: "[{\"valor\":true,\"nombre\":\"inscribirse\",\"tipo\":\"checkbox\"}]"))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "evento[id]", value: offerId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "evento[type]", value: "convocatoria"))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.incorrect); return}
            guard responseCode == .success else {completion(.incorrect); return}
            
            completion(.correct)
            return
            

        })
        
    }
    
    func create(appToken: String, userToken: String, title: String, startDate: String, endDate: String, jsonData: String, formId: String, contentOwnerId: String, completion: (@escaping([String : Any], RequestResult) -> ())){
        
        let url = CREATE_OFFER_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "fecha_inicio", value: startDate))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "fecha_fin", value: endDate))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "titulo", value: title))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "type", value: "convocatoria"))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "formulario[id]", value: formId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "json_data", value: jsonData))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "propietariocontenido[id]", value: contentOwnerId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "propietariocontenido[type]", value: "propcontenido"))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "is_publico", value: "true"))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [String : Any]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
    
    func edit(appToken: String, userToken: String, id:Int, title: String, startDate: String, endDate: String, jsonData: String, formId: String, contentOwnerId: String, completion: (@escaping([String : Any], RequestResult) -> ())){
        
        let url = EDIT_OFFER_ENDPOINT + String(id)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "PATCH")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "fecha_inicio", value: startDate))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "fecha_fin", value: endDate))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "titulo", value: title))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "type", value: "convocatoria"))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "formulario[id]", value: formId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "json_data", value: jsonData))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "propietariocontenido[id]", value: contentOwnerId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "propietariocontenido[type]", value: "propcontenido"))
        
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [String : Any]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
    
    func changeState(appToken: String, userToken: String, id:Int, active: Bool, completion: (@escaping([String : Any], RequestResult) -> ())){
        
        let url = EDIT_OFFER_ENDPOINT + String(id)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "PATCH")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "type", value: "convocatoria"))
        
        if active == true{
            bodyCompontents.queryItems!.append(URLQueryItem(name: "is_publico", value: "true"))
        }
        else{
            bodyCompontents.queryItems!.append(URLQueryItem(name: "is_publico", value: "false"))
        }
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [String : Any]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
    
    func updateSubscription(appToken: String, userToken: String, id: String, comment: String, accepted: Bool, rejected: Bool, completion: (@escaping(SubscribeResult) -> ())){
        
        let url = UPDATE_SUBSCRIPTION_TO_OFFERS_ENDPOINT.replacingOccurrences(of: "$1", with: id)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "PATCH")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "comentario_privado", value: comment))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "descartar", value: String(rejected)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "destacado", value: String(accepted)))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.incorrect); return}
            guard responseCode == .success else {completion(.incorrect); return}
            
            completion(.correct)
            return
            

        })
        
    }
    
    func getOffer(appToken: String, userToken: String, id: Int, completion: (@escaping([String : Any], RequestResult) -> ())){
        
        let url = GET_OFFER_ENDPOINT.replacingOccurrences(of: "$0", with: String(id))
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [String : Any]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            

        })
        
    }
}
