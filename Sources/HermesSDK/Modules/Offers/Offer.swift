//
//  Offer.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 09/11/2020.
//

import Foundation
import UIKit

public class Offer : HermesObject {
    
    
    internal init(model: OfferModel) {
        
        self.id = model.id
        self.titulo = model.titulo
        
        self.descripcionRaw = model.descripcion
        self.descripcionCortaRaw = model.descripcionCorta
        
        self.ubicacion = model.ubicacion
        
        if let fechaInicioDate = Formatter.stringToDate(string: model.fechaInicio, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            self.fechaInicio = fechaInicioDate
        }
        else{
            self.fechaInicio = Date()
        }
        
        if let fechaFinDate = Formatter.stringToDate(string: model.fechaFin, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            self.fechaFin = fechaFinDate
        }
        else{
            self.fechaFin = Date()
        }
        
        self.imagen = model.imagen
        self.urlInscripcion = model.urlInscripcion
        self.aforo = model.aforo
        self.inscritos = model.inscritos
        self.latitud = model.latitud
        self.longitud = model.longitud
        self.ponente = model.ponente
        self.direccion = model.direccion
        self.precio = model.precio
        self.entidad = model.entidad
        self.imagenEntidad = model.imagenEntidad
        self.webEntidad = model.webEntidad
        self.completo = model.completo
        self.publico = model.publico
        self.inscrito = model.inscrito
        self.idFormularioInscripcion = model.idFormularioInscripcion
        self.categorias = model.categorias.map{OfferCategory(model: $0)}
        self.local = model.local
        self.jsonData = model.jsonData
        self.suscripciones = model.suscripciones.map{OfferSubscription(model: $0)}
        self.idPropietarioContenido = model.idPropietarioContenido
    }
    
    public init(offer: Offer) {
        
        self.id = offer.id
        self.titulo = offer.titulo
        self.descripcionRaw = offer.descripcion
        self.descripcionCortaRaw = offer.descripcionCorta
        self.ubicacion = offer.ubicacion
        self.fechaInicio = offer.fechaInicio
        self.fechaFin = offer.fechaFin
        self.imagen = offer.imagen
        self.urlInscripcion = offer.urlInscripcion
        self.aforo = offer.aforo
        self.inscritos = offer.inscritos
        self.latitud = offer.latitud
        self.longitud = offer.longitud
        self.ponente = offer.ponente
        self.direccion = offer.direccion
        self.precio = offer.precio
        self.entidad = offer.entidad
        self.imagenEntidad = offer.imagenEntidad
        self.webEntidad = offer.webEntidad
        self.completo = offer.completo
        self.publico = offer.publico
        self.inscrito = offer.inscrito
        self.idFormularioInscripcion = offer.idFormularioInscripcion
        self.categorias = offer.categorias
        self.local = offer.local
        self.jsonData = offer.jsonData
        self.suscripciones = offer.suscripciones
        self.idPropietarioContenido = offer.idPropietarioContenido
    }
    
    private var descripcionRaw : String
    private var descripcionCortaRaw : String
    
    public var id : Int
    public var titulo : String
    
    public var descripcion : String {
        return Formatter.removeHtml(string: self.descripcionRaw)
    }
    
    public var descripcionHtml : NSAttributedString {
        return Formatter.convertHtml(string: self.descripcionRaw, font: UIFont(name: "Helvetica Neue", size: 18.0)!, color: UIColor.darkGray) ?? NSAttributedString(string: "")
    }
    
    public var descripcionCorta : String {
        return Formatter.removeHtml(string: self.descripcionCortaRaw)
    }
    
    public var descripcionCortaHtml : NSAttributedString {
        return Formatter.convertHtml(string: self.descripcionCortaRaw, font: UIFont(name: "Helvetica Neue", size: 18.0)!, color: UIColor.darkGray) ?? NSAttributedString(string: "")
    }
    
    public var ubicacion : String
    public var fechaInicio : Date
    public var fechaFin : Date
    
    public func fechaInicio(formato: String) -> String {
        
        return Formatter.dateToString(date: self.fechaInicio, format: formato)
    }
    
    public func fechaFin(formato: String) -> String {
        
        return Formatter.dateToString(date: self.fechaFin, format: formato)
    }
    
    public var imagen : String
    public var urlInscripcion : String
    public var aforo : Int
    public var inscritos : Int
    public var latitud : String
    public var longitud : String
    public var ponente : String
    public var direccion : String
    public var precio : String
    public var entidad : String
    public var imagenEntidad : String
    public var webEntidad : String
    public var completo : Bool
    public var publico : Bool
    public var inscrito : Bool
    public var idFormularioInscripcion : Int
    public var jsonData : String
    public var idPropietarioContenido : Int
    
    public var entidadVisible : Bool {
        
        let entidades = DataBaseManager.shared.getObjects(type: EntityModel.self)
        
        if entidades.count > 1{
            return true
        }
        else{
            return false
        }
        
    }
    
    public var categorias : [OfferCategory]
    
    public var local : Bool
    
    public var leida : Bool {
        guard let readNews : [Int] = UserDefaults.standard.array(forKey: "readOffers") as? [Int] else {return false}
        
        return readNews.contains(self.id)
        
    }
    
    public func marcarLeida(){
        
        if var readNews : [Int] = UserDefaults.standard.array(forKey: "readOffers") as? [Int]{
            
            if readNews.contains(self.id) == false{
                readNews.append(self.id)
                
                UserDefaults.standard.set(readNews, forKey: "readOffers")
            }
            
        }
        else{
            UserDefaults.standard.set([self.id], forKey: "readOffers")
        }
    }
    
    public var suscripciones : [OfferSubscription]
    
    public func marcarFavorita(){
        
        if var favoriteOffers : [Int] = UserDefaults.standard.array(forKey: "favoriteOffers") as? [Int]{
            
            if favoriteOffers.contains(self.id) == false{
                favoriteOffers.append(self.id)
                
                UserDefaults.standard.set(favoriteOffers, forKey: "favoriteOffers")
            }
            
        }
        else{
            UserDefaults.standard.set([self.id], forKey: "favoriteOffers")
        }
    }
    
    public func quitarFavorita(){
        
        if var favoriteOffers : [Int] = UserDefaults.standard.array(forKey: "favoriteOffers") as? [Int]{
            
            if favoriteOffers.contains(self.id) == true{
                if let index = favoriteOffers.firstIndex(of: self.id){
                    favoriteOffers.remove(at: index)
                }
                
                UserDefaults.standard.set(favoriteOffers, forKey: "favoriteOffers")
            }
            
        }

    }
    
    public var favorita : Bool {
        guard let favoriteOffers : [Int] = UserDefaults.standard.array(forKey: "favoriteOffers") as? [Int] else {return false}
        
        let esFavorita = favoriteOffers.contains(self.id)
        
        return esFavorita
        
    }
    
    public var estaDesactivada : Bool {
        
        return (self.fechaFin < Date()) || (self.publico == false)
    }
    
    public var hayNuevosCandidatos : Bool {
        
        let key = "candidatosAnteriores-" + String(self.id)
        let candidatosAnteriores = UserDefaults.standard.integer(forKey: key)
        
        if self.suscripciones.count > candidatosAnteriores{
            return true
        }
        
        return false
    }
    
    public func guardarNumeroCandidatos(){
        let key = "candidatosAnteriores-" + String(self.id)
        
        UserDefaults.standard.set(self.suscripciones.count, forKey: key)
    }
}
