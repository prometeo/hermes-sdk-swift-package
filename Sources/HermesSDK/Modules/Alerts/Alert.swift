//
//  Alert.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 2/12/22.
//

import Foundation

public class Alert : HermesObject {
        
    internal init(model: AlertModel) {
        self.id = model.id
        self.fechaEnvio = model.fechaEnvio
        self.nombre = model.nombre
        self.contenido = model.contenido
    }
        
    public var id : Int
    public var nombre : String
    public var contenido : String
    public var fechaEnvio : String
    
    public func fecha(formato: String) -> String{
        
        var resultado = ""
        
        if let fechaDate = Formatter.stringToDate(string: self.fechaEnvio, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
        
        return resultado
    }
    
    
    
}
