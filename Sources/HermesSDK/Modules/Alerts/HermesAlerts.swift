//
//  HermesAlerts.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 2/12/22.
//

import Foundation

public class HermesAlerts {
    
    private var userToken : String?
    private var appToken : String!
    
    public init() {

        self.userToken = nil

        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }

    }
    
    public func get() -> [HermesObject] {
        
        let alerts = DataBaseManager.shared.getObjects(type: AlertModel.self).map{Alert(model: $0)}
        
        return alerts
    }
    
    public func update(completion: ((UpdateResult) -> ())?) {
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
        
        let request = AlertsRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: AlertModel.self)
                        
                        var alerts = [AlertModel]()
                        
                        for element in results{
                            let alert = AlertModel(info: element)
                            
                            alerts.append(alert)
                        }
                        
                        DataBaseManager.shared.save(objects: alerts)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
    }
    
}
