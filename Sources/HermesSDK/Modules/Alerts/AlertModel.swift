//
//  AlertModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 2/12/22.
//

import Foundation
import RealmSwift

class AlertModel: Object {
    
    @objc dynamic var id : Int = 0
    @objc dynamic var fechaEnvio : String = ""
    @objc dynamic var nombre : String = ""
    @objc dynamic var contenido : String  = ""

    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.fechaEnvio = info["fecha_envio"] as? String ?? ""
        self.nombre = info["nombre"] as? String ?? ""
        self.contenido = info["contenido"] as? String ?? ""
        
    }
    
    
}
