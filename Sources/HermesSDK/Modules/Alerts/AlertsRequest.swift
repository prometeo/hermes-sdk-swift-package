//
//  AlertsRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 2/12/22.
//

import Foundation

class AlertsRequest {
    
    func download(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = ALERTS_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            
            if result.contains("OBJECT_NOT_EXIST"){
                completion(results, .correct)
                
                return
            }
            
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
}
