//
//  ProgramModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 9/1/23.
//

import Foundation
import RealmSwift

class ProgramModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var nombre : String = ""
    @objc dynamic var descripcion : String = ""
    @objc dynamic var fechaInicio : String = ""
    @objc dynamic var horaInicio : String = ""
    @objc dynamic var duracion : Int = 0
    @objc dynamic var duracionTipo : Int = 0
    @objc dynamic var foto : String = ""
    @objc dynamic var avisar : Bool = false
    @objc dynamic var periodicidad : Int = 0
    @objc dynamic var periodicidadTipo : Int = 0
    @objc dynamic var textoAviso : String = ""
    @objc dynamic var activarAlarma : Bool = false
    @objc dynamic var activarNotificacion : Bool = false
    @objc dynamic var soloAviso : Bool = false
    @objc dynamic var confirmarToma : Bool = false
    @objc dynamic var confirmarPosponer : Bool = false
    @objc dynamic var activo : Bool = true
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.nombre = info["nombre"] as? String ?? ""
        self.descripcion = info["descripcion"] as? String ?? ""
        self.fechaInicio = info["fecha_inicio"] as? String ?? ""
        self.horaInicio = info["hora_inicio"] as? String ?? ""
        self.duracion = info["duracion"] as? Int ?? 0
        self.duracionTipo = info["duracion_tipo"] as? Int ?? 0
        self.foto = info["foto"] as? String ?? ""
        self.avisar = info["avisar"] as? Bool ?? false
        self.periodicidad = info["periodicidad"] as? Int ?? 0
        self.periodicidadTipo = info["periodicidad_tipo"] as? Int ?? 0
        self.textoAviso = info["texto_aviso"] as? String ?? ""
        self.activarAlarma = info["activar_alarma"] as? Bool ?? false
        self.activarNotificacion = info["activar_notificacion"] as? Bool ?? false
        self.soloAviso = info["solo_aviso"] as? Bool ?? false
        self.confirmarToma = info["confirmar_toma"] as? Bool ?? false
        self.confirmarPosponer = info["confirmar_posponer"] as? Bool ?? false
        self.activo = info["activo"] as? Bool ?? false
    }
    
    
}

public enum PeriodType : Int {
    case horas = 0
    case dias = 1
    case semanas = 2
    case meses = 3
}
