//
//  HermesPrograms.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 9/1/23.
//

import Foundation

public class HermesPrograms : HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    
    public init() {
        self.userToken = nil
        
        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        return DataBaseManager.shared.getObjects(type: ProgramModel.self).map{Program(model: $0)}
    }
    
    public func update(completion: ((UpdateResult) -> ())?) {
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = ProgramsRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: ProgramModel.self)
                        
                        var programItems = [ProgramModel]()
                        
                        for element in results{
                            let programItem = ProgramModel(info: element)
                            
                            programItems.append(programItem)
                        }
                        
                        DataBaseManager.shared.save(objects: programItems)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
    }
    
    public func saveProgram(nombre: String, descripcion: String, fechaInicio: Date, horaInicio: Date, duracion: Int, duracionTipo: PeriodType, foto: String, avisar: Bool, periodicidad: Int, periodicidadTipo: PeriodType, textoAviso: String, activarAlarma: Bool, activarNotificacion: Bool, soloAviso: Bool, confirmarToma: Bool, confirmarPosponer: Bool, activo: Bool, completion: ((UpdateResult) -> ())?) {
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
        
        let request = ProgramsRequest()
        
        let fechaInicioString = Formatter.dateToString(date: fechaInicio, format: "yyyy-MM-dd'T'HH:mm:ssZ").replacingOccurrences(of: "+", with: "%2B")
        let horaInicioString = Formatter.dateToString(date: horaInicio, format: "yyyy-MM-dd'T'HH:mm:ssZ").replacingOccurrences(of: "+", with: "%2B")
        
        request.create(appToken: self.appToken, userToken: userToken, nombre: nombre, descripcion: descripcion, fechaInicio: fechaInicioString, horaInicio: horaInicioString, duracion: duracion, duracionTipo: duracionTipo, foto: foto, avisar: avisar, periodicidad: periodicidad, periodicidadTipo: periodicidadTipo, textoAviso: textoAviso, activarAlarma: activarAlarma, activarNotificacion: activarNotificacion, soloAviso: soloAviso, confirmarToma: confirmarToma, confirmarPosponer: confirmarPosponer, activo: activo, completion: {(data, createResult) -> () in
            
            DispatchQueue.main.async {
                if createResult == .correct{
                    
                    if completion != nil{
                        completion!(.correct)
                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
        })
        
    }
    
    public func updateProgram(program: Program, completion: ((UpdateResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
        
        let request = ProgramsRequest()
        
        let fechaInicioString = program.fechaInicio(formato: "yyyy-MM-dd'T'HH:mm:ssZ").replacingOccurrences(of: "+", with: "%2B")
        let horaInicioString = program.horaInicio(formato: "yyyy-MM-dd'T'HH:mm:ssZ").replacingOccurrences(of: "+", with: "%2B")
        
        request.edit(appToken: self.appToken, userToken: userToken, id: program.id, nombre: program.nombre, descripcion: program.descripcion, fechaInicio: fechaInicioString, horaInicio: horaInicioString, duracion: program.duracion, duracionTipo: program.duracionTipo, foto: program.foto, avisar: program.avisar, periodicidad: program.periodicidad, periodicidadTipo: program.periodicidadTipo, textoAviso: program.textoAviso, activarAlarma: program.activarAlarma, activarNotificacion: program.activarNotificacion, soloAviso: program.soloAviso, confirmarToma: program.confirmarToma, confirmarPosponer: program.confirmarPosponer, activo: program.activo, completion: {(data, createResult) -> () in
            
            DispatchQueue.main.async {
                if createResult == .correct{
                    
                    if completion != nil{
                        completion!(.correct)
                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
        })
        
    }
    
}
