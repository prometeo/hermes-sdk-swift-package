//
//  ProgramsRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 16/2/23.
//

import Foundation

class ProgramsRequest{
    
    func download(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = PROGRAMS_ENDPOINT + "?$locale=" + LOCALE
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            
            if result.contains("OBJECT_NOT_EXIST"){
                completion(results, .correct)
                
                return
            }
            
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
    
    func create(appToken: String, userToken: String, nombre: String, descripcion: String, fechaInicio: String, horaInicio: String, duracion: Int, duracionTipo: PeriodType, foto: String, avisar: Bool, periodicidad: Int, periodicidadTipo: PeriodType, textoAviso: String, activarAlarma: Bool, activarNotificacion: Bool, soloAviso: Bool, confirmarToma: Bool, confirmarPosponer: Bool, activo: Bool, completion: (@escaping([String : Any], RequestResult) -> ())){
        
        let url = PROGRAMS_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "nombre", value: nombre))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "descripcion", value: descripcion))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "fecha_inicio", value: fechaInicio))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "hora_inicio", value: horaInicio))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "duracion", value: String(duracion)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "duracion_tipo", value: String(duracionTipo.rawValue)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "foto", value: foto))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "avisar", value: String(avisar)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "periodicidad", value: String(periodicidad)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "periodicidad_tipo", value: String(periodicidadTipo.rawValue)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "texto_aviso", value: textoAviso))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "activar_alarma", value: String(activarAlarma)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "activar_notificacion", value: String(activarNotificacion)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "solo_aviso", value: String(soloAviso)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "confirmar_toma", value: String(confirmarToma)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "confirmar_posponer", value: String(confirmarPosponer)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "activo", value: String(activo)))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [String : Any]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
    
    func edit(appToken: String, userToken: String, id: Int, nombre: String, descripcion: String, fechaInicio: String, horaInicio: String, duracion: Int, duracionTipo: PeriodType, foto: String, avisar: Bool, periodicidad: Int, periodicidadTipo: PeriodType, textoAviso: String, activarAlarma: Bool, activarNotificacion: Bool, soloAviso: Bool, confirmarToma: Bool, confirmarPosponer: Bool, activo: Bool, completion: (@escaping([String : Any], RequestResult) -> ())){
        
        let url = PROGRAMS_ENDPOINT + "/" + String(id)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "PATCH")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "nombre", value: nombre))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "descripcion", value: descripcion))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "fecha_inicio", value: fechaInicio))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "hora_inicio", value: horaInicio))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "duracion", value: String(duracion)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "duracion_tipo", value: String(duracionTipo.rawValue)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "foto", value: foto))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "avisar", value: String(avisar)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "periodicidad", value: String(periodicidad)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "periodicidad_tipo", value: String(periodicidadTipo.rawValue)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "texto_aviso", value: textoAviso))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "activar_alarma", value: String(activarAlarma)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "activar_notificacion", value: String(activarNotificacion)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "solo_aviso", value: String(soloAviso)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "confirmar_toma", value: String(confirmarToma)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "confirmar_posponer", value: String(confirmarPosponer)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "activo", value: String(activo)))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [String : Any]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
    
}
