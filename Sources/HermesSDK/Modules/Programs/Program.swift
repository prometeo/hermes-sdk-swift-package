//
//  Program.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 9/1/23.
//

import Foundation
import RealmSwift

public class Program : HermesObject {
    
    public init() {
        self.id = 0
        self.nombre = ""
        self.descripcion = ""
        self.fechaInicio = ""
        self.horaInicio = ""
        self.duracion = 0
        self.duracionTipo = .horas
        self.foto = ""
        self.avisar = false
        self.periodicidad = 0
        self.periodicidadTipo = .horas
        self.textoAviso = ""
        self.activarAlarma = false
        self.activarNotificacion = false
        self.soloAviso = false
        self.confirmarToma = false
        self.confirmarPosponer = false
        self.activo = true
    }
    
    internal init(model: ProgramModel) {
        self.id = model.id
        self.nombre = model.nombre
        self.descripcion = model.descripcion
        self.fechaInicio = model.fechaInicio
        self.horaInicio = model.horaInicio
        self.duracion = model.duracion
        self.duracionTipo = PeriodType(rawValue: model.duracionTipo) ?? .horas
        self.foto = model.foto
        self.avisar = model.avisar
        self.periodicidad = model.periodicidad
        self.periodicidadTipo = PeriodType(rawValue: model.periodicidadTipo) ?? .horas
        self.textoAviso = model.textoAviso
        self.activarAlarma = model.activarAlarma
        self.activarNotificacion = model.activarNotificacion
        self.soloAviso = model.soloAviso
        self.confirmarToma = model.confirmarToma
        self.confirmarPosponer = model.confirmarPosponer
        self.activo = model.activo
    }
    
    public var id : Int
    public var nombre : String
    public var descripcion : String
    public var fechaInicio : String
    public var horaInicio : String
    public var duracion : Int
    public var duracionTipo : PeriodType
    public var foto : String
    public var avisar : Bool
    public var periodicidad : Int
    public var periodicidadTipo : PeriodType
    public var textoAviso : String
    public var activarAlarma : Bool
    public var activarNotificacion : Bool
    public var soloAviso : Bool
    public var confirmarToma : Bool
    public var confirmarPosponer : Bool
    public var activo : Bool
    
    public func fechaInicio(formato: String) -> String {
        var resultado = ""
    
        if let fechaDate = Formatter.stringToDate(string: self.fechaInicio, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
    
        return resultado
    }
    
    public func horaInicio(formato: String) -> String {
        var resultado = ""
    
        if let fechaDate = Formatter.stringToDate(string: self.horaInicio, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
    
        return resultado
    }
}
