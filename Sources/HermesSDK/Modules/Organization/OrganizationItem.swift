//
//  OrganizationItem.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 6/4/21.
//

import Foundation
import UIKit
import RealmSwift

public class OrganizationItem : HermesObject {
        
    internal init(model: OrganizationItemModel) {
        self.id = model.id
        self.nombre = model.nombre
        self.descripcion = model.descripcion
        self.correo = model.correo
        self.direccion = model.direccion
        self.telefono = model.telefono
        self.ocultar = model.ocultar
        self.level = model.level
        self.parent = model.parent
        self.childrenIds = model.children
        self.responsables = model.responsables.map{OrganizationResponsible(model: $0)}.sorted{$0.nombre < $1.nombre}
        self.miembros = model.miembros.map{OrganizationMember(model: $0)}.sorted{$0.nombre < $1.nombre}
    }
    
    public var id : Int
    public var nombre : String
    public var descripcion : String
    public var correo : String
    public var direccion : String
    public var telefono : String
    public var ocultar : Bool
    public var level : Int
    public var parent : Int
    public var responsables : [OrganizationResponsible]
    public var miembros : [OrganizationMember]
    public var childrenIds : List<Int>
    
    public var children : [OrganizationItem] {
        
        var children = DataBaseManager.shared.filter(type: OrganizationItemModel.self, identifiers: self.childrenIds)
        
        if children.isEmpty {
            children = DataBaseManager.shared.getObjects(type: OrganizationItemModel.self).filter{$0.parent == self.id}
        }
        
        return children.map{OrganizationItem(model: $0)}.sorted{$0.nombre < $1.nombre}
    }
    
    public var descripcionHtml : NSAttributedString {
        return Formatter.convertHtml(string:self.descripcion, font: UIFont(name: "Helvetica Neue", size: 18.0)!, color: UIColor.darkGray) ?? NSAttributedString(string: "")
    }
    
}
