//
//  OrganizationItemModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 6/4/21.
//

import Foundation
import RealmSwift

class OrganizationItemModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var nombre : String = ""
    @objc dynamic var descripcion : String = ""
    @objc dynamic var correo : String = ""
    @objc dynamic var direccion : String = ""
    @objc dynamic var telefono : String = ""
    @objc dynamic var ocultar : Bool = false
    @objc dynamic var level : Int  = 0
    @objc dynamic var parent : Int = -1
    
    let children = List<Int>()
    let responsables = List<OrganizationResponsibleModel>()
    let miembros = List<OrganizationMemberModel>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.nombre = info["nombre"] as? String ?? ""
        self.descripcion = info["descripcion"] as? String ?? ""
        self.correo = info["correo"] as? String ?? ""
        self.direccion = info["direccion"] as? String ?? ""
        self.telefono = info["telefono"] as? String ?? ""
        self.ocultar = info["ocultar"] as? Bool ?? false
        self.level = info["level"] as? Int ?? 0
        
        if let children = info["children"] as? [[String :Any]]{
            for child in children{
                if let childId = child["id"] as? Int{
                    self.children.append(childId)
                }
            }
        }
        
        if let parent = info["parent"] as? [String :Any]{
            self.parent = parent["id"] as? Int ?? -1
        }
        
        if self.parent == -1{
            self.parent = info["parent"] as? Int ?? -1
        }
        
        if let responsables = info["responsables"] as? [[String :Any]]{
            for responsable in responsables{
                
                let organizationResponsible = OrganizationResponsibleModel(info: responsable)
                
                self.responsables.append(organizationResponsible)
            }
        }
        
        if let miembros = info["miembros"] as? [[String :Any]]{
            for miembro in miembros{
                
                let organizationMember = OrganizationMemberModel(info: miembro)
                
                self.miembros.append(organizationMember)
            }
        }
    }
    
    
}
