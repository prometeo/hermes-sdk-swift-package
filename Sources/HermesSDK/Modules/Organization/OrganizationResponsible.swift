//
//  OrganizationResponsible.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 6/4/21.
//

import Foundation
import UIKit

public class OrganizationResponsible : HermesObject {
    
    
    internal init(model: OrganizationResponsibleModel) {
        self.nombre = model.nombre
        self.apellidos = model.apellidos
        self.cargo = model.cargo
        self.comentario = model.comentario
        self.correo = model.correo
        self.ext = model.ext
        self.imagen = model.imagen
        self.telefono = model.telefono
        self.titulo = model.titulo
        self.activo = model.activo
        self.borrado = model.borrado
        self.visible = model.visible
    }
            
    public var nombre : String
    public var apellidos : String
    public var cargo : String
    public var comentario : String
    public var correo : String
    public var ext : String
    public var imagen : String
    public var telefono : String
    public var titulo : String
    public var activo : Bool
    public var borrado : Bool
    public var visible : Bool
    
}
