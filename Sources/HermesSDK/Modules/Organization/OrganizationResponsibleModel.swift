//
//  OrganizationResponsibleModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 6/4/21.
//

import Foundation
import RealmSwift

class OrganizationResponsibleModel: Object {
    
    @objc dynamic var nombre : String = ""
    @objc dynamic var apellidos : String = ""
    @objc dynamic var cargo : String = ""
    @objc dynamic var comentario : String = ""
    @objc dynamic var correo : String = ""
    @objc dynamic var ext : String = ""
    @objc dynamic var imagen : String = ""
    @objc dynamic var telefono : String = ""
    @objc dynamic var titulo : String = ""
    @objc dynamic var activo : Bool = true
    @objc dynamic var borrado : Bool = false
    @objc dynamic var visible : Bool = true
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.nombre = info["nombre"] as? String ?? ""
        self.apellidos = info["apellidos"] as? String ?? ""
        self.cargo = info["cargo"] as? String ?? ""
        self.comentario = info["comentario_publico"] as? String ?? ""
        self.correo = info["correo"] as? String ?? ""
        self.ext = info["extension"] as? String ?? ""
        self.telefono = info["telefono"] as? String ?? ""
        self.titulo = info["titulo"] as? String ?? ""
        self.activo = info["activo"] as? Bool ?? true
        self.borrado = info["es_borrado"] as? Bool ?? false
        self.visible = info["visible"] as? Bool ?? true
        
        if let imagen = info["imagen"] as? String{
            if imagen.hasPrefix("http") == false{
                self.imagen = (BASE_URL + imagen).replacingOccurrences(of: "//", with: "/")
            }
            else{
                self.imagen = imagen
            }
        }
    }
    
    
}
