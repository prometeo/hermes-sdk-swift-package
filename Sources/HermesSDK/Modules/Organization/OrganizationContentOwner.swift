//
//  OrganizationContentOwner.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 15/11/22.
//

import Foundation
import UIKit

public class OrganizationContentOwner : HermesObject {
        
    internal init(model: OrganizationContentOwnerModel) {
        self.id = model.id
        self.alias = model.alias
        self.nombre = model.nombre
        self.apellidos = model.apellidos
        self.telefono = model.telefono
        self.token = model.token
        self.fechaAlta = model.fechaAlta
        self.color = model.color
        self.colorTxt = model.colorTxt
        self.imagen = model.imagen
        self.idEntidad = model.idEntidad
        self.nombreEntidad = model.nombreEntidad
        self.direccionEntidad = model.direccionEntidad
        self.correoEntidad = model.correoEntidad
        self.telefonoEntidad = model.telefonoEntidad
        self.codigoEntidad = model.codigoEntidad
        self.imagenEntidad = model.imagenEntidad
        self.webEntidad = model.webEntidad
        self.dni = model.dni
        self.idExterno = model.idExterno
        self.direccion = model.direccion
        self.email = model.email
    }
    
    public var id : Int
    public var alias : String
    public var nombre : String
    public var apellidos : String
    public var telefono : String
    public var token : String
    public var fechaAlta : String
    public var color : String
    public var colorTxt : String
    public var imagen : String
    public var idEntidad : Int
    public var nombreEntidad : String
    public var direccionEntidad : String
    public var correoEntidad : String
    public var telefonoEntidad : String
    public var codigoEntidad : String
    public var imagenEntidad : String
    public var webEntidad : String
    public var dni : String
    public var idExterno : String
    public var direccion : String
    public var email : String
}
