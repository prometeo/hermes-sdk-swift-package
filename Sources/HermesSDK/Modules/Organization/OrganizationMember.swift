//
//  OrganizationMember.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 11/11/22.
//

import Foundation
import UIKit

public class OrganizationMember : HermesObject {
    
    
    internal init(model: OrganizationMemberModel) {
        self.id = model.id
        self.nombre = model.nombre
        self.apellidos = model.apellidos
        self.correo = model.correo
        self.imagen = model.imagen

    }
            
    public var id : Int
    public var nombre : String
    public var apellidos : String
    public var correo : String
    public var imagen : String

    
}
