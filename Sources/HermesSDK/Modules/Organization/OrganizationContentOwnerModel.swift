//
//  OrganizationContentOwnerModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 15/11/22.
//

import Foundation
import RealmSwift

class OrganizationContentOwnerModel: Object {
    
    @objc dynamic var id : Int = 0
    @objc dynamic var alias : String = ""
    @objc dynamic var nombre : String = ""
    @objc dynamic var apellidos : String  = ""
    @objc dynamic var telefono : String  = ""
    @objc dynamic var token : String  = ""
    @objc dynamic var fechaAlta : String  = ""
    @objc dynamic var color : String  = ""
    @objc dynamic var colorTxt : String  = ""
    @objc dynamic var imagen : String  = ""
    @objc dynamic var idEntidad : Int = 0
    @objc dynamic var nombreEntidad : String = ""
    @objc dynamic var direccionEntidad : String = ""
    @objc dynamic var correoEntidad : String = ""
    @objc dynamic var telefonoEntidad : String = ""
    @objc dynamic var codigoEntidad : String = ""
    @objc dynamic var imagenEntidad : String = ""
    @objc dynamic var webEntidad : String = ""
    @objc dynamic var dni : String = ""
    @objc dynamic var direccion : String = ""
    @objc dynamic var idExterno : String = ""
    @objc dynamic var email : String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.nombre = info["nombre"] as? String ?? ""
        self.apellidos = info["apellidos"] as? String ?? ""
        self.telefono = info["telefono"] as? String ?? ""
        self.token = info["tokenPaciente"] as? String ?? ""
        self.fechaAlta = info["fechaAlta"] as? String ?? ""
        self.color = info["color"] as? String ?? ""
        self.colorTxt = info["color_txt"] as? String ?? ""
        self.imagen = info["image_route"] as? String ?? ""
        self.dni = info["dni"] as? String ?? ""
        
        if self.token == ""{
            self.token = info["token_paciente"] as? String ?? ""
        }
        
        if self.fechaAlta == ""{
            self.fechaAlta = info["fecha_alta"] as? String ?? ""
        }
        
        if self.colorTxt == ""{
            self.colorTxt = info["color_txt"] as? String ?? ""
        }
        
        let nombreCompleto = self.nombre + " " + self.apellidos
        self.alias = nombreCompleto.getAcronyms()
        
        if let entidades = info["entidades"] as? [[String : Any]], let entidad = entidades.first {
            self.idEntidad = entidad["id"] as? Int ?? 0
            self.nombreEntidad = entidad["nombre"] as? String ?? ""
            self.direccionEntidad = entidad["direccion"] as? String ?? ""
            self.correoEntidad = entidad["correo"] as? String ?? ""
            self.telefonoEntidad = entidad["telefono"] as? String ?? ""
            self.codigoEntidad = entidad["codigo"] as? String ?? ""
            self.imagenEntidad = entidad["image_route"] as? String ?? ""
            self.webEntidad = entidad["paginaweb"] as? String ?? ""
        }
        
        if let entidad = info["entidad"] as? [String : Any]{
            self.idEntidad = entidad["id"] as? Int ?? 0
            self.nombreEntidad = entidad["nombre"] as? String ?? ""
            self.direccionEntidad = entidad["direccion"] as? String ?? ""
            self.correoEntidad = entidad["correo"] as? String ?? ""
            self.telefonoEntidad = entidad["telefono"] as? String ?? ""
            self.codigoEntidad = entidad["codigo"] as? String ?? ""
            self.imagenEntidad = entidad["image_route"] as? String ?? ""
            self.webEntidad = entidad["paginaweb"] as? String ?? ""
        }
        
        self.direccion = info["direccion"] as? String ?? ""
        self.idExterno = info["identificador_externo"] as? String ?? ""
        
        self.email = info["correo"] as? String ?? ""
    }
    
    
}
