//
//  HermesOrganization.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 6/4/21.
//

import Foundation

public class HermesOrganization: HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        
        let organizationItems = DataBaseManager.shared.getObjects(type: OrganizationItemModel.self).sorted{$0.nombre.uppercased() < $1.nombre.uppercased()}.map{OrganizationItem(model: $0)}
        
        return organizationItems
    }
    
    public func update(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = OrganizationRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: OrganizationItemModel.self)
                        
                        var organizationItems = [OrganizationItemModel]()
                        
                        for element in results{
                            let organizationItem = OrganizationItemModel(info: element)
                            
                            organizationItems.append(organizationItem)
                        }
                        
                        DataBaseManager.shared.save(objects: organizationItems)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
    public func updateOrganizationContentOwners(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = OrganizationRequest()
        
        request.downloadContentOwners(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                                                
                        DataBaseManager.shared.deleteObjects(type: OrganizationContentOwnerModel.self)
                        
                        var contentOwners = [OrganizationContentOwnerModel]()
                        
                        for element in results{
                            let contentOwner = OrganizationContentOwnerModel(info: element)
                            
                            contentOwners.append(contentOwner)
                            
                        }
                        
                        DataBaseManager.shared.save(objects: contentOwners)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
    public func getOrganizationContentOwners() -> [OrganizationContentOwner] {
        let chats = DataBaseManager.shared.getObjects(type: OrganizationContentOwnerModel.self).map{OrganizationContentOwner(model: $0)}
        
        return chats
    }
}
