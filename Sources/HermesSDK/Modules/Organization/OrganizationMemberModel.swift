//
//  OrganizationMemberModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 11/11/22.
//

import Foundation
import RealmSwift

class OrganizationMemberModel: Object {
    
    @objc dynamic var id : Int = 0
    @objc dynamic var nombre : String = ""
    @objc dynamic var apellidos : String = ""
    @objc dynamic var correo : String = ""
    @objc dynamic var imagen : String = ""

    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.nombre = info["nombre"] as? String ?? ""
        self.apellidos = info["apellidos"] as? String ?? ""
        self.correo = info["correo"] as? String ?? ""

        if let imagen = info["imagen"] as? String{
            if imagen.hasPrefix("http") == false{
                self.imagen = (BASE_URL + imagen).replacingOccurrences(of: "//", with: "/")
            }
            else{
                self.imagen = imagen
            }
        }
    }
    
    
}
