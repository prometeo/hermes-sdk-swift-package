//
//  HermesTexts.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 9/9/21.
//

import Foundation

public class HermesTexts: HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        
        let texts = DataBaseManager.shared.getObjects(type: TextModel.self).map{Text(model: $0)}
        
        return texts
    }
    
    public func update(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = TextsRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: TextModel.self)
                        
                        var texts = [TextModel]()
                        
                        for element in results{
                            let text = TextModel(info: element)
                            
                            texts.append(text)
                        }
                        
                        DataBaseManager.shared.save(objects: texts)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
}
