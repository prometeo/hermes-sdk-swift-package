//
//  TextModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 9/9/21.
//

import Foundation
import RealmSwift

class TextModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var token : String = ""
    @objc dynamic var descripcion : String = ""
    @objc dynamic var texto : String = ""
    @objc dynamic var idEntidad : Int = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.token = info["token"] as? String ?? ""
        self.descripcion = info["descripcion"] as? String ?? ""
        self.texto = info["texto"] as? String ?? ""
        
        if let entidad = info["entidad"] as? [String : Any]{
            self.idEntidad = entidad["id"] as? Int ?? 0
        }
    }
}
