//
//  Text.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 9/9/21.
//

import Foundation

public class Text : HermesObject {
        
    internal init(model: TextModel) {
        self.id = model.id
        self.token = model.token
        self.descripcion = model.descripcion
        self.texto = model.texto
        self.idEntidad = model.idEntidad
    }
    
    public var id : Int
    public var token : String
    public var descripcion : String
    public var texto : String
    public var idEntidad : Int
    
}
