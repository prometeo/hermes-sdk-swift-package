//
//  Incidence.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 10/11/2020.
//

import Foundation
import UIKit

public class Incidence : HermesObject {
        
    internal init(model: IncidenceModel) {
        self.id = model.id
        self.nombre = model.nombre
        self.texto = Formatter.removeHtml(string: model.texto) 
        self.fechaEnvio = model.fechaEnvio
        self.estado = IncidenceStatus(rawValue: model.estado) ?? .inReview
        self.idPerfil = model.idPerfil
        self.nombrePerfil = model.nombrePerfil
        self.idProveedor = model.idProveedor
        self.nombreProveedor = model.nombreProveedor
        self.correoProveedor = model.correoProveedor
        self.telefonoProveedor = model.telefonoProveedor
        self.tipoProveedor = model.tipoProveedor
        self.observaciones = model.obervaciones
        self.imagenes = Array(model.imagenes)
    }
    
    public var id : Int
    public var nombre : String
    public var texto : String
    public var observaciones : String
    public var fechaEnvio : String
    
    public func fecha(formato: String) -> String{
        
        var resultado = ""
        
        if let fechaDate = Formatter.stringToDate(string: self.fechaEnvio, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
        
        return resultado
    }
    
    public var estado : IncidenceStatus
    public var idPerfil : Int
    public var nombrePerfil : String
    public var idProveedor : Int
    public var nombreProveedor : String
    public var correoProveedor : String
    public var telefonoProveedor : String
    public var tipoProveedor : String
    public var imagenes : [String]

}

public enum IncidenceStatus : Int {
    case inReview = 1
    case approved = 2
    case cancelled = 3
}
