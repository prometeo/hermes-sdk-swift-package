//
//  IncidencesRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 10/11/2020.
//

import Foundation
import UIKit

class IncidencesRequest{
    
    /*func download(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
     
     let url = INCIDENCES_ENDPOINT
     
     let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded"]
     
     let request = Request(endpoint: url, method: "POST")
     
     var bodyCompontents = URLComponents()
     bodyCompontents.queryItems = [URLQueryItem]()
     bodyCompontents.queryItems!.append(URLQueryItem(name: "appToken", value: appToken))
     bodyCompontents.queryItems!.append(URLQueryItem(name: "userToken", value: userToken))
     
     let body = bodyCompontents.query?.data(using: .utf8)
     
     request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
     
     let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
     var results = [[String : Any]]()
     
     guard error == nil else {completion(results, .error); return}
     guard responseCode == .success else {completion(results, .error); return}
     
     guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
     
     guard let str = jsonData["str"] as? String else {completion(results, .error); return}
     
     let message = jsonData["message"] as? String ?? ""
     
     if str == "OK" || message == "NOT_FOUND"{
     
     let info = jsonData["incidencias"] as? [[String : Any]] ?? [[String : Any]]()
     
     results = info
     
     completion(results, .correct)
     return
     }
     else{
     completion(results, .error)
     return
     }
     
     
     })
     
     }*/
    
    func download(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = INCIDENCES_ENDPOINT + "?$locale=" + LOCALE
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            
            if result.contains("OBJECT_NOT_EXIST"){
                completion(results, .correct)
                
                return
            }
            
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
    
    
    func downloadProfiles(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = INCIDENCES_PROFILES_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded"]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "appToken", value: appToken))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "userToken", value: userToken))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            guard let str = jsonData["str"] as? String else {completion(results, .error); return}
            
            if str == "OK"{
                
                guard let info = jsonData["perfiles_incidencia"] as? [[String : Any]] else {completion(results, .error); return}
                
                results = info
                
                completion(results, .correct)
                return
            }
            else{
                completion(results, .error)
                return
            }
            
            
        })
        
    }
    
    /*func createIncidence(appToken: String, userToken: String, title: String, description: String, profile: String, images: [UIImage]?, latitude: Double?, longitude: Double?, completion: (@escaping(RequestResult) -> ())){
     
     let url = CREATE_INCIDENCE_ENDPOINT
     
     let request = Request(endpoint: url, method: "POST")
     
     var body = Data()
     
     let boundary = "------VohpleBoundary4sadfasdfeafe4w5w45435hwttre"
     let contentType = "multipart/form-data; boundary=" + boundary
     let separator = "--" + boundary + "\r\n"
     let newLine = "\r\n"
     
     let paramNameAppToken = "Content-Disposition: form-data; name=\"appToken\"\r\n\r\n"
     let paramNameUserToken = "Content-Disposition: form-data; name=\"userToken\"\r\n\r\n"
     let paramNameTitle = "Content-Disposition: form-data; name=\"titulo\"\r\n\r\n"
     let paramNameDescription = "Content-Disposition: form-data; name=\"descripcion\"\r\n\r\n"
     let paramNameProfile = "Content-Disposition: form-data; name=\"idPerfil\"\r\n\r\n"
     let paramNameLatitude = "Content-Disposition: form-data; name=\"latitud\"\r\n\r\n"
     let paramNameLongitude = "Content-Disposition: form-data; name=\"longitud\"\r\n\r\n"
     
     
     body.append(separator.data(using: .utf8)!)
     body.append(paramNameAppToken.data(using: .utf8)!)
     body.append(appToken.data(using: .utf8)!)
     body.append(newLine.data(using: .utf8)!)
     
     body.append(separator.data(using: .utf8)!)
     body.append(paramNameUserToken.data(using: .utf8)!)
     body.append(userToken.data(using: .utf8)!)
     body.append(newLine.data(using: .utf8)!)
     
     body.append(separator.data(using: .utf8)!)
     body.append(paramNameTitle.data(using: .utf8)!)
     body.append(title.data(using: .utf8)!)
     body.append(newLine.data(using: .utf8)!)
     
     body.append(separator.data(using: .utf8)!)
     body.append(paramNameDescription.data(using: .utf8)!)
     body.append(description.data(using: .utf8)!)
     body.append(newLine.data(using: .utf8)!)
     
     body.append(separator.data(using: .utf8)!)
     body.append(paramNameProfile.data(using: .utf8)!)
     body.append(profile.data(using: .utf8)!)
     body.append(newLine.data(using: .utf8)!)
     
     if let latitudeIncidence = latitude, let longitudeIncidence = longitude{
     
     body.append(separator.data(using: .utf8)!)
     body.append(paramNameLatitude.data(using: .utf8)!)
     body.append(String(latitudeIncidence).data(using: .utf8)!)
     body.append(newLine.data(using: .utf8)!)
     
     body.append(separator.data(using: .utf8)!)
     body.append(paramNameLongitude.data(using: .utf8)!)
     body.append(String(longitudeIncidence).data(using: .utf8)!)
     body.append(newLine.data(using: .utf8)!)
     }
     
     let typeImage = "Content-Type:image/jpeg\r\n\r\n"
     
     if let imagesIncidence = images {
     
     for i in 0..<imagesIncidence.count{
     
     let image = imagesIncidence[i]
     
     guard let imageData = image.compress() else {continue}
     
     body.append(separator.data(using: .utf8)!)
     
     let imageField = "Content-Disposition: form-data; name=\"imagenes[]\"; filename=\"imagen" + String(i) + ".jpg\"\r\n"
     
     body.append(imageField.data(using: .utf8)!)
     body.append(typeImage.data(using: .utf8)!)
     body.append(imageData)
     body.append(newLine.data(using: .utf8)!)
     }
     }
     
     let end = "--" + boundary + "--\r\n"
     
     body.append(end.data(using: .utf8)!)
     
     let headers : [String : String] = ["Content-Type" : contentType]
     
     request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
     
     let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
     
     guard error == nil else {completion(.error); return}
     guard responseCode == .success else {completion(.error); return}
     
     guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion( .error); return}
     
     guard let str = jsonData["str"] as? String else {completion(.error); return}
     
     if str == "OK"{
     
     completion(.correct)
     return
     }
     else{
     completion(.error)
     return
     }
     })
     }*/
    
    func createIncidence(appToken: String, userToken: String, entityId: String, contentOwnerId: String, title: String, description: String, profile: String, images: [UIImage]?, latitude: Double?, longitude: Double?, completion: (@escaping(RequestResult) -> ())){
        
        let url = CREATE_INCIDENCE_ENDPOINT
        
        let request = Request(endpoint: url, method: "POST")
        
        var body = Data()
        
        let boundary = "------VohpleBoundary4sadfasdfeafe4w5w45435hwttre"
        let contentType = "multipart/form-data; boundary=" + boundary
        let separator = "--" + boundary + "\r\n"
        let newLine = "\r\n"
        
        let paramNameEntidad = "Content-Disposition: form-data; name=\"entidad[id]\"\r\n\r\n"
        let paramNameTitle = "Content-Disposition: form-data; name=\"titulo\"\r\n\r\n"
        let paramNameDescription = "Content-Disposition: form-data; name=\"descripcion\"\r\n\r\n"
        let paramNameProfile = "Content-Disposition: form-data; name=\"perfil[id]\"\r\n\r\n"
        let paramNameLatitude = "Content-Disposition: form-data; name=\"latitud\"\r\n\r\n"
        let paramNameLongitude = "Content-Disposition: form-data; name=\"longitud\"\r\n\r\n"
        let paramNamePaciente = "Content-Disposition: form-data; name=\"paciente[id]\"\r\n\r\n"
        let paramNameProveedor = "Content-Disposition: form-data; name=\"proveedor[0][id]\"\r\n\r\n"
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameEntidad.data(using: .utf8)!)
        body.append(entityId.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameTitle.data(using: .utf8)!)
        body.append(title.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameDescription.data(using: .utf8)!)
        body.append(description.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameProfile.data(using: .utf8)!)
        body.append(profile.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNamePaciente.data(using: .utf8)!)
        body.append(contentOwnerId.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameProveedor.data(using: .utf8)!)
        body.append("50".data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        if let latitudeIncidence = latitude, let longitudeIncidence = longitude{
            
            body.append(separator.data(using: .utf8)!)
            body.append(paramNameLatitude.data(using: .utf8)!)
            body.append(String(latitudeIncidence).data(using: .utf8)!)
            body.append(newLine.data(using: .utf8)!)
            
            body.append(separator.data(using: .utf8)!)
            body.append(paramNameLongitude.data(using: .utf8)!)
            body.append(String(longitudeIncidence).data(using: .utf8)!)
            body.append(newLine.data(using: .utf8)!)
        }
        
        let typeImage = "Content-Type:image/jpeg\r\n\r\n"
        
        if let imagesIncidence = images {
            
            for i in 0..<imagesIncidence.count{
                
                let image = imagesIncidence[i]
                
                guard let imageData = image.compress() else {continue}
                
                body.append(separator.data(using: .utf8)!)
                
                let imageField = "Content-Disposition: form-data; name=\"imagenes[]\"; filename=\"imagen" + String(i) + ".jpg\"\r\n"
                
                body.append(imageField.data(using: .utf8)!)
                body.append(typeImage.data(using: .utf8)!)
                body.append(imageData)
                body.append(newLine.data(using: .utf8)!)
            }
        }
        
        let end = "--" + boundary + "--\r\n"
        
        body.append(end.data(using: .utf8)!)
        
        let headers : [String : String] = ["Content-Type" : contentType, "appToken" : appToken, "userToken" : userToken]
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            completion(.correct)
            return
        })
    }
    
}
