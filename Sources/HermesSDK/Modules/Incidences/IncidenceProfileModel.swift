//
//  IncidenceProfileModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 17/11/2020.
//

import Foundation
import RealmSwift

class IncidenceProfileModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var nombre : String = ""

    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.nombre = info["nombre"] as? String ?? ""

    }
    
    
}
