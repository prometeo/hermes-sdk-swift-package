//
//  IncidenceModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 10/11/2020.
//

import Foundation
import RealmSwift

class IncidenceModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var nombre : String = ""
    @objc dynamic var texto : String = ""
    @objc dynamic var fechaEnvio : String = ""
    @objc dynamic var estado : Int  = 0
    @objc dynamic var idPerfil : Int  = 0
    @objc dynamic var nombrePerfil : String = ""
    @objc dynamic var idProveedor : Int  = 0
    @objc dynamic var nombreProveedor : String = ""
    @objc dynamic var correoProveedor : String = ""
    @objc dynamic var telefonoProveedor : String = ""
    @objc dynamic var tipoProveedor : String = ""
    @objc dynamic var obervaciones : String = ""
    
    let imagenes = List<String>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    //V2
    /*init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.nombre = info["nombre"] as? String ?? ""
        self.texto = info["textoLibre"] as? String ?? ""
        self.fechaEnvio = info["fecha_envio"] as? String ?? ""
        self.estado = info["estado"] as? Int ?? 0
        self.obervaciones = info["observaciones_publicas"] as? String ?? ""
        
        if let perfil = info["perfil"] as? [String : Any]{
            self.idPerfil = perfil["id"] as? Int ?? 0
            self.nombrePerfil = perfil["nombre"] as? String ?? ""
        }
        
        if let proveedores = info["proveedor"] as? [[String : Any]]{
            if let proveedor = proveedores.first {
                self.idProveedor = proveedor["id"] as? Int ?? 0
                self.nombreProveedor = proveedor["nombre"] as? String ?? ""
                self.correoProveedor = proveedor["correo"] as? String ?? ""
                self.telefonoProveedor = proveedor["telefono"] as? String ?? ""
                self.tipoProveedor = proveedor["tipo"] as? String ?? ""
            }
        }
        
        if let imagenesIncidencia = info["imagenes"] as? [String]{
            for imagen in imagenesIncidencia{
                if imagen.hasPrefix("http") == false{
                    self.imagenes.append((BASE_URL + "uploads/" + imagen).replacingOccurrences(of: "uploads//uploads", with: "uploads").replacingOccurrences(of: "//", with: "/"))
                }
                else{
                    self.imagenes.append(imagen)
                }
                
                
            }
        }
    }*/
    
    
    //V4
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.nombre = info["titulo"] as? String ?? ""
        self.texto = info["descripcion"] as? String ?? ""
        self.fechaEnvio = info["fecha_envio"] as? String ?? ""
        self.obervaciones = info["observaciones_publicas"] as? String ?? ""
        
        if let estado_incidencia = info["estado_incidencia"] as? [String : Any]{
            self.estado = estado_incidencia["id"] as? Int ?? 0
        }
        
        if let perfil = info["perfil"] as? [String : Any]{
            self.idPerfil = perfil["id"] as? Int ?? 0
            self.nombrePerfil = perfil["nombre"] as? String ?? ""
        }
        
        if let proveedores = info["proveedor"] as? [[String : Any]]{
            if let proveedor = proveedores.first {
                self.idProveedor = proveedor["id"] as? Int ?? 0
                self.nombreProveedor = proveedor["nombre"] as? String ?? ""
                self.correoProveedor = proveedor["correo"] as? String ?? ""
                self.telefonoProveedor = proveedor["telefono"] as? String ?? ""
                self.tipoProveedor = proveedor["tipo"] as? String ?? ""
            }
        }
        
        if let imagenesIncidencia = info["images"] as? [String]{
            for imagen in imagenesIncidencia{
                if imagen.hasPrefix("http") == false{
                    self.imagenes.append((BASE_URL + "uploads/" + imagen).replacingOccurrences(of: "uploads//uploads", with: "uploads").replacingOccurrences(of: "//", with: "/"))
                }
                else{
                    self.imagenes.append(imagen)
                }
                
                
            }
        }
    }
    
    
}
