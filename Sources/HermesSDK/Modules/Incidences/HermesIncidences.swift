//
//  HermesIncidences.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 10/11/2020.
//

import Foundation
import UIKit

public class HermesIncidences: HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    private var entityId : String!
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        self.entityId = ENTITY_ID
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        let incidences = DataBaseManager.shared.getObjects(type: IncidenceModel.self).sorted{$0.fechaEnvio > $1.fechaEnvio}.map{Incidence(model: $0)}
        
        return incidences
    }
    
    public func update(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = IncidencesRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: IncidenceModel.self)
                        
                        var incidences = [IncidenceModel]()
                        
                        for element in results{
                            let incidence = IncidenceModel(info: element)
                            
                            incidences.append(incidence)
                        }
                        
                        DataBaseManager.shared.save(objects: incidences)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
    public func getProfiles() -> [IncidenceProfile] {
        let profiles = DataBaseManager.shared.getObjects(type: IncidenceProfileModel.self).map{IncidenceProfile(model: $0)}
        
        return profiles
    }
    
    public func updateProfiles(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = IncidencesRequest()
        
        request.downloadProfiles(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: IncidenceProfileModel.self)
                        
                        var profiles = [IncidenceProfileModel]()
                        
                        for element in results{
                            let profile = IncidenceProfileModel(info: element)
                            
                            profiles.append(profile)
                        }
                        
                        DataBaseManager.shared.save(objects: profiles)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
    public func getUserName() -> String{
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            return user.nombre + " " + user.apellido
        }
        
        return ""
    }
    
    public func getUserEmail() -> String{
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            return user.correo
        }
        
        return ""
    }
    
    public func create(contentOwner: ContentOwner, title: String, description: String, profile: String, images: [UIImage]?, latitude: Double?, longitude: Double?, completion: ((SendResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.error)
                }
                
            }
            return
        }
                
        let request = IncidencesRequest()
        
        //request.createIncidence(appToken: self.appToken, userToken: userToken, title: title, description: description, profile: profile, images: images, latitude: latitude, longitude: longitude, completion: {(requestResult) -> () in
            
        request.createIncidence(appToken: self.appToken, userToken: userToken, entityId: self.entityId, contentOwnerId: String(contentOwner.id), title: title, description: description, profile: profile, images: images, latitude: latitude, longitude: longitude, completion: {(requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    self.update(completion: {(updateResult) -> () in
                        
                        DispatchQueue.main.async {
                            
                            if completion != nil{
                                completion!(.correct)
                            }

                        }
                        
                    })

                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
}
