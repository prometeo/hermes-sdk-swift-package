//
//  IncidenceProfile.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 17/11/2020.
//

import Foundation
import UIKit

public class IncidenceProfile : HermesObject {
        
    internal init(model: IncidenceProfileModel) {
        self.id = model.id
        self.nombre = model.nombre
    }
    
    public var id : Int
    public var nombre : String
    

}
