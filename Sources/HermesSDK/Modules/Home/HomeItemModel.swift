//
//  HomeItemModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 19/5/22.
//

import Foundation
import RealmSwift

class HomeItemModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var tipo : String = ""
    @objc dynamic var orden : Int  = 0
    @objc dynamic var categoriaNoticias : String = ""
    @objc dynamic var categoriaEventos : String = ""
    @objc dynamic var activoLogueado : Bool = false
    @objc dynamic var activoNoLogueado : Bool = false
    @objc dynamic var enlace1Titulo : String = ""
    @objc dynamic var enlace1Link : String = ""
    @objc dynamic var enlace1Imagen : String = ""
    @objc dynamic var enlace2Titulo : String = ""
    @objc dynamic var enlace2Link : String = ""
    @objc dynamic var enlace2Imagen : String = ""
    @objc dynamic var enlace3Titulo : String = ""
    @objc dynamic var enlace3Link : String = ""
    @objc dynamic var enlace3Imagen : String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.tipo = info["tipo_tarjeta_str"] as? String ?? ""
        self.orden = info["orden"] as? Int ?? 0
        
        if let categorias = info["categorias"] as? [[String : Any]]{
            if let categoria = categorias.first{
                if let idCategoria = categoria["id"] as? Int{
                    self.categoriaNoticias = String(idCategoria)
                }
            }
        }
        
        if let categorias_eventos = info["categoria_eventos"] as? [[String : Any]]{
            if let categoria = categorias_eventos.first{
                if let idCategoria = categoria["id"] as? Int{
                    self.categoriaEventos = String(idCategoria)
                }
            }
        }
        self.activoLogueado = info["logueado_enabled"] as? Bool ?? false
        self.activoNoLogueado = info["no_logueado_enabled"] as? Bool ?? false
        
        if let enlace_home1 = info["enlace_home1"] as? [String : Any]{
            self.enlace1Titulo = enlace_home1["titulo"] as? String ?? ""
            self.enlace1Link = enlace_home1["enlace"] as? String ?? ""
        }
        
        let image1 = info["image1"] as? String ?? ""
        self.enlace1Imagen = BASE_URL + "uploads/images/photos/" + image1
        
        if let enlace_home2 = info["enlace_home2"] as? [String : Any]{
            self.enlace2Titulo = enlace_home2["titulo"] as? String ?? ""
            self.enlace2Link = enlace_home2["enlace"] as? String ?? ""
        }
        
        let image2 = info["image2"] as? String ?? ""
        self.enlace2Imagen = BASE_URL + "uploads/images/photos/" + image2
        
        if let enlace_home3 = info["enlace_home3"] as? [String : Any]{
            self.enlace3Titulo = enlace_home3["titulo"] as? String ?? ""
            self.enlace3Link = enlace_home3["enlace"] as? String ?? ""
        }
        
        let image3 = info["image3"] as? String ?? ""
        self.enlace3Imagen = BASE_URL + "uploads/images/photos/" + image3
        
        self.enlace1Imagen = self.enlace1Imagen.replacingOccurrences(of: "/uploads/images/photos//uploads/images/photos/", with: "/uploads/images/photos/")
        self.enlace2Imagen = self.enlace2Imagen.replacingOccurrences(of: "/uploads/images/photos//uploads/images/photos/", with: "/uploads/images/photos/")
        self.enlace3Imagen = self.enlace3Imagen.replacingOccurrences(of: "/uploads/images/photos//uploads/images/photos/", with: "/uploads/images/photos/")
    }
    
}
