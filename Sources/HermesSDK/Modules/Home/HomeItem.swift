//
//  HomeItem.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 19/5/22.
//

import Foundation

public class HomeItem : HermesObject {
        
    internal init(model: HomeItemModel) {
        self.id = model.id
        self.orden = model.orden
        self.categoriaNoticias = model.categoriaNoticias
        self.categoriaEventos = model.categoriaEventos
        self.activoLogueado = model.activoLogueado
        self.activoNoLogueado = model.activoNoLogueado
        
        switch model.tipo {
        case NEWS_TYPE:
            self.tipo = .news
            
        case MESSAGES_TYPE:
            self.tipo = .messages
            
        case PROFILE_TYPE:
            self.tipo = .profile
            
        case EVENTS_TYPE:
            self.tipo = .events
            
        case ONE_LINK_TYPE:
            self.tipo = .oneLink
            
        case THREE_LINKS_TYPE:
            self.tipo = .threeLinks
            
        default:
            self.tipo = .none
        }
        
        self.links = [[String : Any]]()
        
        if model.enlace1Titulo != ""{
            let enlace = ["title" : model.enlace1Titulo, "link" : model.enlace1Link, "image" : model.enlace1Imagen]
            self.links.append(enlace)
        }
        
        if model.enlace2Titulo != ""{
            let enlace = ["title" : model.enlace2Titulo, "link" : model.enlace2Link, "image" : model.enlace2Imagen]
            self.links.append(enlace)
        }
        
        if model.enlace3Titulo != ""{
            let enlace = ["title" : model.enlace3Titulo, "link" : model.enlace3Link, "image" : model.enlace3Imagen]
            self.links.append(enlace)
        }
    }
    
    public var id : Int
    public var tipo : HomeItemType
    public var orden : Int
    public var categoriaNoticias : String
    public var categoriaEventos : String
    public var activoLogueado : Bool
    public var activoNoLogueado : Bool
    public var links : [[String : Any]]

    private let NEWS_TYPE = "enum.tipo_tarjeta.noticia"
    private let MESSAGES_TYPE = "enum.tipo_tarjeta.buzon"
    private let PROFILE_TYPE = "enum.tipo_tarjeta.mi_perfil"
    private let EVENTS_TYPE = "enum.tipo_tarjeta.eventos"
    private let ONE_LINK_TYPE = "enum.tipo_tarjeta.enlace_unico"
    private let THREE_LINKS_TYPE = "enum.tipo_tarjeta.tres_enlaces"
}

public enum HomeItemType {
    case news
    case messages
    case profile
    case events
    case oneLink
    case threeLinks
    case none
    
}
