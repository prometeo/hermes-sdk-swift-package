//
//  HermesHome.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 19/5/22.
//

import Foundation

public class HermesHome {
    
    private var appToken : String!
    private var entityId : String!
    private var entityToken : String!
    
    public init() {

        self.appToken = APP_TOKEN
        self.entityId = ENTITY_ID
        self.entityToken = ENTITY_TOKEN

    }
    
    public func get() -> [HomeItem] {
        
        let homeItems = DataBaseManager.shared.getObjects(type: HomeItemModel.self).sorted{$0.orden < $1.orden}.map{HomeItem(model: $0)}
        
        return homeItems
    }
    
    public func update(completion: ((UpdateResult) -> ())?) {
        
        let request = HomeRequest()
        
        request.download(appToken: self.appToken, entityToken: self.entityToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: HomeItemModel.self)
                        
                        var homeItems = [HomeItemModel]()
                        
                        for element in results{
                            let homeItem = HomeItemModel(info: element)
                            
                            homeItems.append(homeItem)
                        }
                        
                        DataBaseManager.shared.save(objects: homeItems)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
    }
    
}
