//
//  ContentOwnersRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 06/11/2020.
//

import Foundation

class AllContentOwnersRequest{
    
    private var totalResults = [[String : Any]]()

    func download(appToken: String, entityToken: String, maxResults: Int, firstResult: Int, rol: String?, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        if firstResult == 0{
            self.totalResults = [[String : Any]]()
        }
        
        self.getPage(appToken: appToken, entityToken: entityToken, maxResults: maxResults, firstResult: firstResult, rol: rol, completion: {(elements) -> () in
            
            if elements.count == 0{
                completion(self.totalResults, .correct)
            }
            else{
                self.totalResults.append(contentsOf: elements)
                                
                self.download(appToken: appToken, entityToken: entityToken, maxResults: maxResults, firstResult: firstResult + maxResults, rol: rol, completion: completion)
            }
            
        })
        
    }
    
    private func getPage(appToken: String, entityToken: String, maxResults: Int, firstResult: Int, rol: String?, completion: (@escaping([[String : Any]]) -> ())){
        
        var url = ALL_CONTENT_OWNERS_ENDPOINT + "&$maxResults=" + String(maxResults) + "&$firstResult=" + String(firstResult)
        
        if rol != nil{
            url += "&$filter[]=roles like '%" + rol! + "%'"
        }
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "entidadToken" : entityToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results); return}
            guard responseCode == .success else {completion(results); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results); return}
            
            results = jsonData
            
            completion(results)
            
            
        })
        
    }
    
}
