//
//  HermesContentOwners.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 06/11/2020.
//

import Foundation

public class HermesAllContentOwners : HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    private var entityToken : String!
    private var rol : String?
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        self.entityToken = ENTITY_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func rol(_ rol : String) -> HermesAllContentOwners {
        
        self.rol = rol
        
        return self
    }
    
    public func get() -> [HermesObject] {
        
        let contentOwners = DataBaseManager.shared.getObjects(type: AllContentOwnerModel.self).map{AllContentOwner(model: $0)}
        
        return contentOwners
    }
    
    private var pendingContentOwners = 0
    
    public func update(completion: ((UpdateResult) -> ())?) {
        

                
        let request = AllContentOwnersRequest()
        
        request.download(appToken: self.appToken, entityToken: self.entityToken, maxResults: 100, firstResult: 0, rol: self.rol, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: AllContentOwnerModel.self)
                        
                        var contentOwners = [AllContentOwnerModel]()
                        
                        for element in results{
                            let contentOwner = AllContentOwnerModel(info: element)
                            
                            contentOwners.append(contentOwner)
                        }
                        
                        DataBaseManager.shared.save(objects: contentOwners)
                        
                        if completion != nil{
                            completion!(.correct)
                        }
                        

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
    }
    
}
