//
//  TutorialItem.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 18/5/22.
//

import Foundation

public class TutorialItem : HermesObject {
        
    internal init(model: TutorialItemModel) {
        self.id = model.id
        self.orden = model.orden
        self.titulo = model.titulo
        self.activo = model.activo
        self.imagen = model.imagen
        self.esPublico = model.esPublico
    }
    
    public var id : Int
    public var orden : Int
    public var titulo : String
    public var activo : Bool
    public var imagen : String
    public var esPublico : Bool
    
}

