//
//  HermesTutorial.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 18/5/22.
//

import Foundation

public class HermesTutorial {
    
    private var appToken : String!
    private var entityId : String!
    private var entityToken : String!
    
    public init() {

        self.appToken = APP_TOKEN
        self.entityId = ENTITY_ID
        self.entityToken = ENTITY_TOKEN

    }
    
    public func get() -> [TutorialItem] {
        
        let menuItems = DataBaseManager.shared.getObjects(type: TutorialItemModel.self).filter{$0.activo == true}.sorted{$0.orden < $1.orden}.map{TutorialItem(model: $0)}
        
        return menuItems
    }
    
    public func update(completion: ((UpdateResult) -> ())?) {
        
        let request = TutorialRequest()
        
        request.download(appToken: self.appToken, entityToken: self.entityToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: TutorialItemModel.self)
                        
                        var tutorialItems = [TutorialItemModel]()
                        
                        for element in results{
                            let tutorialItem = TutorialItemModel(info: element)
                            
                            tutorialItems.append(tutorialItem)
                        }
                        
                        DataBaseManager.shared.save(objects: tutorialItems)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
    }
    
}
