//
//  TutorialModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 18/5/22.
//

import Foundation
import RealmSwift

class TutorialItemModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var orden : Int  = 0
    @objc dynamic var titulo : String = ""
    @objc dynamic var imagen : String = ""
    @objc dynamic var activo : Bool = false
    @objc dynamic var esPublico : Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.orden = info["orden"] as? Int ?? 0
        self.titulo = info["titulo"] as? String ?? ""
        self.activo = info["activo"] as? Bool ?? false
        self.imagen = info["imagen"] as? String ?? ""
        self.esPublico = info["is_publico"] as? Bool ?? false
        
        self.imagen = self.imagen.replacingOccurrences(of: "/uploads/images/photos//uploads/images/photos/", with: "/uploads/images/photos/")
    }
    
}
