//
//  TutorialRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 18/5/22.
//

import Foundation

let TUTORIAL_ALIAS = "__TUTORIAL"

class TutorialRequest {
    
    func download(appToken: String, entityToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = CMS_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "entidadToken" : entityToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            
            if result.contains("OBJECT_NOT_EXIST"){
                completion(results, .correct)
                
                return
            }
            
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            for element in jsonData{
                let categories = element["categorias"] as? [[String : Any]] ?? [[String : Any]]()
                
                for category in categories {
                    let alias = category["alias"] as? String ?? ""
                    
                    if alias == TUTORIAL_ALIAS{
                        results.append(element)
                    }
                }
            }
            
            completion(results, .correct)
            
            
        })
        
    }
}
