//
//  ResponseAppointment.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 11/11/2020.
//

import Foundation
import UIKit

public class ResponseAppointment : HermesObject {
        
    internal init(model: ResponseAppointmentModel) {
        self.id = model.id
        self.nombre = model.nombre
        self.estado = model.estado
    }
    
    public var id : Int
    public var nombre : String
    public var estado : String
  
}
