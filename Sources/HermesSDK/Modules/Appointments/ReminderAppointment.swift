//
//  ReminderAppointment.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 11/11/2020.
//

import Foundation
import UIKit

public class ReminderAppointment : HermesObject {
        
    internal init(model: ReminderAppointmentModel) {
        self.id = model.id
        self.antelacion = model.antelacion
        self.activo = model.activo
    }
    
    public var id : Int
    public var antelacion : Int
    public var activo : Bool
  
    public func cambiarEstado(nuevoEstado : Bool){
        
        let models = DataBaseManager.shared.getObjects(type: ReminderAppointmentModel.self).filter{$0.id == self.id}
        
        guard let model = models.first else {return}
        
        DataBaseManager.shared.startUpdate()
        model.activo = nuevoEstado
        DataBaseManager.shared.endUpdate()
        
        self.activo = nuevoEstado
    }
}
