//
//  AppointmentModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 11/11/2020.
//

import Foundation
import RealmSwift

class AppointmentModel: Object {
    
    @objc dynamic var id : Int = 0
    @objc dynamic var fecha : String = ""
    @objc dynamic var estado : String  = ""
    @objc dynamic var tipo : String  = ""
    @objc dynamic var color : String  = ""
    @objc dynamic var colorTxt : String  = ""
    @objc dynamic var idPropietarioContenido : Int = 0
    @objc dynamic var nombrePropietarioContenido : String  = ""
    @objc dynamic var apellidosPropietarioContenido : String  = ""
    @objc dynamic var aliasPropietarioContenido : String  = ""
    @objc dynamic var imagenPropietarioContenido : String  = ""
    @objc dynamic var imagenEntidad : String  = ""
    @objc dynamic var webEntidad : String  = ""
    @objc dynamic var nombreEntidad : String  = ""
    @objc dynamic var nombreRecurso : String  = ""
    @objc dynamic var direccionRecurso : String  = ""
    @objc dynamic var correoRecurso : String  = ""
    @objc dynamic var tipoRecurso : String  = ""
    @objc dynamic var idCentro : Int = 0
    @objc dynamic var nombreCentro : String  = ""
    @objc dynamic var direccionCentro : String  = ""
    @objc dynamic var telefonoCentro : String  = ""
    @objc dynamic var idRespuesta : Int = 0
    @objc dynamic var nombreRespuesta : String  = ""
    @objc dynamic var estadoRespuesta : String  = ""
    @objc dynamic var comentarioPublico : String  = ""
    
    let avisos = List<ReminderAppointmentModel>()
    let posiblesRespuestas = List<ResponseAppointmentModel>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.fecha = info["fecha_cita"] as? String ?? ""
        
        self.tipo = info["tipo_cita_txt"] as? String ?? ""
        self.color = info["color"] as? String ?? ""
        self.colorTxt = info["color_txt"] as? String ?? ""
        self.comentarioPublico = info["comentario_publico"] as? String ?? ""
        
        if let estado = info["estado"] as? [String : Any]{
            self.estado = estado["key"] as? String ?? ""
        }
        
        if let paciente = info["paciente"] as? [String : Any]{
            self.idPropietarioContenido = paciente["id"] as? Int ?? 0
            self.nombrePropietarioContenido = paciente["nombre"] as? String ?? ""
            self.apellidosPropietarioContenido = paciente["apellidos"] as? String ?? ""
            self.imagenPropietarioContenido = paciente["image_route"] as? String ?? ""
            self.imagenEntidad = paciente["image_entidad"] as? String ?? ""
            self.webEntidad = paciente["paginaweb_entidad"] as? String ?? ""
            
            let nombreCompleto = self.nombrePropietarioContenido + " " + self.apellidosPropietarioContenido
            self.aliasPropietarioContenido = nombreCompleto.getAcronyms()
        }
        
        if let destino = info["destino"] as? [String : Any]{
            self.nombreRecurso = destino["nombre"] as? String ?? ""
        }
        
        if let entidad = info["entidad"] as? [String : Any]{
            self.nombreEntidad = entidad["nombre"] as? String ?? ""
        }
        
        if let centros = info["centros"] as? [[String : Any]], let centro = centros.first{
            self.idCentro = centro["id"] as? Int ?? 0
            self.nombreCentro = centro["nombre"] as? String ?? ""
            self.direccionCentro = centro["direccion"] as? String ?? ""
            self.telefonoCentro = centro["telefono"] as? String ?? ""
        }
        
        if let respuesta = info["respuesta"] as? [String : Any]{
            self.idRespuesta = respuesta["id"] as? Int ?? 0
            self.nombreRespuesta = respuesta["nombre"] as? String ?? ""
            self.estadoRespuesta = respuesta["estado"] as? String ?? ""
        }
        
        if let avisos = info["aviso"] as? [[String : Any]]{
            for aviso in avisos{
                let avisoCita = ReminderAppointmentModel(info: aviso)
                self.avisos.append(avisoCita)
            }
        }
        
        if let respuestas = info["posibles_respuestas"] as? [[String : Any]]{
            for respuesta in respuestas{
                let respuestaCita = ResponseAppointmentModel(info: respuesta)
                self.posiblesRespuestas.append(respuestaCita)
            }
        }
        
    }
    
    
}
