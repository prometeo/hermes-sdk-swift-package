//
//  AppointmentsRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 11/11/2020.
//

import Foundation

class AppointmentsRequest{
    
    func download(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = APPOINTMENTS_ENDPOINT + "?$locale=" + LOCALE
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            
            if result.contains("OBJECT_NOT_EXIST"){
                completion(results, .correct)
                
                return
            }
            
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
    
    func changeStatus(appToken: String, userToken: String, appointmentId: String, status: AppointmentStatus, completion: (@escaping(RequestResult) -> ())){
        
        let url = CHANGE_APPOINTMENT_STATE_ENDPOINT + String(appointmentId)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "PATCH")
        
        var statusId = 3
        
        switch status {
        case .opened:
            statusId = 4
        case .received:
            statusId = 3
        case .sent:
            statusId = 2
        case .answered:
            statusId = 5
        }
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "estado[id]", value: String(statusId)))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            completion(.correct)
            return
            

        })
        
        
    }
    
    func changeResponse(appToken: String, userToken: String, appointmentId: String, responseId: String, completion: (@escaping(RequestResult) -> ())){
        
        let url = CHANGE_APPOINTMENT_STATE_ENDPOINT + String(appointmentId)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "PATCH")
        
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "respuesta[id]", value: String(responseId)))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            completion(.correct)
            return
            

        })
        
    }
    
    func changeReminder(userToken: String, appointmentId: String, reminder: ReminderAppointment, completion: (@escaping(RequestResult) -> ())){
        
        let url = CHANGE_APPOINTMENT_REMINDER_ENDPOINT.replacingOccurrences(of: "$0", with: appointmentId).replacingOccurrences(of: "$1", with: String(reminder.id))
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded"]
        
        let request = Request(endpoint: url, method: "POST")
                
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "userToken", value: userToken))
        
        if reminder.activo == true{
            bodyCompontents.queryItems!.append(URLQueryItem(name: "activo", value: "1"))
        }
        else{
            bodyCompontents.queryItems!.append(URLQueryItem(name: "activo", value: "0"))
        }
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(.error); return}
            
            guard let str = jsonData["str"] as? String else {completion(.error); return}
            
            if str == "OK"{
                
                completion(.correct)
                return
            }
            else{
                completion(.error)
                return
            }
            

        })
        
    }
}
