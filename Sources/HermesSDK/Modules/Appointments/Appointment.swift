//
//  Appointment.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 11/11/2020.
//

import Foundation
import UIKit

public class Appointment : HermesObject {
        
    internal init(model: AppointmentModel) {
        self.id = model.id
        
        if let fechaDate = Formatter.stringToDate(string: model.fecha, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            self.fecha = fechaDate
        }
        else{
            self.fecha = Date()
        }
        
        self.estado = AppointmentStatus(rawValue: model.estado) ?? .sent
        self.tipo = model.tipo
        self.color = model.color
        self.colorTxt = model.colorTxt
        self.comentarioPublico = model.comentarioPublico
        self.idPropietarioContenido = model.idPropietarioContenido
        
        if let propietarioContenido = DataBaseManager.shared.getObjects(type: ContentOwnerModel.self).filter({$0.id == model.idPropietarioContenido}).first{
            
            self.nombrePropietarioContenido = propietarioContenido.nombre
            self.apellidosPropietarioContenido = propietarioContenido.apellidos
            self.aliasPropietarioContenido = propietarioContenido.alias
            self.imagenPropietarioContenido = propietarioContenido.imagen
            self.imagenEntidad = propietarioContenido.imagenEntidad
            self.webEntidad = propietarioContenido.webEntidad
            self.nombreEntidad = propietarioContenido.nombreEntidad
        }
        else{
            self.nombrePropietarioContenido = ""
            self.apellidosPropietarioContenido = ""
            self.aliasPropietarioContenido = ""
            self.imagenPropietarioContenido = ""
            self.imagenEntidad = ""
            self.webEntidad = ""
            self.nombreEntidad = ""
        }
        
        self.nombreRecurso = model.nombreRecurso
        self.direccionRecurso = model.direccionRecurso
        self.correoRecurso = model.correoRecurso
        self.tipoRecurso = model.tipoRecurso
        self.idCentro = model.idCentro
        self.nombreCentro = model.nombreCentro
        self.direccionCentro = model.direccionCentro
        self.telefonoCentro = model.telefonoCentro
        self.idRespuesta = model.idRespuesta
        self.nombreRespuesta = model.nombreRespuesta
        self.estadoRespuesta = model.estadoRespuesta
        self.avisos = model.avisos.map{ReminderAppointment(model: $0)}
        self.posiblesRespuestas = model.posiblesRespuestas.map{ResponseAppointment(model: $0)}
    }
    
    public var id : Int
    
    public func fecha(formato: String) -> String{
        return Formatter.dateToString(date: self.fecha, format: formato)
    }
    
    public var fecha : Date
    
    public var estado : AppointmentStatus
    public var tipo : String
    public var color : String
    public var colorTxt : String
    public var comentarioPublico : String
    public var idPropietarioContenido : Int
    public var nombrePropietarioContenido : String
    public var apellidosPropietarioContenido : String
    public var aliasPropietarioContenido : String
    public var imagenPropietarioContenido : String
    public var imagenEntidad : String
    public var webEntidad : String
    public var nombreEntidad : String
    public var nombreRecurso : String
    public var direccionRecurso : String
    public var correoRecurso : String
    public var tipoRecurso : String
    public var idCentro : Int
    public var nombreCentro : String
    public var direccionCentro : String
    public var telefonoCentro : String
    public var idRespuesta : Int
    public var nombreRespuesta : String
    public var estadoRespuesta : String
    public var avisos : [ReminderAppointment]
    public var posiblesRespuestas : [ResponseAppointment]
    
    public var infoPropietarioVisible : Bool {
        
        let propietarios = DataBaseManager.shared.getObjects(type: ContentOwnerModel.self)
        
        if propietarios.count > 1{
            return true
        }
        else{
            return false
        }
    }
    
    public var entityImageVisible : Bool {
        
        let entidades = DataBaseManager.shared.getObjects(type: EntityModel.self)
        
        if entidades.count > 1{
            return true
        }
        else{
            return false
        }
        
    }
    
    public func cambiarEstado(nuevoEstado: AppointmentStatus){
        
        let models = DataBaseManager.shared.getObjects(type: AppointmentModel.self).filter{$0.id == self.id}
        
        guard let model = models.first else {return}
        
        DataBaseManager.shared.startUpdate()
        model.estado = nuevoEstado.rawValue
        DataBaseManager.shared.endUpdate()
          
    }
    
    public func cambiarRespuesta(nuevaRespuesta: ResponseAppointment){
        
        let models = DataBaseManager.shared.getObjects(type: AppointmentModel.self).filter{$0.id == self.id}
        
        guard let model = models.first else {return}
        
        DataBaseManager.shared.startUpdate()
        model.idRespuesta = nuevaRespuesta.id
        model.nombreRespuesta = nuevaRespuesta.nombre
        model.estadoRespuesta = nuevaRespuesta.estado
        DataBaseManager.shared.endUpdate()
          
    }
}

public enum AppointmentStatus : String {
    case sent = "ENVIADO"
    case received = "RECIBIDO"
    case opened = "ABIERTO"
    case answered = "CONTESTADO"
}
