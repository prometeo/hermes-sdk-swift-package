//
//  HermesAppointments.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 11/11/2020.
//

import Foundation
import UserNotifications

public class HermesAppointments : HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        
        let appointments = DataBaseManager.shared.getObjects(type: AppointmentModel.self).map{Appointment(model: $0)}
        
        return appointments
    }
    
    public func update(completion: ((UpdateResult) -> ())?) {
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = AppointmentsRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: AppointmentModel.self)
                        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
                        
                        var appointments = [AppointmentModel]()
                        
                        for element in results{
                            let appointment = AppointmentModel(info: element)
                            
                            appointments.append(appointment)
                            
                            for reminder in appointment.avisos{
                                self.setupReminder(reminderModel: reminder, appoinmentModel: appointment)
                            }
                        }
                        
                        DataBaseManager.shared.save(objects: appointments)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
    }
    
    public func changeStatus(appointment: Appointment, newStatus: AppointmentStatus){
        
        guard let userToken = self.userToken else {return}
        
        appointment.cambiarEstado(nuevoEstado: newStatus)
        
        let request = AppointmentsRequest()
        
        request.changeStatus(appToken: self.appToken, userToken: userToken, appointmentId: String(appointment.id), status: newStatus, completion: {(requestResult) -> () in
            
            if requestResult == .correct{
                print("APPOINTMENT STATUS CHANGED TO: " + newStatus.rawValue)
            }
            else{
                print("ERROR CHANGE APPOINTMENT STATUS")
            }
        })
        
    }
    
    public func changeResponse(appointment: Appointment, newResponse: ResponseAppointment){
        
        guard let userToken = self.userToken else {return}
        
        appointment.cambiarRespuesta(nuevaRespuesta: newResponse)
 
        let request = AppointmentsRequest()
        
        request.changeResponse(appToken: self.appToken, userToken: userToken, appointmentId: String(appointment.id), responseId: String(newResponse.id), completion: {(requestResult) -> () in
            
            if requestResult == .correct{
                print("APPOINTMENT RESPONSE CHANGED")
            }
            else{
                print("ERROR CHANGE APPOINTMENT RESPONSE")
            }
        })
        
    }
    
    public func accept(appointment: Appointment){
        
        guard let userToken = self.userToken else {return}
         
        let request = AppointmentsRequest()
        
        request.changeResponse(appToken: self.appToken, userToken: userToken, appointmentId: String(appointment.id), responseId: "29", completion: {(requestResult) -> () in
            
            if requestResult == .correct{
                print("APPOINTMENT RESPONSE CHANGED")
            }
            else{
                print("ERROR CHANGE APPOINTMENT RESPONSE")
            }
        })
        
    }
    
    public func cancel(appointment: Appointment){
        
        guard let userToken = self.userToken else {return}
         
        let request = AppointmentsRequest()
        
        request.changeResponse(appToken: self.appToken, userToken: userToken, appointmentId: String(appointment.id), responseId: "30", completion: {(requestResult) -> () in
            
            if requestResult == .correct{
                print("APPOINTMENT RESPONSE CHANGED")
            }
            else{
                print("ERROR CHANGE APPOINTMENT RESPONSE")
            }
        })
        
    }
    
    public func changeReminder(appointment: Appointment, reminder: ReminderAppointment){
        
        guard let userToken = self.userToken else {return}
                
        let request = AppointmentsRequest()
        
        if reminder.activo == true{
            self.planNotification(reminder: reminder, appointment: appointment)
        }
        else{
            self.removeNotification(reminder: reminder)
        }
        
        request.changeReminder(userToken: userToken, appointmentId: String(appointment.id), reminder: reminder, completion: {(requestResult) -> () in
            
            if requestResult == .correct{
                print("APPOINTMENT REMINDER CHANGED")
            }
            else{
                print("ERROR CHANGE APPOINTMENT RESPONSE")
            }
        })
        
    }
    
    private func planNotification(reminder: ReminderAppointment, appointment: Appointment){
        
        guard let notDate = Calendar.current.date(byAdding: .hour, value: (-1 * reminder.antelacion), to: appointment.fecha) else {return}
        
        let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second,], from: notDate)
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
        
        
        let content = UNMutableNotificationContent()
        
        let date = appointment.fecha(formato: "dd/MM/yyyy")
        let time = appointment.fecha(formato: "HH:mm")
        
        content.title = "Recordatorio"
        content.body = "Cita con " + appointment.nombreRecurso + " el " + date + " a las " + time
        
        content.sound = UNNotificationSound.default
        
        let identifier = String(reminder.id)
        
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
    }
    

    private func removeNotification(reminder: ReminderAppointment){
        
        let id = String(reminder.id)
    
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [id])
    }
    
    private func setupReminder(reminderModel: ReminderAppointmentModel, appoinmentModel: AppointmentModel){
        
        let reminder = ReminderAppointment(model: reminderModel)
        let appointment = Appointment(model: appoinmentModel)
        
        self.removeNotification(reminder: reminder)
        
        if reminder.activo == true{
            self.planNotification(reminder: reminder, appointment: appointment)
        }
    }
}
