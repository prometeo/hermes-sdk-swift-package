//
//  ReminderAppointmentModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 11/11/2020.
//

import Foundation
import RealmSwift

class ReminderAppointmentModel: Object {
    
    @objc dynamic var id : Int = 0
    @objc dynamic var antelacion : Int = 0
    @objc dynamic var activo : Bool  = false

    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.antelacion = info["antelacion"] as? Int ?? 0
        self.activo = info["activo"] as? Bool ?? false
        
    }
    
    
}
