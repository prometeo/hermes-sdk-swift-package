//
//  HermesSentForms.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 12/11/2020.
//

import Foundation

public class HermesSentForms: HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        let files = DataBaseManager.shared.getObjects(type: SentFormModel.self).sorted{$0.fecha > $1.fecha}.map{SentForm(model: $0)}
        
        return files
    }
    
    public func getContentOwners() -> [ContentOwner] {
        
        let contentOwners = DataBaseManager.shared.getObjects(type: ContentOwnerModel.self).map{ContentOwner(model: $0)}
        
        return contentOwners
    }
    
    public func update(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = SentFormsRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            if requestResult == .correct{
                
                DispatchQueue.main.async {
                    
                    DataBaseManager.shared.deleteObjects(type: SentFormModel.self)
                    
                    var forms = [SentFormModel]()
                    
                    for element in results{
                        let form = SentFormModel(info: element)
                        
                        forms.append(form)
                    }
                    
                    DataBaseManager.shared.save(objects: forms)
                    
                    if completion != nil{
                        completion!(.correct)
                    }

                }
                
            }
            else{
                if completion != nil{
                    completion!(.error)
                }
            }
            
            
        })
 
    
    }
    
}
