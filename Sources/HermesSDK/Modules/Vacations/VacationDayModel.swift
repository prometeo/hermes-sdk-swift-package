//
//  VacationDayModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 17/10/22.
//

import Foundation
import RealmSwift

class VacationDayModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var fecha : String = ""
    @objc dynamic var tipo : Int = 0
    @objc dynamic var descripcion : String = ""
    @objc dynamic var horaInicio : String = ""
    @objc dynamic var horaFin : String = ""
    @objc dynamic var idPropietarioContenido : Int  = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.descripcion = info["descripcion"] as? String ?? ""
        self.fecha = info["fecha"] as? String ?? ""
        self.tipo = info["vacacion_type"] as? Int ?? 0
        self.horaInicio = info["hora_inicio"] as? String ?? ""
        self.horaFin = info["hora_fin"] as? String ?? ""
        
        if let propietario = info["propietario"] as? [String : Any]{
            self.idPropietarioContenido = propietario["id"] as? Int ?? -1
        }

    }
    
    
    
}
