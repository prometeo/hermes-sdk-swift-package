//
//  VacationDay.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 17/10/22.
//

import Foundation
import UIKit
import RealmSwift

public class VacationDay : HermesObject {
    
    public init() {
        self.id = 0
        self.fecha = ""
        self.tipo = .vacaciones
        self.descripcion = ""
        self.horaInicio = ""
        self.horaFin = ""
        self.idPropietarioContenido = -1
    }
    
    internal init(model: VacationDayModel) {
        self.id = model.id
        self.fecha = model.fecha
        self.tipo = VacationType(rawValue: model.tipo) ?? .vacaciones
        self.descripcion = model.descripcion
        self.horaInicio = model.horaInicio
        self.horaFin = model.horaFin
        self.idPropietarioContenido = model.idPropietarioContenido

    }
    
    public var id : Int
    public var fecha : String
    public var descripcion : String
    public var tipo : VacationType
    public var horaInicio : String
    public var horaFin : String
    public var idPropietarioContenido : Int
    
    public func fecha(formato: String) -> String {
        var resultado = ""
    
        if let fechaDate = Formatter.stringToDate(string: self.fecha, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
    
        return resultado
    }
    
    public func horaInicio(formato: String) -> String {
        var resultado = ""
    
        if let fechaDate = Formatter.stringToDate(string: self.fecha, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
    
        return resultado
    }
    
    public func horaFin(formato: String) -> String {
        var resultado = ""
    
        if let fechaDate = Formatter.stringToDate(string: self.fecha, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
    
        return resultado
    }
}

public enum VacationType : Int {
    case vacaciones = 0
    case dia = 1
    case horas = 2
    case festivo = 3
}
