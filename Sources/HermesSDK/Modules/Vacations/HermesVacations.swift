//
//  HermesVacations.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 17/10/22.
//

import Foundation

public class HermesVacations: HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    public init() {
        self.userToken = nil
        
        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        return DataBaseManager.shared.getObjects(type: VacationDayModel.self).sorted{$0.fecha > $1.fecha}.map{VacationDay(model: $0)}
    }
    
    public func update(completion: ((UpdateResult) -> ())?) {
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = VacationsRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: VacationDayModel.self)
                        
                        var vacationItems = [VacationDayModel]()
                        
                        for element in results{
                            let vacationItem = VacationDayModel(info: element)
                            
                            vacationItems.append(vacationItem)
                        }
                        
                        DataBaseManager.shared.save(objects: vacationItems)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
    }
    
    public func saveVacationDay(date: Date, startTime: Date?, endTime: Date?, type: VacationType, description: String, contentOwner: ContentOwner, completion: ((UpdateResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let dateString = Formatter.dateToString(date: date, format: "yyyy-MM-dd'T'HH:mm:ssZ").replacingOccurrences(of: "+", with: "%2B")
        
        var startTimeString = dateString
        var endTimeString = dateString
        
        if startTime != nil{
            startTimeString = Formatter.dateToString(date: startTime, format: "yyyy-MM-dd'T'HH:mm:ssZ").replacingOccurrences(of: "+", with: "%2B")
        }
        
        if endTime != nil{
            endTimeString = Formatter.dateToString(date: endTime, format: "yyyy-MM-dd'T'HH:mm:ssZ").replacingOccurrences(of: "+", with: "%2B")
        }
        
        let request = VacationsRequest()
        
        request.create(appToken: self.appToken, userToken: userToken, date: dateString, startTime: startTimeString, endTime: endTimeString, type: type, description: description, contentOwnerId: String(contentOwner.id), completion: {(data, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    if completion != nil{
                        completion!(.correct)
                    }
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
        
        
        
    }
    
}
