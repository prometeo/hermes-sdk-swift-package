//
//  WeatherRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 23/11/2020.
//

import Foundation

class WeatherRequest : NSObject{
    
    func download(localityId: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = WEATHER_ENDPOINT.replacingOccurrences(of: "{$0}", with: localityId)
        
        let session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        request.timeoutInterval = 30
        
        var results = [[String : Any]]()
        
        let task = session.dataTask(with: request) { (data, response, error) in
            
            guard let data = data else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: data, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            guard let datos = jsonData["datos"] as? String else {completion(results, .error); return}
            
            guard let datosUrl = URL(string: datos) else {completion(results, .error); return}
            
            let datosRequest = URLRequest(url: datosUrl)
            
            let session2 = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
            
            let task2 = session2.dataTask(with: datosRequest) { (data2, response2, error2) in
                
                guard let jsonData2 = data2 else {completion(results, .error); return}
                
                let str = String(decoding: jsonData2, as: UTF8.self)
                
                do {
                    
                    if let datosResult = try JSONSerialization.jsonObject(with: str.data(using: .utf8)!, options:[]) as? [[String:Any]]
                    {
                        guard let primerDato = datosResult.first else {completion(results, .error); return}
                        guard let prediccion = primerDato["prediccion"] as? [String : Any] else {completion(results, .error); return}
                        guard let dia = prediccion["dia"] as? [[String : Any]] else {completion(results, .error); return}
                        
                        results = dia
                        
                        completion(results, .correct)
                        return
                    }
                    
                } catch {
                    print(error.localizedDescription)
                    completion(results, .error)
                    return
                }
                
                
            }
            
            task2.resume()
        }
        
        task.resume()
        
        
        
        
    }
    
    
    
}

extension WeatherRequest : URLSessionDelegate {
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        let urlCredential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
        
        completionHandler(.useCredential, urlCredential)
    }
}
