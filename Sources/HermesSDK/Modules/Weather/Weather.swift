//
//  Weather.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 23/11/2020.
//

import Foundation

public class Weather {
    
    
    internal init(model: WeatherModel) {
        self.iconCodeToday = model.iconCodeToday
        self.iconCodeTomorrow = model.iconCodeTomorrow
        self.tempMinToday = model.tempMinToday
        self.tempMaxToday = model.tempMaxToday
        self.tempMinTomorrow = model.tempMinTomorrow
        self.tempMaxTomorrow = model.tempMaxTomorrow
        self.dateToday = model.dateToday
        self.dateTomorrow = model.dateTomorrow
    }
    
    public var iconCodeToday : String
    public var iconCodeTomorrow : String
    public var tempMinToday : Int
    public var tempMaxToday : Int
    public var tempMinTomorrow : Int
    public var tempMaxTomorrow: Int
    public var dateToday : String
    public var dateTomorrow : String
    
    public func dateToday(formato: String) -> String{
        
        var resultado = ""
        
        if let fechaDate = Formatter.stringToDate(string: self.dateToday, format: "yyyy-MM-ddTHH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
        
        return resultado
    }
    
    public func dateTomorrow(formato: String) -> String{
        
        var resultado = ""
        
        if let fechaDate = Formatter.stringToDate(string: self.dateTomorrow, format: "yyyy-MM-ddTHH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
        
        return resultado
    }

}
