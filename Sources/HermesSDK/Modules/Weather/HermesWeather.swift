//
//  HermesWeather.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 23/11/2020.
//

import Foundation

public class HermesWeather  {
    
    private var localityId : String?
    
    public init() {
  
    }
    
    public func localityId(_ localityId : String) -> HermesWeather {
        
        self.localityId = localityId
        
        return self
    }
    
    public func get() -> Weather? {
        let weather = DataBaseManager.shared.getObjects(type: WeatherModel.self).map{Weather(model: $0)}
        
        return weather.first
    }
    
    public func update(completion: ((UpdateResult) -> ())?){
        
        
        guard let localityId = self.localityId else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.error)
                }
                
            }
            return
        }
                
        let request = WeatherRequest()
        
        request.download(localityId: localityId, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: WeatherModel.self)
                        
                        let wheater = WeatherModel(info: results)
                        
                        DataBaseManager.shared.save(object: wheater)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
}
