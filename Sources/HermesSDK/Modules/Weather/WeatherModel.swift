//
//  WeatherModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 23/11/2020.
//

import Foundation
import RealmSwift

class WeatherModel: Object {
    
    @objc dynamic var iconCodeToday : String = ""
    @objc dynamic var iconCodeTomorrow : String = ""
    @objc dynamic var tempMinToday : Int  = 0
    @objc dynamic var tempMaxToday : Int  = 0
    @objc dynamic var tempMinTomorrow : Int  = 0
    @objc dynamic var tempMaxTomorrow : Int  = 0
    @objc dynamic var dateToday: String  = ""
    @objc dynamic var dateTomorrow: String  = ""

        
    override static func primaryKey() -> String? {
        return "iconCodeToday"
    }
    
    override required init() {
        
    }
    
    init(info: [[String : Any]]) {
        
        guard info.count >= 2 else {return}
        
        let today = info[0]
        let tomorrow = info[1]
        
        //TODAY
        if let statusToday = today["estadoCielo"] as? [[String : Any]]{
            
            for status in statusToday{
                guard let value = status["value"] as? String else {continue}
                
                if value != ""{
                    self.iconCodeToday = value
                    break
                }
            }
        }
        
        if let tempToday = today["temperatura"] as? [String : Any]{
            if let max = tempToday["maxima"] as? Int{
                self.tempMaxToday = max
            }
            
            if let min = tempToday["minima"] as? Int{
                self.tempMinToday = min
            }
        }
        
        if let date = today["fecha"] as? String{
            self.dateToday = date
        }
        
        //TOMORROW
        if let statusTomorrow = tomorrow["estadoCielo"] as? [[String : Any]]{
            
            for status in statusTomorrow{
                guard let value = status["value"] as? String else {continue}
                
                if value != ""{
                    self.iconCodeTomorrow = value
                    break
                }
            }
        }
        
        if let tempTomorrow = tomorrow["temperatura"] as? [String : Any]{
            if let max = tempTomorrow["maxima"] as? Int{
                self.tempMaxTomorrow = max
            }
            
            if let min = tempTomorrow["minima"] as? Int{
                self.tempMinTomorrow = min
            }
        }
        
        if let date = tomorrow["fecha"] as? String{
            self.dateTomorrow = date
        }
    }
    
    
}
