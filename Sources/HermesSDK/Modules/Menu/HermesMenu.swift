//
//  HermesMenu.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 06/11/2020.
//

import Foundation

public class HermesMenu {
    
    private var appToken : String!
    private var entityId : String!
    private var entityToken : String!
    
    public init() {

        self.appToken = APP_TOKEN
        self.entityId = ENTITY_ID
        self.entityToken = ENTITY_TOKEN

    }
    
    public func get() -> [MenuItem] {
        
        let menuItems = DataBaseManager.shared.getObjects(type: MenuItemModel.self).filter{$0.activo == true}.filter{$0.padre == -1}.sorted{$0.orden < $1.orden}.map{MenuItem(model: $0)}
        
        return menuItems
    }
    
    public func getByCategory(id: Int) -> [MenuItem] {
        
        let menuItems = DataBaseManager.shared.getObjects(type: MenuItemModel.self).filter{$0.activo == true}.filter{$0.padre == -1 && $0.tipos.contains(String(id))}.sorted{$0.orden < $1.orden}.map{MenuItem(model: $0)}
        
        return menuItems
    }
    
    public func getItemBy(name: String) -> MenuItem? {
        
        let menuItems = DataBaseManager.shared.getObjects(type: MenuItemModel.self).filter{$0.activo == true}.filter{$0.nombre == name}.map{MenuItem(model: $0)}
        
        return menuItems.first
        
    }
    
    public func getItemBy(module: String) -> MenuItem? {
        
        let menuItems = DataBaseManager.shared.getObjects(type: MenuItemModel.self).filter{$0.activo == true}.filter{$0.modulo == module}.map{MenuItem(model: $0)}
        
        return menuItems.first
        
    }
    
    public func update(completion: ((UpdateResult) -> ())?) {
        
        let request = MenuRequest()
        
        request.download(appToken: self.appToken, entityToken: self.entityToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: MenuItemModel.self)
                        
                        var menuItems = [MenuItemModel]()
                        
                        for element in results{
                            let menuItem = MenuItemModel(info: element)
                            
                            menuItems.append(menuItem)
                        }
                        
                        DataBaseManager.shared.save(objects: menuItems)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
    }
    
    public func loadFrom(file: URL){
        
        guard let data = try? Data(contentsOf: file) else {return}
        guard let jsonData = try? JSONSerialization.jsonObject(with: data, options:[]) as? [String:Any] else {return}
        guard let info = jsonData["menu_mecitas"] as? [[String : Any]] else {return}

        DataBaseManager.shared.deleteObjects(type: MenuItemModel.self)
        
        var menuItems = [MenuItemModel]()
        
        for element in info{
            let menuItem = MenuItemModel(local: element)
            
            menuItems.append(menuItem)
        }
        
        DataBaseManager.shared.save(objects: menuItems)
    }
    
    public func updateBadges(completion: (@escaping(_ updateResult: UpdateResult) -> ())){
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        guard let user = users.first else {
            DispatchQueue.main.async {
                completion(.missingUserToken)
            }
            return
        }
                
        var lastEventId = -1
        var lastNewsId = -1
        var lastFormId = -1
        
        let events = DataBaseManager.shared.getObjects(type: EventModel.self).sorted{$0.id > $1.id}
        let news = DataBaseManager.shared.getObjects(type: NewsItemModel.self).sorted{$0.id > $1.id}
        let forms = DataBaseManager.shared.getObjects(type: FormModel.self).sorted{$0.id > $1.id}
        
        lastEventId = events.first?.id ?? -1
        lastNewsId = news.first?.id ?? -1
        lastFormId = forms.first?.id ?? -1
        
        let request = MenuRequest()
        let chatsModule = HermesChat()

    
        chatsModule.updateOnlyChats(completion: {(chatsUpdateResult) -> () in
            request.downloadBadges(entityId: self.entityId, userToken: user.userToken, lastEventId: String(lastEventId), lastNewsId: String(lastNewsId), lastFormId: String(lastFormId), completion: {(results, requestResult) -> () in
                
                if requestResult == .correct{
                    DispatchQueue.main.async {
                        let messages = results["mensajes"] as? Int ?? -1
                        let appointments = results["citas"] as? Int ?? -1
                        let events = results["eventos"] as? Int ?? -1
                        let news = results["noticias"] as? Int ?? -1
                        let forms = results["formularios"] as? Int ?? -1
                        let treatments = results["tratamientos"] as? Int ?? -1
                        
                        let menuBadges = MenuBadgesModel()
                        
                        menuBadges.mensajes = messages
                        menuBadges.citas = appointments
                        menuBadges.eventos = events
                        menuBadges.noticias = news
                        menuBadges.formularios = forms
                        menuBadges.tratamientos = treatments
                        
                        let chats = chatsModule.get() as! [Chat]
                        
                        let chatsBadge = chats.filter{$0.hasNewMessages}.count
                        
                        menuBadges.chats = chatsBadge
                        
                        DataBaseManager.shared.update(object: menuBadges)
                        
                        completion(.correct)
                        return
                    }
                }
                else{
                    completion(.error)
                    return
                }
                
                
            })
            
        })
        
        
    }
    public func messagesBadge() -> Int{
        
        let menuBadges = DataBaseManager.shared.getObjects(type: MenuBadgesModel.self)
        
        if let badges = menuBadges.first{
            return badges.mensajes
        }
        
        return 0
    }
    
    public func newsBadge() -> Int{
        
        let menuBadges = DataBaseManager.shared.getObjects(type: MenuBadgesModel.self)
        
        if let badges = menuBadges.first{
            return badges.noticias
        }
        
        return 0
    }
    
    public func eventsBadge() -> Int{
        
        let menuBadges = DataBaseManager.shared.getObjects(type: MenuBadgesModel.self)
        
        if let badges = menuBadges.first{
            return badges.eventos
        }
        
        return 0
    }
    
    public func formsBadge() -> Int{
        
        let menuBadges = DataBaseManager.shared.getObjects(type: MenuBadgesModel.self)
        
        if let badges = menuBadges.first{
            return badges.formularios
        }
        
        return 0
    }
    
    public func appointmentsBadge() -> Int{
        
        let menuBadges = DataBaseManager.shared.getObjects(type: MenuBadgesModel.self)
        
        if let badges = menuBadges.first{
            return badges.citas
        }
        
        return 0
    }
    
    public func treatmentsBadge() -> Int{
        
        let menuBadges = DataBaseManager.shared.getObjects(type: MenuBadgesModel.self)
        
        if let badges = menuBadges.first{
            return badges.tratamientos
        }
        
        return 0
    }
    
    public func chatsBadge() -> Int{
        
        let menuBadges = DataBaseManager.shared.getObjects(type: MenuBadgesModel.self)
        
        if let badges = menuBadges.first{
            return badges.chats
        }
        
        return 0
    }
}
