//
//  MenuItemModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 06/11/2020.
//

import Foundation
import RealmSwift

class MenuItemModel: Object {
    
    @objc dynamic var id : Int = -1
    @objc dynamic var nombre : String = ""
    @objc dynamic var modulo : String = ""
    @objc dynamic var padre : Int = -1
    @objc dynamic var orden : Int  = 0
    @objc dynamic var activo : Bool  = false
    @objc dynamic var publico : Bool  = false
    @objc dynamic var categoria : String = ""
    @objc dynamic var icono : String = ""
    @objc dynamic var tipos : String = ""
    
    let subItems = List<MenuItemModel>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? (Int.random(in: 1..<1000000) * -1)
        self.nombre = info["titulo"] as? String ?? ""
        self.modulo = info["slug"] as? String ?? ""
        self.categoria = info["url"] as? String ?? ""
        self.orden = info["orden"] as? Int ?? 0
        self.activo = info["activo"] as? Bool ?? false
        self.publico = info["is_publico"] as? Bool ?? false
        
        self.icono = info["icon_class"] as? String ?? ""
        
        if let parent = info["parent"] as? [String :Any]{
            self.padre = parent["id"] as? Int ?? -1
        }
        
        var tipos = [String]()
        if let categorias = info["categorias"] as? [[String :Any]]{
            for categoria in categorias{
                let categoriaCMS = CMSCategoryModel(info: categoria)
                tipos.append(String(categoriaCMS.id))
            }
        }
        
        self.tipos = tipos.joined(separator: ",")
    }
    
    init(local: [String : Any]){
        self.id = Int.random(in: 1..<1000000) * -1
        self.nombre = local["nombre"] as? String ?? ""
        self.modulo = local["modulo"] as? String ?? ""
        self.padre = local["padre"] as? Int ?? -1
        self.orden = local["orden"] as? Int ?? 0
        self.activo = local["activo"] as? Bool ?? false
        self.publico = local["publico"] as? Bool ?? false
        self.categoria = local["categoria"] as? String ?? ""
        self.icono = local["icono"] as? String ?? ""
        
        guard let subItems = local["subItems"] as? [[String : Any]] else {return}
        
        for subItem in subItems{
            let subItemModel = MenuItemModel(local: subItem)
            subItemModel.padre = self.id
            
            self.subItems.append(subItemModel)
        }
    }
    
    init(nombre: String, orden: Int, activo: Bool, subItems: [MenuItemModel]){
        self.id = Int.random(in: 1..<1000000) * -1
        self.nombre = nombre
        self.orden = orden
        self.activo = activo
        self.subItems.append(objectsIn: subItems)
    }
    
}
