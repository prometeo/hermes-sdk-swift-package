//
//  MenuItem.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 06/11/2020.
//

import Foundation

public class MenuItem : HermesObject {
        
    private var desplegado = false
    
    internal init(model: MenuItemModel) {
        
        print("MENU ITEM " + model.nombre)
        
        self.subItems = DataBaseManager.shared.getObjects(type: MenuItemModel.self).filter{$0.padre == model.id}.filter{$0.activo == true}.sorted{$0.orden < $1.orden}.map{MenuItem(model: $0)}
        
        if self.subItems.count == 0{
            self.desplegado = true
        }
        else{
            self.desplegado = false
        }
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            if user.anonimo == true && model.modulo.uppercased() == "perfilUsuario".uppercased(){
                self.nombre = "Registrarse"
            }
        }
        
        self.nombre = model.nombre
        
        self.modulo = model.modulo
        self.padre = model.padre
        self.categoria = model.categoria
        self.icono = model.icono
        self.orden = model.orden
        self.activo = model.activo
        self.publico = model.publico
        
        self.users = DataBaseManager.shared.getObjects(type: UserModel.self)
        self.contentOwners = DataBaseManager.shared.getObjects(type: ContentOwnerModel.self)
    }
        
    public var nombre : String
    public var modulo : String
    public var padre : Int
    public var categoria : String
    public var icono : String
    public var subItems : [MenuItem]
    public var orden : Int
    public var activo : Bool
    public var publico : Bool
    
    private var users : [UserModel]
    private var contentOwners : [ContentOwnerModel]

    public var estaDesplegado : Bool {
        return self.desplegado
    }
    
    public func pulsado(accion: (@escaping(String) -> ())){
    
        if self.subItems.count == 0{
            self.desplegado = false
        }
        else{
            self.desplegado = !self.desplegado
        }
        
        var action = self.modulo
        
        if self.publico == false{
            
            
            if let user = users.first {
                if user.anonimo == true{
                    
                    if self.modulo.uppercased() == "perfilUsuario".uppercased(){
                        action = "registrarse"
                    }
                    else{
                        action = "noPublico"
                    }
                    
                }
                else{
                    if contentOwners.count == 0{
                        
                        if self.modulo.uppercased() == "perfilUsuario".uppercased(){
                            action = "perfilUsuario"
                        }
                        else if self.modulo.uppercased() != "desconectar".uppercased(){
                            action = "necesitaAsociar"
                        }
                        
                    }
                }
            }
            else{
                action = "noPublico"
            }
        }
        
        DispatchQueue.main.async {
            accion(action)
        }
    }
    
}
