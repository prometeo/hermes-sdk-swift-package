//
//  MenuBadgesModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 01/12/2020.
//

import Foundation
import RealmSwift

class MenuBadgesModel: Object {
    
    @objc dynamic var id : Int = 0
    @objc dynamic var mensajes : Int  = 0
    @objc dynamic var noticias : Int  = 0
    @objc dynamic var eventos : Int  = 0
    @objc dynamic var formularios : Int  = 0
    @objc dynamic var citas : Int  = 0
    @objc dynamic var tratamientos : Int  = 0
    @objc dynamic var chats : Int  = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
