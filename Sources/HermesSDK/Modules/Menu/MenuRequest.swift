//
//  MenuRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 06/11/2020.
//

import Foundation

class MenuRequest{
    
    func download(appToken: String, entityToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = CMS_ENDPOINT + "&$locale=" + LOCALE
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "entidadToken" : entityToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            for element in jsonData{
                let isMenu = element["es_menu"] as? Bool ?? false
                
                if isMenu == true{
                    results.append(element)
                }
            }
            
            completion(results, .correct)
            
            
        })
        
    }
    
    func downloadBadges(entityId: String, userToken: String, lastEventId: String, lastNewsId: String, lastFormId: String, completion: (@escaping([String : Any], RequestResult) -> ())){
        
        let url = MENU_BADGES_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded"]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "userToken", value: userToken))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "idEntidad", value: entityId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "idUltimoEvento", value: lastEventId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "idUltimoNoticia", value: lastNewsId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "idUltimoFormulario", value: lastFormId))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [String : Any]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            guard let str = jsonData["str"] as? String else {completion(results, .error); return}
            
            if str == "OK"{
                
                guard let info = jsonData["notificaciones"] as? [String : Any] else {completion(results, .error); return}
                
                results = info
                
                completion(results, .correct)
                return
            }
            else{
                completion(results, .error)
                return
            }
            

        })
        
    }
    
}
