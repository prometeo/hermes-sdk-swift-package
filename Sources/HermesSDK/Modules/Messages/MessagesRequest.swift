//
//  MessagesRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 04/11/2020.
//

import Foundation

class MessagesRequest{
    
    func download(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = MESSAGES_ENDPOINT + "&$locale=" + LOCALE
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            
            if result.contains("OBJECT_NOT_EXIST"){
                completion(results, .correct)
                
                return
            }
            
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
    
    func changeStatus(appToken: String, userToken: String, messageId: String, status: MessageStatus, completion: (@escaping(RequestResult) -> ())){
        
        let url = CHANGE_MESSAGE_STATE_ENDPOINT + String(messageId)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "POST")
        
        var statusId = 3
        
        switch status {
        case .opened:
            statusId = 4
        case .received:
            statusId = 3
        case .sent:
            statusId = 2
    
        }
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "estado[id]", value: String(statusId)))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            completion(.correct)
            return
            

        })
        
    }
    
}
