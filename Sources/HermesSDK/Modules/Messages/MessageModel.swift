//
//  MessageModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 04/11/2020.
//

import Foundation
import RealmSwift

class MessageModel: Object {
    
    @objc dynamic var id : Int = 0
    @objc dynamic var fechaEnvio : String = ""
    @objc dynamic var asunto : String = ""
    @objc dynamic var texto : String  = ""
    @objc dynamic var latitud : String  = ""
    @objc dynamic var longitud : String  = ""
    @objc dynamic var radio : Int  = 0
    @objc dynamic var estado : String  = ""
    @objc dynamic var uuid : String  = ""
    @objc dynamic var color : String  = ""
    @objc dynamic var colorTxt : String  = ""
    @objc dynamic var idPropietarioContenido : Int  = 0
    @objc dynamic var aliasPropietarioContenido : String  = ""
    @objc dynamic var nombrePropietarioContenido : String  = ""
    @objc dynamic var apellidosPropietarioContenido : String  = ""
    @objc dynamic var imagenPropietarioContenido : String  = ""
    @objc dynamic var imagenEntidad : String  = ""
    @objc dynamic var webEntidad : String  = ""
    @objc dynamic var entidad : String  = ""
    
    let adjuntos = List<String>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.fechaEnvio = info["fecha_envio"] as? String ?? ""
        self.asunto = info["asunto"] as? String ?? ""
        self.texto = info["mensaje"] as? String ?? ""
        self.radio = info["radio"] as? Int ?? 0
        self.latitud = info["latitud"] as? String ?? ""
        self.longitud = info["longitud"] as? String ?? ""
        self.uuid = "MensajeLibre:" + String(self.id)
        self.color = info["color"] as? String ?? ""
        self.colorTxt = info["color_txt"] as? String ?? ""
        
        if let destinatarios = info["destinatario"] as? [[String : Any]]{
            if let destinatario = destinatarios.first{
                self.idPropietarioContenido = destinatario["id"] as? Int ?? 0
                self.nombrePropietarioContenido = destinatario["nombre"] as? String ?? ""
                self.apellidosPropietarioContenido = destinatario["apellidos"] as? String ?? ""
                self.imagenPropietarioContenido = destinatario["image_route"] as? String ?? ""
                
                if let entidad = destinatario["entidad"] as? [String: Any]{
                    self.imagenEntidad = entidad["image_entidad"] as? String ?? ""
                    self.webEntidad = entidad["paginaweb_entidad"] as? String ?? ""
                    self.entidad = entidad["nombre"] as? String ?? ""
                }
                
                
                let nombreCompleto = self.nombrePropietarioContenido + " " + self.apellidosPropietarioContenido
                self.aliasPropietarioContenido = destinatario["alias"] as? String ?? ""
            }
            
        }
        
        if let estado = info["estado"] as? [String : Any]{
            self.estado = estado["key"] as? String ?? ""
        }
        
        if let adjuntos = info["adjunto"] as? [[String : Any]]{
            for adjunto in adjuntos {
                if let nombreAdjunto = adjunto["adjunto_name"] as? String{
                    self.adjuntos.append(nombreAdjunto)
                }
            }
        }
    }
    
    
}
