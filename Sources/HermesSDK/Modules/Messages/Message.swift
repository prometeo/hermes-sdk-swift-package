//
//  Message.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 04/11/2020.
//

import Foundation
import UIKit
import RealmSwift

public class Message : HermesObject {
        
    internal init(model: MessageModel) {
        self.id = model.id
        self.fechaEnvio = model.fechaEnvio
        self.asunto = model.asunto
        self.textoRaw = model.texto
        self.latitud = model.latitud
        self.longitud = model.longitud
        self.radio = model.radio
        self.estado = MessageStatus(rawValue: model.estado) ?? .sent
        self.uuid = model.uuid
        self.color = model.color
        self.colorTxt = model.colorTxt
        self.idPropietarioContenido = model.idPropietarioContenido
        self.nombrePropietarioContenido = model.nombrePropietarioContenido
        self.apellidosPropietarioContenido = model.apellidosPropietarioContenido
        self.imagenPropietarioContenido = model.imagenPropietarioContenido
        self.imagenEntidad = model.imagenEntidad
        self.webEntidad = model.webEntidad
        self.entidad = model.entidad
        
        let nombreCompleto = model.nombrePropietarioContenido + " " + model.apellidosPropietarioContenido
        self.aliasPropietarioContenido = nombreCompleto.getAcronyms()
        
        self.adjuntos = model.adjuntos
    }
    
    private var textoRaw : String
    
    public var id : Int
    public var fechaEnvio : String
    public func fecha(formato: String) -> String{
        
        var resultado = ""
        
        if let fechaDate = Formatter.stringToDate(string: self.fechaEnvio, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
        
        return resultado
    }
    
    public var asunto : String
    public var texto : String {
        return Formatter.removeHtml(string: self.textoRaw)
    }
    public var textoHtml : NSAttributedString {
        var html = self.textoRaw
        
        if self.adjuntos.count > 0{
            html += "<p>"
                        
            for i in 0 ..< self.adjuntos.count{
                html += "<i class=\"material-icons\">attach_file</i><a href=\"" + self.adjuntos[i] + "\"> Adjunto " + String(i + 1) + "</a><br /><br />"
            }
            
            html += "</p>"
        }
        
        return Formatter.convertHtml(string:html, font: UIFont(name: "Helvetica Neue", size: 18.0)!, color: UIColor.darkGray) ?? NSAttributedString(string: "")
    }
    
    public var latitud : String
    public var longitud : String
    public var radio : Int
    public var estado : MessageStatus
    public var uuid : String
    public var color : String
    public var colorTxt : String
    public var idPropietarioContenido : Int
    public var aliasPropietarioContenido : String
    public var nombrePropietarioContenido : String
    public var apellidosPropietarioContenido : String
    public var imagenPropietarioContenido : String
    public var imagenEntidad : String
    public var webEntidad : String
    public var entidad : String
    public var adjuntos : List<String>
    
    public var infoPropietarioVisible : Bool {
        
        let propietarios = DataBaseManager.shared.getObjects(type: ContentOwnerModel.self)
        
        if propietarios.count > 1{
            return true
        }
        else{
            return false
        }
    }
    
    public var entityImageVisible : Bool {
        
        let entidades = DataBaseManager.shared.getObjects(type: EntityModel.self)
        
        if entidades.count > 1{
            return true
        }
        else{
            return false
        }
        
    }
    
    public func cambiarEstado(nuevoEstado: MessageStatus){
        
        let models = DataBaseManager.shared.getObjects(type: MessageModel.self).filter{$0.id == self.id}
        
        guard let model = models.first else {return}
        
        DataBaseManager.shared.startUpdate()
        model.estado = nuevoEstado.rawValue
        DataBaseManager.shared.endUpdate()
          
    }
    
}

public enum MessageStatus : String {
    case sent = "ENVIADO" //id 2
    case received = "RECIBIDO" //id 3
    case opened = "ABIERTO" //id 4
}
