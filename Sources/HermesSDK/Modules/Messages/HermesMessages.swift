//
//  HermesMessages.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 04/11/2020.
//

import Foundation

public class HermesMessages : HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        let messages = DataBaseManager.shared.getObjects(type: MessageModel.self).sorted{$0.fechaEnvio > $1.fechaEnvio}.map{Message(model: $0)}
        
        return messages
    }
    
    public func update(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = MessagesRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: MessageModel.self)
                        
                        var messages = [MessageModel]()
                        
                        for element in results{
                            let message = MessageModel(info: element)
                            
                            messages.append(message)
                        }
                        
                        DataBaseManager.shared.save(objects: messages)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
    public func changeStatus(message: Message, newStatus: MessageStatus){
        
        guard let userToken = self.userToken else {return}
        
        message.cambiarEstado(nuevoEstado: newStatus)
        
        let request = MessagesRequest()
        
        request.changeStatus(appToken: self.appToken, userToken: userToken, messageId: String(message.id), status: newStatus, completion: {(requestResult) -> () in
            
            if requestResult == .correct{
                print("MESSAGE STATUS CHANGED TO: " + newStatus.rawValue)
            }
            else{
                print("ERROR CHANGE MESSAGE STATUS")
            }
        })
        
    }
    
    public func filter(text: String?, sender: String?, startDate: Date?, endDate: Date?) -> [Message] {
        
        let messages = DataBaseManager.shared.getObjects(type: MessageModel.self)
            .sorted{$0.fechaEnvio > $1.fechaEnvio}
            .filter{
                
                var textResult = true
                var senderResult = true
                var startDateResult = true
                var endDateResult = true
                
                if text != nil{
                    if !$0.texto.uppercased().contains(text!.uppercased()) && !$0.asunto.uppercased().contains(text!.uppercased()){
                        textResult = false
                    }
                }
                
                if sender != nil{
                    if !$0.nombrePropietarioContenido.uppercased().contains(sender!.uppercased()) && !$0.apellidosPropietarioContenido.uppercased().contains(sender!.uppercased()) && !$0.entidad.uppercased().contains(sender!.uppercased()){
                        senderResult = false
                    }
                }
                
                if startDate != nil{
                    
                    if let date = Formatter.stringToDate(string: $0.fechaEnvio, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
                        if !(date >= startDate!){
                            startDateResult = false
                        }
                    }
                    else{
                        startDateResult = false
                    }
                    
                }
                
                if endDate != nil{
                    
                    if let date = Formatter.stringToDate(string: $0.fechaEnvio, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
                        if !(date <= endDate!){
                            endDateResult = false
                        }
                    }
                    else{
                        endDateResult = false
                    }
                    
                }
                
                let result = textResult && senderResult && startDateResult && endDateResult
                
                return result
            }
            .map{Message(model: $0)}
        
        return messages
        
    }
    
}
