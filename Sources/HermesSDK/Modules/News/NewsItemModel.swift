//
//  NewsItemModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 09/11/2020.
//

import Foundation
import RealmSwift

class NewsItemModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var titulo : String = ""
    @objc dynamic var descripcion : String = ""
    @objc dynamic var link : String = ""
    @objc dynamic var categorias : String = ""
    @objc dynamic var imagen : String = ""
    @objc dynamic var fecha : String = ""
    @objc dynamic var fechaModificacion : String = ""
    @objc dynamic var fechaInicioVigencia : String = ""
    @objc dynamic var fechaFinVigencia : String = ""
    @objc dynamic var publica : Bool = false
    @objc dynamic var pago : Bool = false
    @objc dynamic var tags : String = ""
    @objc dynamic var importe : Double = 0
    @objc dynamic var activa : Bool = true
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.titulo = info["titulo"] as? String ?? ""
        self.descripcion = info["descripcion"] as? String ?? ""
        self.link = info["link_noticia"] as? String ?? ""
        
        self.categorias = info["categorias"] as? String ?? ""
        
        self.imagen = info["imagen"] as? String ?? ""
        self.fecha = info["fecha_noticia"] as? String ?? ""
        self.fechaModificacion = info["fecha_modificacion"] as? String ?? ""
        self.fechaInicioVigencia = info["fecha_hora_inicio_vigencia"] as? String ?? ""
        self.fechaFinVigencia = info["fecha_hora_fin_vigencia"] as? String ?? ""
        self.publica = info["is_publico"] as? Bool ?? false
        self.pago = info["is_noticia_pago"] as? Bool ?? false
        self.tags = info["tags"] as? String ?? ""
        self.importe = info["importe_noticia"] as? Double ?? 0
        self.activa = info["publicar"] as? Bool ?? false
        
        var auxCategorias = [String]()
        
        if let categorias = info["categorias"] as? [[String : Any]] {
            for categoria in categorias {
                if let idCategoria = categoria["id"] as? Int {
                    auxCategorias.append(String(idCategoria))
                }
            }
        }
        
        self.categorias = auxCategorias.joined(separator: ",")

        if self.imagen.hasPrefix("http") == false{
            self.imagen = (BASE_URL + self.imagen).replacingOccurrences(of: "//", with: "/")
        }

    }
    
    
}
