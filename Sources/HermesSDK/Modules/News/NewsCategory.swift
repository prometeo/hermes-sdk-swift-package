//
//  NewsCategory.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 09/11/2020.
//

import Foundation
import UIKit

public class NewsCategory : HermesObject {
        
    internal init(model: NewsCategoryModel) {
        self.id = model.id
        self.nombre = model.nombre
        self.activa = model.activa
    }
    
    public var id : Int
    public var nombre : String
    public var activa : Bool
 
}
