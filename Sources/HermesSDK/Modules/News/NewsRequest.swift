//
//  NewsRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 09/11/2020.
//

import Foundation
import UIKit

class NewsRequest{
    
    private var totalResults = [[String : Any]]()
    
    func download(appToken: String, userToken: String, categorias: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = NEWS_ENDPOINT + "?$locale=" + LOCALE
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            
            if result.contains("OBJECT_NOT_EXIST"){
                completion(results, .correct)
                
                return
            }
            
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
    
    func downloadByContentOwner(appToken: String, userToken: String, contentOwnerId: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = NEWS_ENDPOINT + "?$locale=" + LOCALE + "&propietarioemisor=" + contentOwnerId + "&$all"
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            
            if result.contains("OBJECT_NOT_EXIST"){
                completion(results, .correct)
                
                return
            }
            
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
    
    func createNews(appToken: String, userToken: String, categoryId: String, contentOwnerId: String, title: String, description: String, startDate: String, endDate: String, image: UIImage?, publish: String, completion: (@escaping([String : Any], RequestResult) -> ())){
        
        let url = CREATE_NEWS_ENDPOINT
        
        let request = Request(endpoint: url, method: "POST")
        
        var body = Data()
        
        let boundary = "------VohpleBoundary4sadfasdfeafe4w5w45435hwttre"
        let contentType = "multipart/form-data; boundary=" + boundary
        let separator = "--" + boundary + "\r\n"
        let newLine = "\r\n"
        
        let paramNameCategoria = "Content-Disposition: form-data; name=\"categorias[][id]\"\r\n\r\n"
        let paramNameTitle = "Content-Disposition: form-data; name=\"titulo\"\r\n\r\n"
        let paramNameDescription = "Content-Disposition: form-data; name=\"descripcion\"\r\n\r\n"
        let paramNameContentOwner = "Content-Disposition: form-data; name=\"propietarioemisor[id]\"\r\n\r\n"
        let paramNameEmisor = "Content-Disposition: form-data; name=\"emisorTipo\"\r\n\r\n"
        let paramNameStartDate = "Content-Disposition: form-data; name=\"fecha_hora_inicio_vigencia\"\r\n\r\n"
        let paramNameEndDate = "Content-Disposition: form-data; name=\"fecha_hora_fin_vigencia\"\r\n\r\n"
        let paramNamePublish = "Content-Disposition: form-data; name=\"publicar\"\r\n\r\n"
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameCategoria.data(using: .utf8)!)
        body.append(categoryId.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameTitle.data(using: .utf8)!)
        body.append(title.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameDescription.data(using: .utf8)!)
        body.append(description.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameContentOwner.data(using: .utf8)!)
        body.append(contentOwnerId.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameEmisor.data(using: .utf8)!)
        body.append("3".data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameStartDate.data(using: .utf8)!)
        body.append(startDate.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameEndDate.data(using: .utf8)!)
        body.append(endDate.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNamePublish.data(using: .utf8)!)
        body.append(publish.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        let typeImage = "Content-Type:image/jpeg\r\n\r\n"
        
        if let image = image {
            
            if let imageData = image.compress() {
                body.append(separator.data(using: .utf8)!)
                
                let imageField = "Content-Disposition: form-data; name=\"imagenFile\"; filename=\"imagen" + Date().timeIntervalSince1970.description + ".jpg\"\r\n"
                
                body.append(imageField.data(using: .utf8)!)
                body.append(typeImage.data(using: .utf8)!)
                body.append(imageData)
                body.append(newLine.data(using: .utf8)!)
            }
            
            
        }
        
        let end = "--" + boundary + "--\r\n"
        
        body.append(end.data(using: .utf8)!)
        
        let headers : [String : String] = ["Content-Type" : contentType, "appToken" : appToken, "userToken" : userToken]
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [String : Any]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            return
        })
    }
    
    func editNews(appToken: String, userToken: String, newsItemId: String, title: String, description: String, startDate: String, endDate: String, image: UIImage?, publish: String, completion: (@escaping(RequestResult) -> ())){
        
        let url = CREATE_NEWS_ENDPOINT + "/" + newsItemId
        
        let request = Request(endpoint: url, method: "POST")
        
        var body = Data()
        
        let boundary = "------VohpleBoundary4sadfasdfeafe4w5w45435hwttre"
        let contentType = "multipart/form-data; boundary=" + boundary
        let separator = "--" + boundary + "\r\n"
        let newLine = "\r\n"
        
        let paramNameTitle = "Content-Disposition: form-data; name=\"titulo\"\r\n\r\n"
        let paramNameDescription = "Content-Disposition: form-data; name=\"descripcion\"\r\n\r\n"
        let paramNameEmisor = "Content-Disposition: form-data; name=\"emisorTipo\"\r\n\r\n"
        let paramNameStartDate = "Content-Disposition: form-data; name=\"fecha_hora_inicio_vigencia\"\r\n\r\n"
        let paramNameEndDate = "Content-Disposition: form-data; name=\"fecha_hora_fin_vigencia\"\r\n\r\n"
        let paramNamePublish = "Content-Disposition: form-data; name=\"publicar\"\r\n\r\n"
        

        body.append(separator.data(using: .utf8)!)
        body.append(paramNameTitle.data(using: .utf8)!)
        body.append(title.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameDescription.data(using: .utf8)!)
        body.append(description.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameEmisor.data(using: .utf8)!)
        body.append("3".data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameStartDate.data(using: .utf8)!)
        body.append(startDate.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameEndDate.data(using: .utf8)!)
        body.append(endDate.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNamePublish.data(using: .utf8)!)
        body.append(publish.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        
        let typeImage = "Content-Type:image/jpeg\r\n\r\n"
        
        if let image = image {
            
            if let imageData = image.compress() {
                body.append(separator.data(using: .utf8)!)
                
                let imageField = "Content-Disposition: form-data; name=\"imagenFile\"; filename=\"imagen" + Date().timeIntervalSince1970.description + ".jpg\"\r\n"
                
                body.append(imageField.data(using: .utf8)!)
                body.append(typeImage.data(using: .utf8)!)
                body.append(imageData)
                body.append(newLine.data(using: .utf8)!)
            }
            
            
        }
        
        let end = "--" + boundary + "--\r\n"
        
        body.append(end.data(using: .utf8)!)
        
        let headers : [String : String] = ["Content-Type" : contentType, "appToken" : appToken, "userToken" : userToken]
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            completion(.correct)
            return
        })
    }
    
    /*func download(appToken: String, userToken: String, maxResults: Int, firstResult: Int, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        if firstResult == 0{
            self.totalResults = [[String : Any]]()
        }
        
        self.getPage(appToken: appToken, userToken: userToken, maxResults: maxResults, firstResult: firstResult, completion: {(elements) -> () in
            
            if elements.count == 0{
                completion(self.totalResults, .correct)
            }
            else{
                self.totalResults.append(contentsOf: elements)
                                
                self.download(appToken: appToken, userToken: userToken, maxResults: maxResults, firstResult: firstResult + maxResults, completion: completion)
            }
            
        })
        
    }
    
    private func getPage(appToken: String, userToken: String, maxResults: Int, firstResult: Int, completion: (@escaping([[String : Any]]) -> ())){
        
        let url = NEWS_ENDPOINT + "?$maxResults=" + String(maxResults) + "&$firstResult=" + String(firstResult) + "&$locale=" + LOCALE
        
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results); return}
            guard responseCode == .success else {completion(results); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results); return}
            
            results = jsonData
            
            completion(results)
            
            
        })
        
    }*/
    
}
