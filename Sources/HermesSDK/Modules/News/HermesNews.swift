//
//  HermesNews.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 09/11/2020.
//

import Foundation
import UIKit

public class HermesNews: HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    private var categoriesModule = HermesNewsCategories()
    private var filterCategory : String?
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func filterBy(_ category : String) -> HermesNews {
        
        self.filterCategory = category
        
        return self
    }
    
    public func get() -> [HermesObject] {
        let news = DataBaseManager.shared.getObjects(type: NewsItemModel.self)
        .sorted{$0.fecha > $1.fecha}
        .filter{
            let dateStart = Formatter.stringToDate(string: $0.fechaInicioVigencia, format: "yyyy-MM-dd'T'HH:mm:sszzzz")
            let dateEnd = Formatter.stringToDate(string: $0.fechaFinVigencia, format: "yyyy-MM-dd'T'HH:mm:sszzzz")
            
            if dateStart != nil && dateEnd != nil{
                return Date() >= dateStart! && Date() <= dateEnd!
            }
            else if dateStart != nil{
                return Date() >= dateStart!
            }
            else if dateEnd != nil{
                return Date() <= dateEnd!
            }
            
            return true
            
            
        }.map{NewsItem(model: $0)}
        
        if self.filterCategory != nil && self.filterCategory! != "" {
            let categories = self.filterCategory!.split(separator: ",")
            
            var aux = [NewsItem]()
            
            for newsItem in news{
                for category in categories {
                    if newsItem.categorias.contains(category){
                        aux.append(newsItem)
                    }
                }
            }
            return aux
        }
        else{
            return news
        }
        
    }
    
    public func getAll() -> [HermesObject] {
        let news = DataBaseManager.shared.getObjects(type: NewsItemModel.self)
        .sorted{$0.fecha > $1.fecha}
        .map{NewsItem(model: $0)}
        
        return news
        
    }
    
    public func filter(text: String?, categories: [NewsCategory]?, tags: [String]?, startDate: Date?, endDate: Date?) -> [NewsItem] {
        
        let news = DataBaseManager.shared.getObjects(type: NewsItemModel.self)
            .sorted{$0.fecha > $1.fecha}
            .filter{
                guard let dateStart = Formatter.stringToDate(string: $0.fechaInicioVigencia, format: "yyyy-MM-dd'T'HH:mm:sszzzz") else {return true}
                guard let dateEnd = Formatter.stringToDate(string: $0.fechaFinVigencia, format: "yyyy-MM-dd'T'HH:mm:sszzzz") else {return true}
                
                return Date() >= dateStart && Date() <= dateEnd
                
            }
            .filter{
                
                var textResult = true
                var categoriesResult = true
                var tagsResult = true
                var startDateResult = true
                var endDateResult = true
                
                if text != nil{
                    if !$0.descripcion.uppercased().contains(text!.uppercased()) && !$0.titulo.uppercased().contains(text!.uppercased()){
                        textResult = false
                    }
                }
                
                if categories != nil{
                    
                    for category in categories!{
                        if !$0.categorias.contains(String(category.id)){
                            categoriesResult = false
                        }
                        else{
                            categoriesResult = true
                            break
                        }
                    }
                }
                
                if tags != nil{
                    for tag in tags!{
                        if !$0.tags.contains(tag){
                            tagsResult = false
                        }
                        else{
                            tagsResult = true
                            break
                        }
                    }
                }
                
                if startDate != nil{
                    
                    if let date = Formatter.stringToDate(string: $0.fecha, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
                        if !(date >= startDate!){
                            startDateResult = false
                        }
                    }
                    else{
                        startDateResult = false
                    }
                    
                }
                
                if endDate != nil{
                    
                    if let date = Formatter.stringToDate(string: $0.fecha, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
                        if !(date <= endDate!){
                            endDateResult = false
                        }
                    }
                    else{
                        endDateResult = false
                    }
                    
                }
                
                let result = textResult && categoriesResult && tagsResult && startDateResult && endDateResult
                
                return result
            }
            .map{NewsItem(model: $0)}
        
        return news
        
    }
    
    public func update(completion: ((UpdateResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = NewsRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, categorias: "", completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: NewsItemModel.self)
                        
                        var news = [NewsItemModel]()
                        
                        for element in results{
                            let newsItem = NewsItemModel(info: element)
                            
                            news.append(newsItem)
                        }
                        
                        DataBaseManager.shared.save(objects: news)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
        
        self.categoriesModule.update(completion: {(updateResult) -> () in
            
            /*guard let newsCategories = self.categoriesModule.get() as? [NewsCategory] else {
                if completion != nil{
                    
                    DispatchQueue.main.async {
                        completion!(.error)
                    }
                    
                }
                return
            }
            
            var categoryIds = [String]()
            
            for category in newsCategories{
                categoryIds.append(String(category.id))
            }
            
            let categories = categoryIds.joined(separator: ",")*/
            
            
            
        })
        
 
    
    }
    
    public func updateByContentOwner(contentOwnerId: Int, completion: ((UpdateResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = NewsRequest()
        
        request.downloadByContentOwner(appToken: self.appToken, userToken: userToken, contentOwnerId: String(contentOwnerId), completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: NewsItemModel.self)
                        
                        var news = [NewsItemModel]()
                        
                        for element in results{
                            let newsItem = NewsItemModel(info: element)
                            
                            news.append(newsItem)
                        }
                        
                        DataBaseManager.shared.save(objects: news)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
        
    }
    
    public func create(contentOwner: ContentOwner, categoryId: Int, title: String, description: String, image: UIImage?, startDate: Date, endDate: Date, publish: Bool, completion: ((NewsItem?, SendResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(nil, .error)
                }
                
            }
            return
        }
                
        let request = NewsRequest()
        
        let startDateString = Formatter.dateToString(date: startDate, format: "yyyy-MM-dd'T'HH:mm:ssZ")
        let endDateString = Formatter.dateToString(date: endDate, format: "yyyy-MM-dd'T'HH:mm:ssZ")
        
        var publishString = "true"
        
        if publish == false{
            publishString = "false"
        }
        
        request.createNews(appToken: self.appToken, userToken: userToken, categoryId: String(categoryId), contentOwnerId: String(contentOwner.id), title: title, description: description, startDate: startDateString, endDate: endDateString, image: image, publish: publishString, completion:  {(info, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct {
                    
                    let newsItemModel = NewsItemModel(info: info)
                    let newsItem = NewsItem(model: newsItemModel)
                    
                    if completion != nil{
                        completion!(newsItem, .correct)
                    }

                }
                else{
                    if completion != nil{
                        completion!(nil, .error)
                    }
                }
            }
            
        })
 
    
    }
    
    public func edit(newsItem: NewsItem, title: String, description: String, image: UIImage?, startDate: Date, endDate: Date, publish: Bool, completion: ((SendResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.error)
                }
                
            }
            return
        }
                
        let request = NewsRequest()
        
        let startDateString = Formatter.dateToString(date: startDate, format: "yyyy-MM-dd'T'HH:mm:ssZ")
        let endDateString = Formatter.dateToString(date: endDate, format: "yyyy-MM-dd'T'HH:mm:ssZ")
        
        var publishString = "true"
        
        if publish == false{
            publishString = "false"
        }
        
        request.editNews(appToken: self.appToken, userToken: userToken, newsItemId: String(newsItem.id), title: title, description: description, startDate: startDateString, endDate: endDateString, image: image, publish: publishString, completion:  {(requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }

                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
}
