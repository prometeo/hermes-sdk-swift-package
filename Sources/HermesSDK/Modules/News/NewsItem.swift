//
//  NewsItem.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 09/11/2020.
//

import Foundation
import UIKit

public class NewsItem : HermesObject {
        
    internal init(model: NewsItemModel) {
        self.id = model.id
        self.titulo = model.titulo
        self.descripcionRaw = model.descripcion
        self.link = model.link
        self.categorias = model.categorias
        self.imagen = model.imagen
        self.publica = model.publica
        self.pago = model.pago
        self.tags = model.tags
        self.importe = String(format: "%.2f", model.importe).replacingOccurrences(of: ".", with: ",")
        self.fecha = model.fecha
        self.fechaModificacion = model.fechaModificacion
        self.fechaInicioVigencia = model.fechaInicioVigencia
        self.fechaFinVigencia = model.fechaFinVigencia
        self.activa = model.activa
    }
    
    private var descripcionRaw : String
    
    public var id : Int
    public var titulo : String
    public var descripcion : String {
        return Formatter.removeHtml(string: self.descripcionRaw)
    }
    public var descripcionHtml : NSAttributedString {
        return Formatter.convertHtml(string: self.descripcionRaw, font: UIFont(name: "Helvetica Neue", size: 18.0)!, color: UIColor.darkGray) ?? NSAttributedString(string: "")
    }
    public var link : String
    public var categorias : String
    
    public var nombresCategorias : String{
        let categorias = DataBaseManager.shared.getObjects(type: NewsCategoryModel.self)
        
        var nombresCategorias = [String]()
        
        for categoria in categorias{
            if self.categorias.contains(String(categoria.id)) == true{
                nombresCategorias.append(categoria.nombre)
            }
        }
        
        return nombresCategorias.joined(separator: ", ")
    }
    
    public var imagen : String
    public var publica : Bool
    public var activa : Bool
    public var pago : Bool
    public var tags : String
    public var importe : String
    public var leida : Bool {
        guard let readNews : [Int] = UserDefaults.standard.array(forKey: "readNews") as? [Int] else {return false}
        
        return readNews.contains(self.id)
        
    }
    
    public func marcarLeida(){
        
        if var readNews : [Int] = UserDefaults.standard.array(forKey: "readNews") as? [Int]{
            
            if readNews.contains(self.id) == false{
                readNews.append(self.id)
                
                UserDefaults.standard.set(readNews, forKey: "readNews")
            }
            
        }
        else{
            UserDefaults.standard.set([self.id], forKey: "readNews")
        }
    }
    
    
    public var fecha : String
    public var fechaModificacion : String
    public var fechaInicioVigencia : String
    public var fechaFinVigencia : String
    
    public func fecha(formato: String) -> String {
        var resultado = ""
    
        if let fechaDate = Formatter.stringToDate(string: self.fecha, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
    
        return resultado
    }
    
    public func fechaModificacion(formato: String) -> String {
        var resultado = ""
        
        if let fechaDate = Formatter.stringToDate(string: self.fechaModificacion, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
        
        return resultado
    }
    
    public func fechaInicioVigencia(formato: String) -> String {
        var resultado = ""
        
        if let fechaDate = Formatter.stringToDate(string: self.fechaInicioVigencia, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
        
        return resultado
    }
    
    public func fechaFinVigencia(formato: String) -> String {
        var resultado = ""
        
        if let fechaDate = Formatter.stringToDate(string: self.fechaFinVigencia, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
        
        return resultado
    }
 
}
