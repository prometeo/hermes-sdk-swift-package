//
//  NewsCategoriesRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 09/11/2020.
//

import Foundation

class NewsCategoriesRequest{
    
    func download(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = NEWS_CATEGORIES_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded"]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "appToken", value: appToken))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "userToken", value: userToken))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            guard let str = jsonData["str"] as? String else {completion(results, .error); return}
            
            if str == "OK"{
                
                guard let info = jsonData["categorias"] as? [[String : Any]] else {completion(results, .error); return}
                
                results = info
                
                completion(results, .correct)
                return
            }
            else{
                completion(results, .error)
                return
            }
            

        })
        
    }
    
    func activate(userToken: String, categoryId: String, completion: (@escaping(RequestResult) -> ())){
        
        let url = ACTIVATE_NEWS_CATEGORY_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded"]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "idCategoria", value: categoryId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "userToken", value: userToken))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {return}
            guard responseCode == .success else {return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {return}
            
            guard let str = jsonData["message"] as? String else {completion(.error); return}
            
            if str == "OK"{
                
                completion(.correct)
                return
            }
            else{
                completion(.error)
                return
            }
            

        })
        
    }
    
    func deactivate(userToken: String, categoryId: String, completion: (@escaping(RequestResult) -> ())){
        
        let url = DEACTIVATE_NEWS_CATEGORY_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded"]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "idCategoria", value: categoryId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "userToken", value: userToken))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {return}
            guard responseCode == .success else {return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {return}
            
            guard let str = jsonData["message"] as? String else {completion(.error); return}
            
            if str == "OK"{
                
                completion(.correct)
                return
            }
            else{
                completion(.error)
                return
            }
            

        })
        
    }
    
}
