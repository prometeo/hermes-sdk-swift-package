//
//  Resource.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 15/6/21.
//

import Foundation
import UIKit

public class Resource : HermesObject {
    
    
    internal init(model: ResourceModel) {
        self.id = model.id
        self.nombre = model.nombre
        self.descripcion = model.descripcion
        self.informacionRaw = model.informacion
        self.aforo = model.aforo
        self.tiempoMinimoReserva = model.tiempoMinimoReserva
        self.tiempoMaximoReserva = model.tiempoMaximoReserva
        self.reservaAutomatica = model.reservaAutomatica
        self.reservaColectiva = model.reservaColectiva
        self.ubicacion = model.ubicacion
        self.tipoPago = model.tipoPago
        self.latitud = (model.latitud as NSString).doubleValue
        self.longitud = (model.longitud as NSString).doubleValue
        self.diasNoDisponibles = Array(model.diasNoDisponibles)
        self.disponibilidad = model.disponibilidad.map{ResourceAvailableDate(model: $0)}
        
        var fechas = [Date]()
        
        for fechaString in model.fechasNoDisponibles{
            if let fecha = Formatter.stringToDate(string: fechaString, format: "dd/MM/yyyy"){
                fechas.append(fecha)
            }
        }
        
        self.fechasNoDisponibles = fechas
        self.tipoReserva = model.tipoReserva
        
        self.categorias = model.categorias.map{ResourceCategory(model: $0)}
    }
    
    public var id : Int
    public var nombre : String
    public var descripcion : String
    
    private var informacionRaw : String
    
    public var informacion : String {
        Formatter.removeHtml(string: self.informacionRaw)
    }
    public var informacionHtml : NSAttributedString {
        Formatter.convertHtml(string: self.informacionRaw, font: UIFont(name: "Helvetica Neue", size: 18.0)!, color: UIColor.darkGray) ?? NSAttributedString(string: "")
    }
    public var aforo : Int
    public var tiempoMinimoReserva : Int
    public var tiempoMaximoReserva : Int
    public var reservaAutomatica : Bool
    public var reservaColectiva : Bool
    public var ubicacion : String
    public var tipoPago : String
    public var latitud : Double
    public var longitud : Double
    public var fechasNoDisponibles : [Date]
    public var diasNoDisponibles : [Int]
    public var disponibilidad : [ResourceAvailableDate]
    public var tipoReserva : Int
    public var categorias : [ResourceCategory]
}
