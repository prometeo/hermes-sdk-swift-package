//
//  Reserve.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 26/11/21.
//

import Foundation
import UIKit

public class Reserve : HermesObject {
    
    
    internal init(model: ReserveModel) {
        self.id = model.id
        self.descripcion = model.descripcion
        self.importe = model.importe
        self.tipo = model.tipo
        self.tipoPago = model.tipoPago
        self.propietarioContenido = model.propietarioContenido
        self.estado = model.estado
        
        self.inicio = Date()
        
        if let inicioDate = Formatter.stringToDate(string: model.inicio, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            self.inicio = inicioDate
        }

        
        self.fin = Date()
        
        if let finDate = Formatter.stringToDate(string: model.fin, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            self.fin = finDate
        }
        
        let recursos = DataBaseManager.shared.getObjects(type: ResourceModel.self).filter{$0.id == model.idRecurso}.map{Resource(model: $0)}

        self.recurso = recursos.first
        
    }
    
    public var horaInicio : String {
        return Formatter.dateToString(date: self.inicio, format: "HH:mm")
    }
    
    public var horaFin : String {
        return Formatter.dateToString(date: self.fin, format: "HH:mm")
    }
    
    public func fechaInicio(formato: String) -> String {
        return Formatter.dateToString(date: self.inicio, format: formato)
    }
    
    public func fechaFin(formato: String) -> String {
        return Formatter.dateToString(date: self.fin, format: formato)
    }
    
    public var id : Int
    public var descripcion : String
    public var importe : Int
    public var inicio : Date
    public var fin : Date
    public var tipo : String
    public var recurso : Resource?
    public var tipoPago : String
    public var propietarioContenido : String
    public var estado : String
    

}
