//
//  ReserveType.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 16/6/21.
//

import Foundation
import UIKit

public class ReserveType : HermesObject {
    
    
    internal init(model: ReserveTypeModel) {
        self.id = model.id
        self.nombre = model.nombre
    }
    
    public var id : Int
    public var nombre : String
}
