//
//  ResourceAvailableDateModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 15/6/21.
//

import Foundation
import RealmSwift

class ResourceAvailableDateModel : Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var diaSemana : Int = 0
    @objc dynamic var horaInicio : String = ""
    @objc dynamic var horaFin : String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.diaSemana = info["diasemana"] as? Int ?? 0
        self.horaInicio = info["hora_inicio"] as? String ?? ""
        self.horaFin = info["hora_fin"] as? String ?? ""
        
    }
}
