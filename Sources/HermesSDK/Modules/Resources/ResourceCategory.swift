//
//  ResourceCategory.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 19/12/22.
//

import Foundation
import UIKit

public class ResourceCategory: Hashable {
    
    public static func == (lhs: ResourceCategory, rhs: ResourceCategory) -> Bool {
        return lhs.id == rhs.id
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.id)
    }
        
    internal init(model: ResourceCategoryModel) {
        self.id = model.id
        self.nombre = model.nombre
        self.alias = model.alias
    }
    
    public var id : Int
    public var nombre : String
    public var alias : String
    
}
