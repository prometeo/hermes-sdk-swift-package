//
//  ReserveTime.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 29/11/21.
//

import Foundation
import UIKit

public class ReserveTime : HermesObject {
        
    internal init(){
        self.id = -1
        self.horaInicio = Date()
        self.horaFin = Date()
    }
    
    internal init(model: ReserveTimeModel) {
        self.id = model.id
        
        if let horaInicioDate = Formatter.stringToDate(string: model.inicio, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            self.horaInicio = horaInicioDate
        }
        else{
            self.horaInicio = Date()
        }
        
        if let horaFinDate = Formatter.stringToDate(string: model.fin, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            self.horaFin = horaFinDate
        }
        else{
            self.horaFin = Date()
        }
        
    }
    
    public init(horaInicio: Date, horaFin: Date) {
        self.id = -1
        self.horaInicio = horaInicio
        self.horaFin = horaFin
        self.estaDisponible = true
    }
    
    public var id : Int
    
    public func horaInicio(formato: String) -> String{
        
        return Formatter.dateToString(date: self.horaInicio, format: formato)
    }
    
    public var horaInicio : Date
    
    public func horaFin(formato: String) -> String{

        return Formatter.dateToString(date: self.horaFin, format: formato)
    }
    
    public var horaFin : Date
    
    public var estaDisponible = true
}
