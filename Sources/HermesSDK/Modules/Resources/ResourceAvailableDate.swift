//
//  ResourceAvailableDate.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 15/6/21.
//

import Foundation
import UIKit

public class ResourceAvailableDate : HermesObject {
        
    internal init(model: ResourceAvailableDateModel) {
        self.id = model.id
        self.diaNumero = model.diaSemana
        
        if let horaInicioDate = Formatter.stringToDate(string: model.horaInicio, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            self.horaInicio = horaInicioDate
        }
        else{
            self.horaInicio = Date()
        }
        
        if let horaFinDate = Formatter.stringToDate(string: model.horaFin, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            self.horaFin = horaFinDate
        }
        else{
            self.horaFin = Date()
        }
    }
    
    public var id : Int
    public var diaNumero : Int
    
    public var diaTexto : String{
        switch self.diaNumero {
        case 0:
            return "Domingo"
        case 1:
            return "Lunes"
        case 2:
            return "Martes"
        case 3:
            return "Miércoles"
        case 4:
            return "Jueves"
        case 5:
            return "Viernes"
        case 6:
            return "Sábado"
        default:
            return ""
        }
    }
    
    public func horaInicio(formato: String) -> String{
        
        return Formatter.dateToString(date: self.horaInicio, format: formato)
    }
    
    public var horaInicio : Date
    
    public func horaFin(formato: String) -> String{

        return Formatter.dateToString(date: self.horaFin, format: formato)
    }
    
    public var horaFin : Date
    
    public func horaInicio(atDate: Date) -> Date{
        let date = Formatter.dateToString(date: atDate, format: "dd-MM-yyyy")
        let time = Formatter.dateToString(date: horaInicio, format: "HH:mm")
        
        let dateAndTime = date + "T" + time
        
        return Formatter.stringToDate(string: dateAndTime, format: "dd-MM-yyyy'T'HH:mm") ?? Date()
        
    }
    
    public func horaFin(atDate: Date) -> Date{
        let date = Formatter.dateToString(date: atDate, format: "dd-MM-yyyy")
        let time = Formatter.dateToString(date: horaFin, format: "HH:mm")
        
        let dateAndTime = date + "T" + time
        
        return Formatter.stringToDate(string: dateAndTime, format: "dd-MM-yyyy'T'HH:mm") ?? Date()
        
    }
}
