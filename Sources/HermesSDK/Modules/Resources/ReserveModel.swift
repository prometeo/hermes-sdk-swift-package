//
//  ReserveModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 26/11/21.
//

import Foundation
import RealmSwift

class ReserveModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var descripcion : String = ""
    @objc dynamic var importe : Int = 0
    @objc dynamic var inicio : String = ""
    @objc dynamic var fin : String = ""
    @objc dynamic var tipo : String = ""
    @objc dynamic var idRecurso : Int = 0
    @objc dynamic var tipoPago : String = ""
    @objc dynamic var propietarioContenido : String = ""
    @objc dynamic var estado : String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.descripcion = info["descripcion"] as? String ?? ""
        self.importe = info["importe"] as? Int ?? 0
        
        if let horarios = info["horario"] as? [[String : Any]]{
            if let horario = horarios.first{
                self.inicio = horario["inicio"] as? String ?? ""
                self.fin = horario["fin"] as? String ?? ""
            }
        }
        
        if let tipo_reserva = info["tipo_reserva"] as? [String : Any]{
            self.tipo = tipo_reserva["nombre"] as? String ?? ""
        }
        
        if let recurso = info["recurso"] as? [String : Any]{
            self.idRecurso = recurso["id"] as? Int ?? -1
        }
        
        if let tipo_pago = info["tipo_pago"] as? [String : Any]{
            self.tipoPago = tipo_pago["nombre"] as? String ?? ""
        }
        
        if let pacientes_reserva = info["pacientes_reserva"] as? [[String : Any]]{
            if let paciente = pacientes_reserva.first{
                let nombre = paciente["nombre"] as? String ?? ""
                let apellidos = paciente["apellidos"] as? String ?? ""
                
                self.propietarioContenido = nombre + " " + apellidos
            }
        }
        
        if let estado = info["estado"] as? [String : Any]{
            self.estado = estado["nombre"] as? String ?? ""
        }
    }
}
