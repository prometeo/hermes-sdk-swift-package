//
//  ResourceModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 15/6/21.
//

import Foundation
import RealmSwift

class ResourceModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var nombre : String = ""
    @objc dynamic var descripcion : String = ""
    @objc dynamic var informacion : String = ""
    @objc dynamic var aforo : Int = 0
    @objc dynamic var tiempoMinimoReserva : Int = 0
    @objc dynamic var tiempoMaximoReserva : Int = 0
    @objc dynamic var reservaAutomatica : Bool = false
    @objc dynamic var reservaColectiva : Bool = false
    @objc dynamic var ubicacion : String = ""
    @objc dynamic var tipoPago : String = ""
    @objc dynamic var latitud : String = ""
    @objc dynamic var longitud : String = ""
    @objc dynamic var tipoReserva : Int  = 0
    
    let fechasNoDisponibles = List<String>()
    let diasNoDisponibles = List<Int>()
    let disponibilidad = List<ResourceAvailableDateModel>()
    let categorias = List<ResourceCategoryModel>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.nombre = info["nombre"] as? String ?? ""
        self.descripcion = info["descripcion"] as? String ?? ""
        self.informacion = info["informacion_adicional"] as? String ?? ""
        self.aforo = info["aforo"] as? Int ?? 0
        self.tiempoMinimoReserva = info["tiempo_minimo_reserva"] as? Int ?? 0
        self.tiempoMaximoReserva = info["tiempo_maximo_reserva"] as? Int ?? 0
        self.reservaAutomatica = info["is_reserva_automatica"] as? Bool ?? false
        self.reservaColectiva = info["is_reserva_colectiva"] as? Bool ?? false
        self.ubicacion = info["ubicacion"] as? String ?? ""
        self.latitud = info["latitud"] as? String ?? ""
        self.longitud = info["longitud"] as? String ?? ""
        
        if let tipo_pago = info["tipo_pago"] as? [String : Any], let nombreTipoPago = tipo_pago["nombre"] as? String{
            self.tipoPago = nombreTipoPago
        }
        
        if let tipo_reserva = info["tipo_reserva"] as? [String : Any], let idTipoReserva = tipo_reserva["id"] as? Int{
            self.tipoReserva = idTipoReserva
        }
        
        if let categorias = info["categorias"] as? [[String : Any]]{
            for categoria in categorias {
                let categoriaModel = ResourceCategoryModel(info: categoria)
                
                self.categorias.append(categoriaModel)
            }
        }
        
    }
    
    public func setTimetable(timetable: [String : Any]){
        
        if let disabledDates = timetable["disabledDates"] as? [String]{
            for disabledDate in disabledDates{
                self.fechasNoDisponibles.append(disabledDate)
            }
        }
        
        if let daysWeekDisabled = timetable["daysWeekDisabled"] as? [Int]{
            for day in daysWeekDisabled{
                self.diasNoDisponibles.append(day)
            }
        }
        
        if let disponibilidadDiaria = timetable["disponibilidadDiaria"] as? [[String : Any]]{
            for disponibilidad in disponibilidadDiaria{
                let avalaibleDate = ResourceAvailableDateModel(info: disponibilidad)
                
                self.disponibilidad.append(avalaibleDate)
            }
        }
    }
}
