//
//  HermesResources.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 15/6/21.
//

import Foundation

public class HermesResources: HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    private var filterCategory : String?
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func filterBy(_ category : String) -> HermesResources {
        
        self.filterCategory = category
        
        return self
    }
    
    public func get() -> [HermesObject] {
        let resources = DataBaseManager.shared.getObjects(type: ResourceModel.self).map{Resource(model: $0)}.sorted{$0.nombre < $1.nombre}
        
        return resources
        
    }
    
    public func getReserves() -> [HermesObject] {
        let resources = DataBaseManager.shared.getObjects(type: ReserveModel.self).map{Reserve(model: $0)}.sorted{$0.inicio < $1.inicio}
        
        return resources
        
    }
    
    public func getReserveTypes() -> [ReserveType] {
        let reserveTypes = DataBaseManager.shared.getObjects(type: ReserveTypeModel.self).map{ReserveType(model: $0)}.sorted{$0.nombre < $1.nombre}
        
        return reserveTypes
    }
    
    public func update(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        self.updateReserveTypes(completion: {(updateResult) -> () in
            
            if updateResult == .correct{
                
                let request = ResourcesRequest()
                
                request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
                    
                    DispatchQueue.main.async {
                        if requestResult == .correct{
                            
                            DispatchQueue.main.async {
                                
                                var resources = [ResourceModel]()
                                
                                var resourcesNum = results.count
                                
                                if resourcesNum == 0{
                                    if completion != nil{
                                        completion!(.correct)
                                    }
                                }
                                else{
                                    
                                    for element in results{
                                        
                                        guard let resourceId = element["id"] as? Int else {return}
                                        
                                        request.getResourceTimetable(appToken: self.appToken, userToken: userToken, resourceId: resourceId, completion: {(timetable, requestResult) -> () in
                                            
                                            DispatchQueue.main.async {
                                                if requestResult == .correct{
                                                    
                                                    let resource = ResourceModel(info: element)
                                                    resource.setTimetable(timetable: timetable)
                                                    
                                                    resources.append(resource)
                                                }
                                                
                                                
                                                
                                                resourcesNum -= 1
                                                
                                                if resourcesNum == 0{
                                                    
                                                    DataBaseManager.shared.deleteObjects(type: ResourceModel.self)
                                                    
                                                    DataBaseManager.shared.save(objects: resources)
                                                    
                                                    if completion != nil{
                                                        completion!(.correct)
                                                    }
                                                }
                                            }
                                            
                                        })
                                        
                                        
                                        
                                    }
                                    
                                    
                                }
                                

                            }
                            
                        }
                        else{
                            if completion != nil{
                                completion!(.error)
                            }
                        }
                    }
                    
                })
            }
            else{
                if completion != nil{
                    completion!(.error)
                }
            }
        })
        
        
        
        
 
    
    }
    
    public func updateReserveTypes(completion: ((UpdateResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = ResourcesRequest()
        
        request.getReserveTypes(appToken: self.appToken, userToken: userToken, completion: {(types, requestResult) -> () in
            
            DispatchQueue.main.async {
                
                if requestResult == .correct{
                    
                    DataBaseManager.shared.deleteObjects(type: ReserveTypeModel.self)
                    
                    var reserveTypes = [ReserveTypeModel]()
                    
                    for type in types{
                        let reserveType = ReserveTypeModel(info: type)
                        reserveTypes.append(reserveType)
                    }
                    
                    DataBaseManager.shared.save(objects: reserveTypes)
                    
                    if completion != nil{
                        completion!(.correct)
                    }
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
            
        })
    }
    
    public func updateReserves(contentOwner: ContentOwner, completion: ((UpdateResult) -> ())?){
        
        self.update(completion: {(updateResourcesResult) -> () in
            
            if updateResourcesResult == .correct{
                guard let userToken = self.userToken else {
                    if completion != nil{
                        
                        DispatchQueue.main.async {
                            completion!(.missingUserToken)
                        }
                        
                    }
                    return
                }
                        
                let request = ResourcesRequest()
                
                request.getReserves(appToken: self.appToken, userToken: userToken, contentOwnerId: contentOwner.id, completion: {(reservesInfo, requestResult) -> () in
                    
                    DispatchQueue.main.async {
                        
                        if requestResult == .correct{
                            
                            DataBaseManager.shared.deleteObjects(type: ReserveModel.self)
                            
                            var reserves = [ReserveModel]()
                            
                            for reserveInfo in reservesInfo{
                                let reserve = ReserveModel(info: reserveInfo)
                                reserves.append(reserve)
                            }
                            
                            DataBaseManager.shared.save(objects: reserves)
                            
                            if completion != nil{
                                completion!(.correct)
                            }
                        }
                        else{
                            if completion != nil{
                                completion!(.error)
                            }
                        }
                    }
                    
                    
                })
            }
            else{
                if completion != nil{
                    completion!(.error)
                }
            }
        })
        
    }
    
    public func reserve(resource: Resource, contentOwner: ContentOwner, date: Date, startTime: Date, endTime: Date, description: String, completion: ((SendResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.error)
                }
                
            }
            return
        }
                
        
        let request = ResourcesRequest()
        
        let auxStartTimeString = Formatter.dateToString(date: date, format: "yyyy-MM-dd") + Formatter.dateToString(date: startTime, format: "'T'HH:mm")
        let auxEndTimeString = Formatter.dateToString(date: date, format: "yyyy-MM-dd") + Formatter.dateToString(date: endTime, format: "'T'HH:mm")
        
        guard let auxStartTime = Formatter.stringToDate(string: auxStartTimeString, format: "yyyy-MM-dd'T'HH:mm") else {completion?(.error); return}
        guard let auxEndTime = Formatter.stringToDate(string: auxEndTimeString, format: "yyyy-MM-dd'T'HH:mm") else {completion?(.error); return}
        
        request.sendReserve(appToken: self.appToken, userToken: userToken, resourceId: resource.id, typeId: resource.tipoReserva, contentOwnerId: contentOwner.id, description: description, startTime: auxStartTime, endTime: auxEndTime, completion: {(reserve, requestResult) ->() in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        if requestResult == .correct{
                            if completion != nil{
                                completion!(.correct)
                            }
                        }
                        else{
                            if completion != nil{
                                completion!(.error)
                            }
                        }
                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
        })
        
        
    }
    
    public func getReservedTimes(resource: Resource, date: Date, completion: (([ReserveTime], DownloadResult) -> ())?){
        
        var result = [ReserveTime]()
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(result, .missingUserToken)
                }
                
            }
            return
        }
                
        let request = ResourcesRequest()
        
        request.getReservedTimes(appToken: self.appToken, userToken: userToken, resourceId: resource.id, completion: {(times, requestResult) -> () in
            
            DispatchQueue.main.async {
                
                if requestResult == .correct{
                    
                    var auxReservedTimes = [ReserveTime]()
                    
                    for time in times{
                        let reservedTimeModel = ReserveTimeModel(info: time)
                        
                        let reservedTime = ReserveTime(model: reservedTimeModel)
                        
                        if let startSelectedDate = date.startOfDay, let endSelectedDate = date.endOfDay{
                            
                            if (reservedTime.horaInicio >= startSelectedDate && reservedTime.horaInicio <= endSelectedDate){
                                 
                                auxReservedTimes.append(reservedTime)
                            }
                        }
                        
                    }
                    
                    auxReservedTimes = auxReservedTimes.sorted{$0.horaInicio <= $1.horaInicio}
                    
                    var reservedTimes = [ReserveTime]()
                    
                    for t in auxReservedTimes{
                        
                        let overlaps = self.overlaps(time: t, with: auxReservedTimes)
                        
                        if overlaps >= resource.aforo{
                            reservedTimes.append(t)
                        }
                    }
                    
                    let weekday = Calendar.current.component(.weekday, from: date) - 1
                    
                    var resourceAvalaibleTimes = resource.disponibilidad.filter{$0.diaNumero == weekday}
                    
                    resourceAvalaibleTimes = resourceAvalaibleTimes.sorted{$0.horaInicio <= $1.horaInicio}
                    
                    var resourceTimes = [ReserveTime]()
                    
                    for resourceAvalaibleTime in resourceAvalaibleTimes{
                        
                        let auxTime = ReserveTime()
                        auxTime.horaInicio = resourceAvalaibleTime.horaInicio(atDate: date)
                        auxTime.horaFin = resourceAvalaibleTime.horaFin(atDate: date)
                        
                        resourceTimes.append(auxTime)
                    }
                    
                    while resourceTimes.isEmpty == false{
                        let resourceTime = resourceTimes.removeFirst()
                        
                        result.append(resourceTime)
                        
                        while reservedTimes.isEmpty == false {
                            let reservedTime = reservedTimes.removeFirst()
                            result = self.remove(time: reservedTime, from: result)
                        }
                        
                    }
                    
                    
                    
                }
                
                if completion != nil{
                    completion!(result, .correct)
                }
            }
            
            
        })
    }
    
    
    private func remove(time: ReserveTime, from: [ReserveTime]) -> [ReserveTime]{
        
        var result = [ReserveTime]()
        
        for t in from{
            result.append(contentsOf: remove(time: time, from: t))
        }
        
        return result
    }
    
    private func remove(time: ReserveTime, from: ReserveTime) -> [ReserveTime]{
        
        var result = [ReserveTime]()
        
        print("RESERVA")
        print(time.horaInicio(formato: "HH:mm"))
        print(time.horaFin(formato: "HH:mm"))
        
        print("RECURSO")
        print(from.horaInicio(formato: "HH:mm"))
        print(from.horaFin(formato: "HH:mm"))
        
        
        if time.horaInicio >= from.horaInicio && time.horaFin <= from.horaFin{
            
            if time.horaInicio == from.horaInicio{
                let firstTime = ReserveTime()
                firstTime.horaInicio = time.horaFin
                firstTime.horaFin = from.horaFin
                
                result.append(firstTime)
            }
            else{
                let firstTime = ReserveTime()
                firstTime.horaInicio = from.horaInicio
                firstTime.horaFin = time.horaInicio
                
                result.append(firstTime)
                
                let secondTime = ReserveTime()
                secondTime.horaInicio = time.horaFin
                secondTime.horaFin = from.horaFin
                
                result.append(secondTime)
            }
            
        }
        else if time.horaInicio >= from.horaInicio && time.horaFin >= from.horaFin{
            if time.horaFin <= from.horaFin{
                let firstTime = ReserveTime()
                firstTime.horaInicio = from.horaInicio
                firstTime.horaFin = time.horaInicio
                
                result.append(firstTime)
            }
            else{
                result.append(from)
            }
        }
        else if time.horaInicio <= from.horaInicio && time.horaFin <= from.horaFin{
            
            if time.horaFin >= from.horaInicio{
                let firstTime = ReserveTime()
                firstTime.horaInicio = time.horaFin
                firstTime.horaFin = from.horaFin
                
                result.append(firstTime)
            }
            else{
                result.append(from)
            }
            
        }
        else{
            result.append(from)
        }
        
        
        return result
    }
    
    private func overlaps(time: ReserveTime, with: [ReserveTime]) -> Int{
        
        var result = 0
        
        for t in with{
            if time.horaInicio >= t.horaInicio && time.horaInicio <= t.horaFin{
                result += 1
            }
        }
        
        return result
        
    }
}
