//
//  ReserveTimeModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 29/11/21.
//

import Foundation

class ReserveTimeModel  {
    
    var id : Int  = 0
    var inicio : String = ""
    var fin : String = ""

    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.inicio = info["inicio"] as? String ?? ""
        self.fin = info["fin"] as? String ?? ""
        
    }
}
