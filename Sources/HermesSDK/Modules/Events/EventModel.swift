//
//  EventModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 09/11/2020.
//

import Foundation
import RealmSwift

class EventModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var titulo : String = ""
    @objc dynamic var descripcion : String = ""
    @objc dynamic var descripcionCorta : String = ""
    @objc dynamic var ubicacion : String = ""
    @objc dynamic var fechaInicio : String = ""
    @objc dynamic var fechaFin : String = ""
    @objc dynamic var imagen : String = ""
    @objc dynamic var urlInscripcion : String = ""
    @objc dynamic var aforo : Int  = -1
    @objc dynamic var inscritos : Int  = 0
    @objc dynamic var latitud : String = ""
    @objc dynamic var longitud : String = ""
    @objc dynamic var ponente : String = ""
    @objc dynamic var direccion : String = ""
    @objc dynamic var precio : String = ""
    @objc dynamic var entidad : String = ""
    @objc dynamic var imagenEntidad : String = ""
    @objc dynamic var webEntidad : String = ""
    @objc dynamic var completo : Bool = false
    @objc dynamic var publico : Bool = false
    @objc dynamic var inscrito : Bool = false
    @objc dynamic var idFormularioInscripcion : Int = -1
    @objc dynamic var local : Bool = false
    
    let categorias = List<EventCategoryModel>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.titulo = info["titulo"] as? String ?? ""
        self.descripcion = info["descripcion"] as? String ?? ""
        self.descripcionCorta = info["descripcion_corta"] as? String ?? ""
        self.ubicacion = info["ubicacion"] as? String ?? ""
        self.fechaInicio = info["fecha_inicio"] as? String ?? ""
        self.fechaFin = info["fecha_fin"] as? String ?? ""
        self.urlInscripcion = info["inscripcion_url"] as? String ?? ""
        self.aforo = info["aforo"] as? Int ?? -1
        self.inscritos = info["inscritos"] as? Int ?? 0
        self.latitud = info["latitud"] as? String ?? ""
        self.longitud = info["longitud"] as? String ?? ""
        self.ponente = info["ponente"] as? String ?? ""
        self.direccion = info["direccion"] as? String ?? ""
        self.precio = info["precio"] as? String ?? ""
        self.entidad = info["entidad"] as? String ?? ""
        self.imagenEntidad = info["entidad_img"] as? String ?? ""
        self.webEntidad = info["entidad_web"] as? String ?? ""
        self.completo = info["completo"] as? Bool ?? false
        self.publico = info["is_publico"] as? Bool ?? false
        self.inscrito = info["subscribed"] as? Bool ?? false
        
        if let imagen = info["imagen_url"] as? String{
            if imagen.hasPrefix("http") == false{
                self.imagen = BASE_URL + imagen
            }
            else{
                self.imagen = imagen
            }
        }
        
        if self.imagen == ""{
            if let imagen = info["imagen"] as? String{
                if imagen.hasPrefix("http") == false{
                    self.imagen = (BASE_URL + imagen).replacingOccurrences(of: "//", with: "/")
                }
                else{
                    self.imagen = imagen
                }
            }
        }
        
        self.imagen = self.imagen.replacingOccurrences(of: "uploads/images/photos//uploads/images/photos", with: "uploads/images/photos")
        
        if let formulario = info["formulario"] as? [String : Any], let idFormulario = formulario["id"] as? Int{
            
            self.idFormularioInscripcion = idFormulario
        }
        
        guard let categorias = info["categoriasevento"] as? [[String :Any]] else {return}
        
        for categoria in categorias{
            let categoriaEvento = EventCategoryModel(info: categoria)
            self.categorias.append(categoriaEvento)
        }
        
    }
    
    
}
