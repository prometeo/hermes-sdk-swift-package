//
//  EventCategory.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 09/11/2020.
//

import Foundation
import UIKit

public class EventCategory : HermesObject {
        
    internal init(model: EventCategoryModel) {
        self.id = model.id
        self.nombre = model.nombre
    }
    
    public var id : Int
    public var nombre : String
    
}
