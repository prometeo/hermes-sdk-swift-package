//
//  Event.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 09/11/2020.
//

import Foundation
import UIKit

public class Event : HermesObject {
    
    
    internal init(model: EventModel) {
        
        self.id = model.id
        self.titulo = model.titulo
        
        self.descripcionRaw = model.descripcion
        self.descripcionCortaRaw = model.descripcionCorta
        
        self.ubicacion = model.ubicacion
        
        if let fechaInicioDate = Formatter.stringToDate(string: model.fechaInicio, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            self.fechaInicio = fechaInicioDate
        }
        else{
            self.fechaInicio = Date()
        }
        
        if let fechaFinDate = Formatter.stringToDate(string: model.fechaFin, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            self.fechaFin = fechaFinDate
        }
        else{
            self.fechaFin = Date()
        }
        
        self.imagen = model.imagen
        self.urlInscripcion = model.urlInscripcion
        self.aforo = model.aforo
        self.inscritos = model.inscritos
        self.latitud = model.latitud
        self.longitud = model.longitud
        self.ponente = model.ponente
        self.direccion = model.direccion
        self.precio = model.precio
        self.entidad = model.entidad
        self.imagenEntidad = model.imagenEntidad
        self.webEntidad = model.webEntidad
        self.completo = model.completo
        self.publico = model.publico
        self.inscrito = model.inscrito
        self.idFormularioInscripcion = model.idFormularioInscripcion
        self.categorias = model.categorias.map{EventCategory(model: $0)}
        self.local = model.local
    }
    
    public init(event: Event) {
        
        self.id = event.id
        self.titulo = event.titulo
        self.descripcionRaw = event.descripcion
        self.descripcionCortaRaw = event.descripcionCorta
        self.ubicacion = event.ubicacion
        self.fechaInicio = event.fechaInicio
        self.fechaFin = event.fechaFin
        self.imagen = event.imagen
        self.urlInscripcion = event.urlInscripcion
        self.aforo = event.aforo
        self.inscritos = event.inscritos
        self.latitud = event.latitud
        self.longitud = event.longitud
        self.ponente = event.ponente
        self.direccion = event.direccion
        self.precio = event.precio
        self.entidad = event.entidad
        self.imagenEntidad = event.imagenEntidad
        self.webEntidad = event.webEntidad
        self.completo = event.completo
        self.publico = event.publico
        self.inscrito = event.inscrito
        self.idFormularioInscripcion = event.idFormularioInscripcion
        self.categorias = event.categorias
        self.local = event.local
    }
    
    private var descripcionRaw : String
    private var descripcionCortaRaw : String
    
    public var id : Int
    public var titulo : String
    
    public var descripcion : String {
        return Formatter.removeHtml(string: self.descripcionRaw)
    }
    
    public var descripcionHtml : NSAttributedString {
        return Formatter.convertHtml(string: self.descripcionRaw, font: UIFont(name: "Helvetica Neue", size: 18.0)!, color: UIColor.darkGray) ?? NSAttributedString(string: "")
    }
    
    public var descripcionCorta : String {
        return Formatter.removeHtml(string: self.descripcionCortaRaw)
    }
    
    public var descripcionCortaHtml : NSAttributedString {
        return Formatter.convertHtml(string: self.descripcionCortaRaw, font: UIFont(name: "Helvetica Neue", size: 18.0)!, color: UIColor.darkGray) ?? NSAttributedString(string: "")
    }
    
    public var ubicacion : String
    public var fechaInicio : Date
    public var fechaFin : Date
    
    public func fechaInicio(formato: String) -> String {
        
        return Formatter.dateToString(date: self.fechaInicio, format: formato)
    }
    
    public func fechaFin(formato: String) -> String {
        
        return Formatter.dateToString(date: self.fechaFin, format: formato)
    }
    
    public var imagen : String
    public var urlInscripcion : String
    public var aforo : Int
    public var inscritos : Int
    public var latitud : String
    public var longitud : String
    public var ponente : String
    public var direccion : String
    public var precio : String
    public var entidad : String
    public var imagenEntidad : String
    public var webEntidad : String
    public var completo : Bool
    public var publico : Bool
    public var inscrito : Bool
    public var idFormularioInscripcion : Int
    
    public var entidadVisible : Bool {
        
        let entidades = DataBaseManager.shared.getObjects(type: EntityModel.self)
        
        if entidades.count > 1{
            return true
        }
        else{
            return false
        }
        
    }
    
    public var categorias : [EventCategory]
    
    public var local : Bool
    
    public var leida : Bool {
        guard let readNews : [Int] = UserDefaults.standard.array(forKey: "readEvents") as? [Int] else {return false}
        
        return readNews.contains(self.id)
        
    }
    
    public func marcarLeida(){
        
        if var readNews : [Int] = UserDefaults.standard.array(forKey: "readEvents") as? [Int]{
            
            if readNews.contains(self.id) == false{
                readNews.append(self.id)
                
                UserDefaults.standard.set(readNews, forKey: "readEvents")
            }
            
        }
        else{
            UserDefaults.standard.set([self.id], forKey: "readEvents")
        }
    }
    
    public func perteneceCategoria(idCategoria: Int) -> Bool{
        
        for categoria in categorias {
            if categoria.id == idCategoria{
                return true
            }
        }
        return false
    }
    
}
