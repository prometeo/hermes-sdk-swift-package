//
//  HermesEvents.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 09/11/2020.
//

import Foundation
import UIKit

public class HermesEvents: HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        let events = DataBaseManager.shared.getObjects(type: EventModel.self).sorted{$0.fechaInicio > $1.fechaInicio}.map{Event(model: $0)}
        
        return events
    }
    
    public func update(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = EventsRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        let oldEvents = DataBaseManager.shared.getObjects(type: EventModel.self)
                        
                        for oldEvent in oldEvents{
                            if oldEvent.local == false{
                                DataBaseManager.shared.delete(object: oldEvent)
                            }
                        }
                                                
                        var events = [EventModel]()
                        
                        for element in results{
                            let event = EventModel(info: element)
                            
                            events.append(event)
                        }
                        
                        DataBaseManager.shared.save(objects: events)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
    public func subscribeTo(event: Event, contentOwnerId : String, completion: ((SubscribeResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.incorrect)
                }
                
            }
            return
        }
                
        let request = EventsRequest()
        
        request.subscribe(appToken: self.appToken, userToken: userToken, contentOwnerId: contentOwnerId, eventId: String(event.id), completion: {(requestResult) -> () in
            
            DispatchQueue.main.async {
                if completion != nil{
                    completion!(requestResult)
                }
            }
        })
        
    }
    
    public func createLocalEvent(title: String, description: String, startDate: Date?, endDate: Date?){
        
        let event = EventModel()
        
        event.titulo = title
        event.descripcion = description
        
        if startDate != nil{
            event.fechaInicio = Formatter.dateToString(date: startDate!, format: "yyyy-MM-dd'T'HH:mm:sszzzz")
        }
        
        if endDate != nil{
            event.fechaFin = Formatter.dateToString(date: endDate!, format: "yyyy-MM-dd'T'HH:mm:sszzzz")

        }
        
        event.local = true
        event.publico = false
        event.inscrito = true
        event.id = Int.random(in: 1..<1000000) * -1
        event.completo = true
        
        DataBaseManager.shared.save(object: event)
    }
    
    public func create(contentOwner: ContentOwner, categoryId: Int, title: String, description: String, image: UIImage?, startDate: Date, endDate: Date, completion: ((Event?, SendResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(nil, .error)
                }
                
            }
            return
        }
                
        let request = EventsRequest()
        
        let startDateString = Formatter.dateToString(date: startDate, format: "yyyy-MM-dd'T'HH:mm:ssZ")
        let endDateString = Formatter.dateToString(date: endDate, format: "yyyy-MM-dd'T'HH:mm:ssZ")
        
 
        request.createEvent(appToken: self.appToken, userToken: userToken, categoryId: String(categoryId), contentOwnerId: String(contentOwner.id), title: title, description: description, startDate: startDateString, endDate: endDateString, image: image, completion:  {(info, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct {
                    
                    let eventModel = EventModel(info: info)
                    let event = Event(model: eventModel)
                    
                    if completion != nil{
                        completion!(event, .correct)
                    }

                }
                else{
                    if completion != nil{
                        completion!(nil, .error)
                    }
                }
            }
            
        })
 
    
    }
}
