//
//  Area.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 11/11/2020.
//

import Foundation
import UIKit

public class Area : HermesObject {
        
    internal init(model: AreaModel) {
        self.id = model.id
        self.nombre = model.nombre
        self.direccion = model.direccion
        self.correo = model.correo
    }
    
    public var id : Int
    public var nombre : String
    public var direccion : String
    public var correo : String
    
}
