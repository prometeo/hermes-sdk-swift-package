//
//  Entity.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 11/11/2020.
//

import Foundation
import UIKit

public class Entity : HermesObject {
        
    internal init(model: EntityModel) {
        self.id = model.id
        self.nombre = model.nombre
        self.direccion = model.direccion
        self.correo = model.correo
        self.telefono = model.telefono
        self.codigo = model.codigo
        self.color = model.color
        self.colorTxt = model.colorTxt
        self.latitud = model.latitud
        self.longitud = model.longitud
        self.imagen = model.imagen
        self.web = model.web
        self.centros = model.centros.map{Center(model: $0)}
    }
    
    public var id : Int
    public var nombre : String
    public var direccion : String
    public var correo : String
    public var telefono : String
    public var codigo : String
    public var color : String
    public var colorTxt : String
    public var latitud : String
    public var longitud : String
    public var imagen : String
    public var web : String
    public var centros : [Center]
    
}
