//
//  EntityModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 11/11/2020.
//

import Foundation
import RealmSwift

class EntityModel: Object {
    
    @objc dynamic var id : Int = 0
    @objc dynamic var nombre : String = ""
    @objc dynamic var direccion : String  = ""
    @objc dynamic var correo : String  = ""
    @objc dynamic var telefono : String  = ""
    @objc dynamic var codigo : String  = ""
    @objc dynamic var color : String  = ""
    @objc dynamic var colorTxt : String  = ""
    @objc dynamic var latitud : String  = ""
    @objc dynamic var longitud : String  = ""
    @objc dynamic var imagen : String  = ""
    @objc dynamic var web : String  = ""
    
    let centros = List<CenterModel>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.nombre = info["nombre"] as? String ?? ""
        self.direccion = info["direccion"] as? String ?? ""
        self.correo = info["correo"] as? String ?? ""
        self.telefono = info["telefono"] as? String ?? ""
        self.codigo = info["codigo"] as? String ?? ""
        self.color = info["color"] as? String ?? ""
        self.colorTxt = info["color_txt"] as? String ?? ""
        self.latitud = info["latitud"] as? String ?? ""
        self.longitud = info["longitud"] as? String ?? ""
        self.imagen = info["image_route"] as? String ?? ""
        self.web = info["paginaweb"] as? String ?? ""
        
        guard let centros = info["centros"] as? [[String : Any]] else {return}
        
        for centro in centros{
            let centroEntidad = CenterModel(info: centro)
            self.centros.append(centroEntidad)
        }
        
    }
    
    
}
