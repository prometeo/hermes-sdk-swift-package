//
//  CentroModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 11/11/2020.
//

import Foundation
import RealmSwift

class CenterModel: Object {
    
    @objc dynamic var id : Int = 0
    @objc dynamic var nombre : String = ""
    @objc dynamic var direccion : String  = ""
    @objc dynamic var correo : String  = ""

    let areas = List<AreaModel>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.nombre = info["nombre"] as? String ?? ""
        self.direccion = info["direccion"] as? String ?? ""
        self.correo = info["correo"] as? String ?? ""
        
        guard let areas = info["areas"] as? [[String : Any]] else {return}
        
        for area in areas{
            let areaCentro = AreaModel(info: area)
            self.areas.append(areaCentro)
        }
        
    }
    
    
}
