//
//  Center.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 11/11/2020.
//

import Foundation
import UIKit

public class Center : HermesObject {
        
    internal init(model: CenterModel) {
        self.id = model.id
        self.nombre = model.nombre
        self.direccion = model.direccion
        self.correo = model.correo
        self.areas = model.areas.map{Area(model: $0)}
    }
    
    public var id : Int
    public var nombre : String
    public var direccion : String
    public var correo : String
    public var areas : [Area]
    
}
