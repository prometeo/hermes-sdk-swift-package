//
//  HermesEntities.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 11/11/2020.
//

import Foundation

public class HermesEntities : HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        
        let entities = DataBaseManager.shared.getObjects(type: EntityModel.self).map{Entity(model: $0)}
        
        return entities
    }
    
    public func update(completion: ((UpdateResult) -> ())?) {
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = EntitiesRequest()
        
        request.download(userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: EntityModel.self)
                        
                        var entities = [EntityModel]()
                        
                        for element in results{
                            let entity = EntityModel(info: element)
                            
                            entities.append(entity)
                        }
                        
                        DataBaseManager.shared.save(objects: entities)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
    }
    
    
    
}
