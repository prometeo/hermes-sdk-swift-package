//
//  HermesReceiveForms.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 12/11/2020.
//

import Foundation

public class HermesReceivedForms: HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        let files = DataBaseManager.shared.getObjects(type: ReceivedFormModel.self).sorted{$0.fecha > $1.fecha}.map{ReceivedForm(model: $0)}
        
        return files
    }
    
    public func getContentOwners() -> [ContentOwner] {
        
        let contentOwners = DataBaseManager.shared.getObjects(type: ContentOwnerModel.self).map{ContentOwner(model: $0)}
        
        return contentOwners
    }
    
    public func update(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = ReceivedFormsRequest()
        
        request.downloadPending(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            var pendingForms = [Int]()
            
            for form in results {
                if let id = form["id"] as? Int{
                    pendingForms.append(id)
                }
            }
            
            UserDefaults.standard.set(pendingForms, forKey: "pendingReceivedForms")
            
            request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
                
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: ReceivedFormModel.self)
                        
                        var forms = [ReceivedFormModel]()
                        
                        for element in results{
                            let form = ReceivedFormModel(info: element)
                            
                            forms.append(form)
                        }
                        
                        DataBaseManager.shared.save(objects: forms)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
                
                
                
            })
        })
        
 
    
    }
    
    public func changeResponseStatus(response: FormResponse, contentOwnerId: String, status: String, phase: String, title: String, message: String, comment: String, completion: ((SendResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.error)
                }
                
            }
            return
        }
        
        let request = ReceivedFormsRequest()
        
        request.changeResponse(appToken: self.appToken, userToken: userToken, contentOwnerId: contentOwnerId, responseId: String(response.id), status: status, comment: comment, phase: phase, completion: {(requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    let formsModule = HermesReceivedForms()
                    
                    formsModule.update(completion: {(updateResult) -> () in
                        
                        DispatchQueue.main.async {
                            
                            if completion != nil{
                                completion!(.correct)
                            }

                        }
                        
                    })

                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
        
    }
}
