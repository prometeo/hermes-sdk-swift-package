//
//  ReceivedFormsRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 12/11/2020.
//

import Foundation

class ReceivedFormsRequest{
    
    func download(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = FORM_RESPONSES_RECEIVED_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            
            if result.contains("OBJECT_NOT_EXIST"){
                completion(results, .correct)
                
                return
            }
            
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)

        })
        
    }
    
    func downloadPending(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = FORM_RESPONSES_RECEIVED_PENDING_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            
            if result.contains("OBJECT_NOT_EXIST"){
                completion(results, .correct)
                
                return
            }
            
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)

        })
        
    }
        
    func changeResponse(appToken : String, userToken: String, contentOwnerId: String, responseId: String, status: String, comment: String, phase: String, completion: (@escaping(RequestResult) -> ())){
        
        let url = CHANGE_FORM_RESPONSE_ENDPOINT.replacingOccurrences(of: "$0", with: responseId)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "PATCH")
        
        let paramEstado = "estado_aprobacion" + phase
        let paramFecha = "fechaestado" + phase
        let paramAprobador = "aprobador" + phase + "[id]"
        let paramComentario = "comentario" + phase
        
        let fecha = Formatter.dateToString(date: Date(), format: "yyyy-MM-dd'T'HH:mm:ssZ").replacingOccurrences(of: "+", with: "%2B")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: paramEstado, value: status))
        bodyCompontents.queryItems!.append(URLQueryItem(name: paramFecha, value: fecha))
        bodyCompontents.queryItems!.append(URLQueryItem(name: paramAprobador, value: contentOwnerId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: paramComentario, value: comment))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            completion(.correct)
            return
            

        })
        
    }
    
    func sendPush(appToken : String, title: String, text: String, recipients: [String], completion: (@escaping(RequestResult) -> ())){
        
        let url = SEND_PUSH_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded"]
        
        let request = Request(endpoint: url, method: "POST")
        
        var data = [String : Any]()
        
        data["usuarios"] = recipients
        data["grupos"] = recipients
        data["todos"] = false
        
        var json = ""
        
        do{
            let contentData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            
            json = String(data: contentData, encoding: String.Encoding.utf8)!
        }
        catch {
            print(error.localizedDescription)
            
        }
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "appToken", value: appToken))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "titulo", value: title))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "descripcion", value: text))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "destinatarios", value: json))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(.error); return}
            
            guard let str = jsonData["str"] as? String else {completion(.error); return}
            
            if str == "OK"{
                completion(.correct)
                return
            }
            else{
                completion(.error)
                return
            }
            

        })
        
    }
}
