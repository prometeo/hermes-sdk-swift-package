//
//  ReceivedFormModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 12/11/2020.
//

import Foundation
import RealmSwift

class ReceivedFormModel: Object {
    
    @objc dynamic var id : Int = 0
    @objc dynamic var titulo : String = ""
    @objc dynamic var idFormulario : Int = 0
    @objc dynamic var idPropietarioContenido : Int = 0
    @objc dynamic var nombrePropietarioContenido : String = ""
    @objc dynamic var imagenPropietarioContenido : String = ""
    @objc dynamic var nombreEntidad : String = ""
    @objc dynamic var imagenEntidad : String = ""
    @objc dynamic var fecha : String = ""
    @objc dynamic var idAprobador1 : Int = -1
    @objc dynamic var nombreAprobador1 : String = ""
    @objc dynamic var estadoAprobacion1 : Int = -1
    @objc dynamic var idAprobador2 : Int = -1
    @objc dynamic var nombreAprobador2 : String = ""
    @objc dynamic var estadoAprobacion2 : Int = -1
    @objc dynamic var idAprobador3 : Int = -1
    @objc dynamic var nombreAprobador3 : String = ""
    @objc dynamic var estadoAprobacion3 : Int = -1
    @objc dynamic var estadoFinal : Int = -1
    @objc dynamic var observaciones : String = ""
    @objc dynamic var fechaEstado1 : String = ""
    @objc dynamic var fechaEstado2 : String = ""
    @objc dynamic var fechaEstado3 : String = ""
    @objc dynamic var comentario1 : String = ""
    @objc dynamic var comentario2 : String = ""
    @objc dynamic var comentario3 : String = ""
    
    let campos = List<FormFieldModel>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.titulo = info["titulo"] as? String ?? ""
        self.idFormulario = info["id_formulario"] as? Int ?? 0
        self.idPropietarioContenido = info["id_paciente"] as? Int ?? 0
        self.nombrePropietarioContenido = info["nombre_usuario"] as? String ?? ""
        self.imagenPropietarioContenido = info["image_paciente"] as? String ?? ""
        self.nombreEntidad = info["nombre_entidad"] as? String ?? ""
        self.imagenEntidad = info["image_entidad"] as? String ?? ""
        self.fecha = info["fecha"] as? String ?? ""
        self.idAprobador1 = info["id_aprobador1"] as? Int ?? -1
        self.nombreAprobador1 = info["nombre_aprobador1"] as? String ?? ""
        self.estadoAprobacion1 = info["estado_aprobacion1"] as? Int ?? -1
        self.idAprobador2 = info["id_aprobador2"] as? Int ?? -1
        self.nombreAprobador2 = info["nombre_aprobador2"] as? String ?? ""
        self.estadoAprobacion2 = info["estado_aprobacion2"] as? Int ?? -1
        self.idAprobador3 = info["id_aprobador3"] as? Int ?? -1
        self.nombreAprobador3 = info["nombre_aprobador3"] as? String ?? ""
        self.estadoAprobacion3 = info["estado_aprobacion3"] as? Int ?? -1
        self.estadoFinal = info["estado_final"] as? Int ?? -1
        self.observaciones = info["observaciones_publicas"] as? String ?? ""
        self.fechaEstado1 = info["fechaestado1"] as? String ?? ""
        self.fechaEstado2 = info["fechaestado2"] as? String ?? ""
        self.fechaEstado3 = info["fechaestado3"] as? String ?? ""
        self.comentario1 = info["comentario1"] as? String ?? ""
        self.comentario2 = info["comentario2"] as? String ?? ""
        self.comentario3 = info["comentario3"] as? String ?? ""
        
        if var jsonformulario = info["jsonformulario"] as? String{
            
            jsonformulario = jsonformulario.removingPercentEncoding!.replacingOccurrences(of: "+", with: " ")
            
            if let camposFormulario = try? JSONSerialization.jsonObject(with: (jsonformulario.data(using: .utf8))!, options:[]) as? [String:Any]{
                
                if let campos = camposFormulario["campos"] as? [[String : Any]]{
                    
                    for campo in campos{
                        self.campos.append(FormFieldModel(info: campo))
                    }
                }
                
            }
        }
        
        if let aprobador1 = info["aprobador1"] as? [String : Any]{
            let nombre = aprobador1["nombre"] as? String ?? ""
            let apellidos = aprobador1["apellidos"] as? String ?? ""
            let id = aprobador1["id"] as? Int ?? -1
            
            self.idAprobador1 = id
            self.nombreAprobador1 = nombre + " " + apellidos
            
        }
        
        if let aprobador2 = info["aprobador2"] as? [String : Any]{
            let nombre = aprobador2["nombre"] as? String ?? ""
            let apellidos = aprobador2["apellidos"] as? String ?? ""
            let id = aprobador2["id"] as? Int ?? -1
            
            self.idAprobador2 = id
            self.nombreAprobador2 = nombre + " " + apellidos
            
        }
        
        if let aprobador3 = info["aprobador3"] as? [String : Any]{
            let nombre = aprobador3["nombre"] as? String ?? ""
            let apellidos = aprobador3["apellidos"] as? String ?? ""
            let id = aprobador3["id"] as? Int ?? -1
            
            self.idAprobador3 = id
            self.nombreAprobador3 = nombre + " " + apellidos
            
        }
    }
    
    
}
