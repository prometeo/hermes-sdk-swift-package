//
//  ReceivedForm.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 12/11/2020.
//

import Foundation
import UIKit

public class ReceivedForm : HermesObject, FormResponse  {
        
    internal init(model: ReceivedFormModel) {
        self.id = model.id
        self.titulo = model.titulo
        self.idFormulario = model.idFormulario
        self.idPropietarioContenido = model.idPropietarioContenido
        self.nombrePropietarioContenido = model.nombrePropietarioContenido
        self.imagenPropietarioContenido = model.imagenPropietarioContenido
        self.nombreEntidad = model.nombreEntidad
        self.imagenEntidad = model.imagenEntidad
        self.fecha = model.fecha
        self.idAprobador1 = model.idAprobador1
        self.nombreAprobador1 = model.nombreAprobador1
        self.estadoAprobacion1 = model.estadoAprobacion1
        self.idAprobador2 = model.idAprobador2
        self.nombreAprobador2 = model.nombreAprobador2
        self.estadoAprobacion2 = model.estadoAprobacion2
        self.idAprobador3 = model.idAprobador3
        self.nombreAprobador3 = model.nombreAprobador3
        self.estadoAprobacion3 = model.estadoAprobacion3
        self.estadoFinal = model.estadoFinal
        self.campos = model.campos.map{FormField(model: $0)}
        self.observaciones = model.observaciones
        self.fechaEstado1 = model.fechaEstado1
        self.fechaEstado2 = model.fechaEstado2
        self.fechaEstado3 = model.fechaEstado3
        self.comentario1 = model.comentario1
        self.comentario2 = model.comentario2
        self.comentario3 = model.comentario3
    }
    
    public var id : Int
    public var titulo : String
    public var idFormulario : Int
    public var idPropietarioContenido : Int
    public var nombrePropietarioContenido : String
    public var imagenPropietarioContenido : String
    public var nombreEntidad : String
    public var imagenEntidad : String
    public var fecha : String
    
    public func fecha(formato: String) -> String{
        
        var resultado = ""
        
        if let fechaDate = Formatter.stringToDate(string: self.fecha, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
        
        return resultado
    }
    
    public var idAprobador1 : Int
    public var nombreAprobador1 : String
    public var estadoAprobacion1 : Int
    public var idAprobador2 : Int
    public var nombreAprobador2 : String
    public var estadoAprobacion2 : Int
    public var idAprobador3 : Int
    public var nombreAprobador3 : String
    public var estadoAprobacion3 : Int
    public var estadoFinal : Int
    public var campos : [FormField]
    public var observaciones : String
    public var comentario1 : String = ""
    public var comentario2 : String = ""
    public var comentario3 : String = ""
    
    public var entityImageVisible : Bool {
        
        let entidades = DataBaseManager.shared.getObjects(type: EntityModel.self)
        
        if entidades.count > 1{
            return true
        }
        else{
            return false
        }
        
    }
    
    public var leida : Bool {
        guard let readNews : [Int] = UserDefaults.standard.array(forKey: "readReceivedForms") as? [Int] else {return false}
        
        return readNews.contains(self.id)
        
    }
    
    public var pendiente : Bool {
        guard let pendingReceivedForms : [Int] = UserDefaults.standard.array(forKey: "pendingReceivedForms") as? [Int] else {return false}
        
        return pendingReceivedForms.contains(self.id)
    }
    
    public func marcarLeida(){
        
        if var readNews : [Int] = UserDefaults.standard.array(forKey: "readReceivedForms") as? [Int]{
            
            if readNews.contains(self.id) == false{
                readNews.append(self.id)
                
                UserDefaults.standard.set(readNews, forKey: "readReceivedForms")
            }
            
        }
        else{
            UserDefaults.standard.set([self.id], forKey: "readReceivedForms")
        }
    }
    
    public var fechaEstado1 : String
    public var fechaEstado2 : String
    public var fechaEstado3 : String
    
    public func fechaEstado1(formato: String) -> String{
        
        var resultado = ""
        
        if let fechaDate = Formatter.stringToDate(string: self.fechaEstado1, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
        
        return resultado
    }
    
    public func fechaEstado2(formato: String) -> String{
        
        var resultado = ""
        
        if let fechaDate = Formatter.stringToDate(string: self.fechaEstado2, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
        
        return resultado
    }
    
    public func fechaEstado3(formato: String) -> String{
        
        var resultado = ""
        
        if let fechaDate = Formatter.stringToDate(string: self.fechaEstado3, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
        
        return resultado
    }
}
