//
//  DocumentationItem.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 29/6/22.
//

import Foundation
import UIKit
import RealmSwift

public class DocumentationItem : HermesObject {
        
    internal init(model: DocumentationItemModel) {
        self.id = model.id
        self.nombre = model.nombre
        self.descripcion = model.descripcion
        self.publico = model.publico
        self.idPadre = model.idPadre
        self.link = model.link
        self.categorias = model.categorias
        self.idExterno = model.idExterno
        
        switch model.tipo {
        case FOLDER_TYPE:
            self.tipo = .folder
            
        case FILE_TYPE:
            self.tipo = .file
                        
        default:
            self.tipo = .file
        }

    }
    
    public func download(completion: (@escaping(String?, Data?, DownloadResult) -> ())){
        
        let hermesDocumentation = HermesDocumentation()
        
        hermesDocumentation.download(fileId: self.id, completion: {(mime, result, resultCode) -> () in
            
            DispatchQueue.main.async {
                completion(mime, result, resultCode)
            }
        })
        
    }
    
    public var id : Int
    public var nombre : String
    public var descripcion : String
    public var publico : Bool
    public var tipo : DocumentationItemType
    public var idPadre : Int
    public var link : String
    public var categorias : List<Int>
    public var idExterno : String
    

    private let FOLDER_TYPE = "AppBundle\\Entity\\Carpeta"
    private let FILE_TYPE = "AppBundle\\Entity\\File"
    
    public var leida : Bool {
        guard let readNews : [Int] = UserDefaults.standard.array(forKey: "readDocumentation") as? [Int] else {return false}
        
        return readNews.contains(self.id)
        
    }
    
    public func marcarLeida(){
        
        if var readNews : [Int] = UserDefaults.standard.array(forKey: "readDocumentation") as? [Int]{
            
            if readNews.contains(self.id) == false{
                readNews.append(self.id)
                
                UserDefaults.standard.set(readNews, forKey: "readDocumentation")
            }
            
        }
        else{
            UserDefaults.standard.set([self.id], forKey: "readDocumentation")
        }
    }
}

public enum DocumentationItemType {
    case folder
    case file    
}
