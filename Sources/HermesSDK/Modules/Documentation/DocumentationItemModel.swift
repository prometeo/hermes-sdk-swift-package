//
//  DocumentationItemModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 29/6/22.
//

import Foundation
import RealmSwift

class DocumentationItemModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var nombre : String = ""
    @objc dynamic var descripcion : String = ""
    @objc dynamic var publico : Bool = false
    @objc dynamic var tipo : String = ""
    @objc dynamic var idPadre : Int = 0
    @objc dynamic var link : String = ""
    @objc dynamic var idExterno : String = ""
    
    let categorias = List<Int>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.nombre = info["nombre"] as? String ?? ""
        self.descripcion = info["descripcion"] as? String ?? ""
        self.publico = info["publico"] as? Bool ?? false
        self.tipo = info["_type"] as? String ?? ""
        self.link = info["link"] as? String ?? ""
        self.idExterno = info["identificador_externo"] as? String ?? ""
 
        if let categorias = info["categorias"] as? [[String : Any]]{
            for categoria in categorias {
                if let idCategoria = categoria["id"] as? Int{
                    self.categorias.append(idCategoria)
                }
            }
        }
        
        if let parent = info["parent"] as? [String : Any]{
            self.idPadre = parent["id"] as? Int ?? 0
        }
    }
    
    
}
