//
//  HermesDocumentation.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 29/6/22.
//

import Foundation

public class HermesDocumentation: HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func getDocumentsOf(parent: Int, categoryId: [Int]? = nil) -> [DocumentationItem]{
        var documents = DataBaseManager.shared.getObjects(type: DocumentationItemModel.self)
            
        if categoryId != nil{
            var aux = [DocumentationItemModel]()
            
            for document in documents {
                for idCategory in document.categorias{
                    if categoryId!.contains(idCategory){
                        aux.append(document)
                    }
                }
            }
            
            documents = aux
        }
        
        documents = documents.filter{$0.idPadre == parent}
        
        return documents.map{DocumentationItem(model: $0)}
    }
    
    public func get() -> [HermesObject] {
        let documents = DataBaseManager.shared.getObjects(type: DocumentationItemModel.self).map{DocumentationItem(model: $0)}
        
        return documents
    }
    
    public func update(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = DocumentationRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: DocumentationItemModel.self)
                        
                        var documents = [DocumentationItemModel]()
                        
                        for element in results{
                            let document = DocumentationItemModel(info: element)
                            
                            documents.append(document)
                        }
                        
                        DataBaseManager.shared.save(objects: documents)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
    func download(fileId: Int, completion: (@escaping(String?, Data?, DownloadResult) -> ())){
        
        
        guard let userToken = self.userToken else {
            completion(nil, nil, .missingUserToken)
            return
        }
                
        let request = DocumentationRequest()
        
        request.get(fileId: fileId, appToken: self.appToken, userToken: userToken, completion: {(mime, result, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        completion(mime, result, .correct)

                    }
                    
                }
                else{
                    completion(nil, nil, .error)
                }
            }
            
        })
 
    
    }
}
