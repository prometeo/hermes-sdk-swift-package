//
//  ExpenseItemModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 23/12/22.
//

import Foundation
import RealmSwift

class ExpenseItemModel: Object {
    
    @objc dynamic var id : Int = 0
    @objc dynamic var tipo : Int = 0
    @objc dynamic var titulo : String = ""
    @objc dynamic var recorrido : String = ""
    @objc dynamic var importe : Double = 0.0
    @objc dynamic var foto : String = ""
    @objc dynamic var kilometros : Double = 0.0
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.tipo = info["item_gasto_type"] as? Int ?? 0
        self.titulo = info["titulo"] as? String ?? ""
        self.recorrido = info["recorrido"] as? String ?? ""
        self.importe = info["importe"] as? Double ?? 0.0
        self.foto = info["foto"] as? String ?? ""
        self.kilometros = info["kilometros"] as? Double ?? 0.0

    }
    
    
    
}
