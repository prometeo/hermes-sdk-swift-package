//
//  ExpensesRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 17/2/23.
//

import Foundation

class ExpensesRequest{
    
    func download(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = EXPENSES_ENDPOINT + "?$locale=" + LOCALE
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            
            if result.contains("OBJECT_NOT_EXIST"){
                completion(results, .correct)
                
                return
            }
            
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
    
    func create(appToken: String, userToken: String, fecha: String, proyectoId: String, destino: String, motivo: String, items: [ExpenseItem], completion: (@escaping([String : Any], RequestResult) -> ())){
        
        let url = EXPENSES_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "fecha", value: fecha))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "proyecto[id]", value: proyectoId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "destino", value: destino))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "motivo", value: motivo))
        
        var index = 0
        
        for item in items {
            
            bodyCompontents.queryItems!.append(URLQueryItem(name: "items[" + String(index) + "]" + "[tipo]", value: String(item.tipo.rawValue)))
            bodyCompontents.queryItems!.append(URLQueryItem(name: "items[" + String(index) + "]" + "[titulo]", value: item.titulo))
            bodyCompontents.queryItems!.append(URLQueryItem(name: "items[" + String(index) + "]" + "[recorrido]", value: item.recorrido))
            bodyCompontents.queryItems!.append(URLQueryItem(name: "items[" + String(index) + "]" + "[importe]", value: String(item.importe)))
            bodyCompontents.queryItems!.append(URLQueryItem(name: "items[" + String(index) + "]" + "[foto]", value: item.foto))
            bodyCompontents.queryItems!.append(URLQueryItem(name: "items[" + String(index) + "]" + "[kilometros]", value: String(item.kilometros)))
            bodyCompontents.queryItems!.append(URLQueryItem(name: "items[" + String(index) + "]" + "[item_gasto_type]", value: String(item.tipo.rawValue)))
            
            index += 1
        }
        
        let body = bodyCompontents.query?.data(using: .utf8)
        

        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [String : Any]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
    
    func edit(appToken: String, userToken: String, id: Int, fecha: String, proyectoId: String, destino: String, motivo: String, items: [ExpenseItem], completion: (@escaping([String : Any], RequestResult) -> ())){
        
        let url = EXPENSES_ENDPOINT + "/" + String(id)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "PATCH")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "fecha", value: fecha))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "proyecto[id]", value: proyectoId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "destino", value: destino))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "motivo", value: motivo))
        
        var index = 0
        
        for item in items {
            
            if item.id != -1{
                bodyCompontents.queryItems!.append(URLQueryItem(name: "items[" + String(index) + "]" + "[id]", value: String(item.id)))
            }
            
            bodyCompontents.queryItems!.append(URLQueryItem(name: "items[" + String(index) + "]" + "[tipo]", value: String(item.tipo.rawValue)))
            bodyCompontents.queryItems!.append(URLQueryItem(name: "items[" + String(index) + "]" + "[titulo]", value: item.titulo))
            bodyCompontents.queryItems!.append(URLQueryItem(name: "items[" + String(index) + "]" + "[recorrido]", value: item.recorrido))
            bodyCompontents.queryItems!.append(URLQueryItem(name: "items[" + String(index) + "]" + "[importe]", value: String(item.importe)))
            bodyCompontents.queryItems!.append(URLQueryItem(name: "items[" + String(index) + "]" + "[foto]", value: item.foto))
            bodyCompontents.queryItems!.append(URLQueryItem(name: "items[" + String(index) + "]" + "[kilometros]", value: String(item.kilometros)))
            bodyCompontents.queryItems!.append(URLQueryItem(name: "items[" + String(index) + "]" + "[item_gasto_type]", value: String(item.tipo.rawValue)))
            
            index += 1
        }
        
        let body = bodyCompontents.query?.data(using: .utf8)
        

        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [String : Any]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            
        })
        
    }
    
    
    
}
