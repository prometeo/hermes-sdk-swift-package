//
//  ExpenseItem.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 23/12/22.
//

import Foundation
import UIKit
import RealmSwift

public class ExpenseItem {
    
    public init() {
        self.id = -1
        self.tipo = .otros
        self.recorrido = ""
        self.titulo = ""
        self.importe = 0.0
        self.foto = ""
        self.kilometros = 0.0
    }
    
    internal init(model: ExpenseItemModel) {
        self.id = model.id
        self.tipo = ExpenseType(rawValue: model.tipo) ?? .otros
        self.recorrido = model.recorrido
        self.titulo = model.titulo
        self.importe = model.importe
        self.foto = model.foto
        self.kilometros = model.kilometros
    }
    
    public var id : Int
    public var titulo : String
    public var recorrido : String
    public var tipo : ExpenseType
    public var importe : Double
    public var foto : String
    public var kilometros : Double

}

public enum ExpenseType : Int {
    case kilometraje = 0
    case dietas = 1
    case parking = 2
    case otros = 3
}
