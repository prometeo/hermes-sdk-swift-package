//
//  ExpenseModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 23/11/22.
//

import Foundation
import RealmSwift

class ExpenseModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var fecha : String = ""
    @objc dynamic var proyecto : String = ""
    @objc dynamic var destino : String = ""
    @objc dynamic var motivo : String = ""
    @objc dynamic var proyectoId : Int  = 0
    
    let items = List<ExpenseItemModel>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.fecha = info["fecha"] as? String ?? ""
        
        self.destino = info["destino"] as? String ?? ""
        self.motivo = info["motivo"] as? String ?? ""

        if let items = info["items"] as? [[String : Any]]{
            for item in items{
                let itemModel = ExpenseItemModel(info: item)
                
                self.items.append(itemModel)
            }
        }
        
        if let project = info["proyecto"] as? [String : Any] {
            self.proyecto = project["nombre"] as? String ?? ""
            self.proyectoId = project["id"] as? Int ?? -1
        }
    }
    
    
    
}
