//
//  Expense.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 23/11/22.
//

import Foundation
import UIKit
import RealmSwift

public class Expense : HermesObject {
    
    public init() {
        self.id = 0
        self.fecha = ""
        self.proyecto = ""
        self.destino = ""
        self.motivo = ""
        self.items = [ExpenseItem]()
        self.proyectoId = 0
    }
    
    internal init(model: ExpenseModel) {
        self.id = model.id
        self.fecha = model.fecha
        self.proyecto = model.proyecto
        self.destino = model.destino
        self.motivo = model.motivo
        self.items = model.items.map{ExpenseItem(model: $0)}
        self.proyectoId = model.proyectoId
    }
    
    public var id : Int
    public var fecha : String
    public var proyecto : String
    public var destino : String
    public var motivo : String
    public var items : [ExpenseItem]
    public var proyectoId : Int
    
    public func fecha(formato: String) -> String {
        var resultado = ""
    
        if let fechaDate = Formatter.stringToDate(string: self.fecha, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
    
        return resultado
    }
}

