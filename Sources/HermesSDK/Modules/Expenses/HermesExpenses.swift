//
//  HermesExpenses.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 23/11/22.
//

import Foundation

public class HermesExpenses: HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    
    public init() {
        self.userToken = nil
        
        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        return DataBaseManager.shared.getObjects(type: ExpenseModel.self).map{Expense(model: $0)}
    }
    
    public func update(completion: ((UpdateResult) -> ())?) {
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = ExpensesRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: ExpenseModel.self)
                        
                        var expenseItems = [ExpenseModel]()
                        
                        for element in results{
                            let expenseItem = ExpenseModel(info: element)
                            
                            expenseItems.append(expenseItem)
                        }
                        
                        DataBaseManager.shared.save(objects: expenseItems)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
    }
    
    public func saveExpense(fecha: Date, proyectoId: Int, destino: String, motivo: String, items: [ExpenseItem], completion: ((UpdateResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let dateString = Formatter.dateToString(date: fecha, format: "yyyy-MM-dd'T'HH:mm:ssZ").replacingOccurrences(of: "+", with: "%2B")
        
        
        let request = ExpensesRequest()
        
        request.create(appToken: self.appToken, userToken: userToken, fecha: dateString, proyectoId: String(proyectoId), destino: destino, motivo: motivo, items: items, completion: {(data, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    if completion != nil{
                        completion!(.correct)
                    }
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
        
        
    }
    
    public func editExpense(expense: Expense, completion: ((UpdateResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        
        let dateString = expense.fecha(formato: "yyyy-MM-dd'T'HH:mm:ssZ").replacingOccurrences(of: "+", with: "%2B")
        
        
        let request = ExpensesRequest()
        
        request.edit(appToken: self.appToken, userToken: userToken, id: expense.id, fecha: dateString, proyectoId: String(expense.proyectoId), destino: expense.destino, motivo: expense.motivo, items: expense.items, completion: {(data, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    if completion != nil{
                        completion!(.correct)
                    }
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
        
        
    }
    
    
    
}
