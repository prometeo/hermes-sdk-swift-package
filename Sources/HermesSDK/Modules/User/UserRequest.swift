//
//  LoginRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 30/10/2020.
//

import Foundation

class UserRequest{
    
    func login(appToken: String, entityToken: String, user: String, password: String, completion: (@escaping(String?, [String : Any]?, LoginResult) -> ())){
        
        let url = LOGIN_ENDPOINT + "?email=" + user + "&password=" + password
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "entidadToken" : entityToken]
        
        let request = Request(endpoint: url, method: "GET")
                
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var token : String?
            var userInfo : [String : Any]?
            
            guard error == nil else {completion(token, userInfo, .error); return}
            
            if responseCode == .forbidden{
                completion(token, userInfo, .needsValidation)
            }
            else{
                guard responseCode == .success else {completion(token, userInfo, .error); return}
                
                guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(token, userInfo, .error); return}
                
                userInfo = jsonData
                
                token = jsonData["user_token"] as? String
                
                if token != nil{
                    completion(token, userInfo, .correct)
                }
                else{
                    completion(token, userInfo, .incorrect)
                }
            }
            
            

        })
        
    }
    
    func loginWithPost(appToken: String, entityToken: String, user: String, password: String, completion: (@escaping(String?, [String : Any]?, LoginResult) -> ())){
        
        let url = LOGIN_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "entidadToken" : entityToken]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "email", value: user))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "password", value: password))

 
        let body = bodyCompontents.query?.data(using: .utf8)
                
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var token : String?
            var userInfo : [String : Any]?
            
            guard error == nil else {completion(token, userInfo, .error); return}
            
            if responseCode == .forbidden{
                completion(token, userInfo, .needsValidation)
            }
            else{
                guard responseCode == .success else {completion(token, userInfo, .error); return}
                
                guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(token, userInfo, .error); return}
                
                userInfo = jsonData
                
                token = jsonData["user_token"] as? String
                
                if token != nil{
                    completion(token, userInfo, .correct)
                }
                else{
                    completion(token, userInfo, .incorrect)
                }
            }
            
            

        })
        
    }
    
    func register(appToken: String, appId: String, entityToken: String, user: String, name: String, surname: String, password: String, completion: (@escaping(RequestResult) -> ())){
        
        let url = REGISTER_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "entidadToken" : entityToken]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "app[id]", value: appId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "nombre", value: name))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "apellido", value: surname))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "correo", value: user))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "plain_password", value: password))
        
 
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            completion(.correct)
            return
            

        })
        
    }
    
    func  registerWithContentOwner(appToken: String, appId: String, entityToken: String, entityId: String, user: String, name: String, surname: String, password: String, pushToken: String, rol: String?, jsonEmpleoDemandante: String?, jsonEmpleoEmpresa: String?, completion: (@escaping(RequestResult) -> ())){
        
        let url = REGISTER_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "entidadToken" : entityToken]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "app[id]", value: appId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "nombre", value: name))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "apellido", value: surname))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "correo", value: user))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "plain_password", value: password))
        
 
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(.error); return}
            
            guard let userId = jsonData["id"] as? Int else {completion(.error); return}
            guard let userToken = jsonData["user_token"] as? String else {completion(.error); return}
            
            let alias = (name + " " + surname).getAcronyms()
            
            self.createAndAssociateContentOwner(appToken: appToken, userToken: userToken, userId: userId, entityId: entityId, alias: alias, name: name, surname: surname, rol: rol, jsonEmpleoDemandante: jsonEmpleoDemandante, jsonEmpleoEmpresa: jsonEmpleoEmpresa, completion: completion)

            

        })
        
    }
    
    private func createAndAssociateContentOwner(appToken: String, userToken: String, userId: Int, entityId: String, alias: String, name: String, surname: String, rol: String?, jsonEmpleoDemandante: String?, jsonEmpleoEmpresa: String?, completion: (@escaping(RequestResult) -> ())){
        
        let url = CREATE_AND_ASSOCIATE_CONTENT_OWNER_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "paciente[entidad][id]", value: entityId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "paciente[alias]", value: alias))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "paciente[type]", value: "propcontenido"))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "paciente[nombre]", value: name))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "paciente[apellidos]", value: surname))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "usuario_movil[id]", value: String(userId)))
 
        if rol != nil{
            bodyCompontents.queryItems!.append(URLQueryItem(name: "paciente[roles][0]", value: rol!))
        }
        
        if jsonEmpleoDemandante != nil{
            bodyCompontents.queryItems!.append(URLQueryItem(name: "paciente[json_empleo_demandante]", value: jsonEmpleoDemandante!))
        }
        
        if jsonEmpleoEmpresa != nil{
            bodyCompontents.queryItems!.append(URLQueryItem(name: "paciente[json_empleo_empresa]", value: jsonEmpleoEmpresa!))
        }
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            if error != nil || responseCode != .success{
                self.deleteUser(appToken: appToken, userToken: userToken, userId: userId, completion: {(deleteResult) -> () in
                    
                    completion(.error)
                    return
                })
            }
            else{
                completion(.correct)
                return
            }
            
            
            

        })
        
        
    }
    
    func resetPassword(entityId: String, email: String, completion: (@escaping(RequestResult) -> ())){
        
        let url = RESET_PASSWORD_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded"]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "idEntidad", value: entityId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "email", value: email))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
  
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(.error); return}
            
            guard let str = jsonData["str"] as? String else {completion(.error); return}
            
            if str == "OK"{
                completion(.correct)
                return
            }
            else{
                completion(.error)
                return
            }
            

        })
        
    }
    
    func changePassword(appToken: String, userToken: String, userId: Int, newPassword: String, completion: (@escaping(RequestResult) -> ())){
        
        let url = CHANGE_PASSWORD_ENDPOINT + String(userId)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "PATCH")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "plain_password", value: newPassword))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            completion(.correct)
            return
            

        })
        
    }
    
    func registerDevice(appToken: String, userToken: String, userId: String, pushToken: String, completion: (@escaping(RequestResult) -> ())){
        
        let url = REGISTER_DEVICE_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
                
        let request = Request(endpoint: url, method: "POST")
        
        let deviceIdentifier = UUID().uuidString
        
        UserDefaults.standard.set(deviceIdentifier, forKey: "deviceIdentifier")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "usuario_movil[id]", value: userId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "dispositivo_movil[nombre]", value: deviceIdentifier))
                
        if pushToken != "" {
            bodyCompontents.queryItems!.append(URLQueryItem(name: "dispositivo_movil[android_token]", value: pushToken))
        }
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            completion(.correct)
            return
            

        })
    }
    
    func unregisterDevice(userToken: String, completion: (@escaping(RequestResult) -> ())){
        
        let url = UNREGISTER_DEVICE_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded"]
        
        let request = Request(endpoint: url, method: "POST")
        
        let deviceIdentifier = UserDefaults.standard.string(forKey: "deviceIdentifier") ?? ""
                
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "userToken", value: userToken))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "identificadorUnico", value: deviceIdentifier))
                
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown

            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(.error); return}
            
            guard let str = jsonData["str"] as? String else {completion(.error); return}
            
            if str == "OK"{
                completion(.correct)
                return
            }
            else{
                completion(.error)
                return
            }
            

        })
    }
    
    func disableUser(appToken: String, userToken: String, completion: (@escaping(RequestResult) -> ())){
        
        let url = DISABLE_USER_ENDPOINT.replacingOccurrences(of: "$0", with: userToken)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
                
        let request = Request(endpoint: url, method: "POST")
        

        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            completion(.correct)
            return
            

        })
    }
    
    func deleteUser(appToken: String, userToken: String, userId: Int, completion: (@escaping(RequestResult) -> ())){
        
        let url = DELETE_USER_ENDPOINT + String(userId)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
                
        let request = Request(endpoint: url, method: "DELETE")
        

        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            completion(.correct)
            return
            

        })
    }
}
