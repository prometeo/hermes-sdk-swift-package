//
//  HermesUser.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 30/10/2020.
//

import Foundation

public class HermesUser {
    
    private var user : String?
    private var password : String?
    private var appToken : String!
    private var entityId : String!
    private var keepSession : Bool = false
    private var name : String?
    private var surname : String?
    private var pushToken : String?
    private var isAnonymous : Bool = false
    private var entityToken : String!
    private var appId : String!
    private var rol : String?
    private var jsonEmpleoDemandante : String?
    private var jsonEmpleoEmpresa : String?
    
    public init() {
        self.user = nil
        self.password = nil
        self.appToken = APP_TOKEN
        self.entityId = ENTITY_ID
        self.entityToken = ENTITY_TOKEN
        self.appId = APP_ID
    }
    
    public func user(_ user : String) -> HermesUser {
        
        self.user = user
        
        return self
    }
    
    public func password(_ password : String) -> HermesUser {
        
        self.password = password
        
        return self
    }
    
    public func name(_ name : String) -> HermesUser {
        
        self.name = name
        
        return self
    }
    
    public func surname(_ surname : String) -> HermesUser {
        
        self.surname = surname
        
        return self
    }
        
    public func pushToken(_ pushToken : String?) -> HermesUser {
        
        self.pushToken = pushToken
        
        return self
    }
        
    public func keepSession(_ keepSession : Bool) -> HermesUser {
        
        self.keepSession = keepSession
        
        return self
    }
    
    public func isAnonymous(_ anonymous : Bool) -> HermesUser {
        
        self.isAnonymous = anonymous
        
        return self
    }
    
    public func rol(_ rol : String) -> HermesUser {
        
        self.rol = rol
        
        return self
    }
    
    public func jsonEmpleoDemandante(_ jsonEmpleoDemandante : String) -> HermesUser {
        
        self.jsonEmpleoDemandante = jsonEmpleoDemandante
        
        return self
    }
    
    public func jsonEmpleoEmpresa(_ jsonEmpleoEmpresa : String) -> HermesUser {
        
        self.jsonEmpleoEmpresa = jsonEmpleoEmpresa
        
        return self
    }
    
    public func login(completion: (@escaping(_ loginResult: LoginResult) -> ())){
        
        guard let user = self.user else {
            completion(.error)
            return
        }
        
        guard let password = self.password else{
            completion(.error)
            return
        }
        
        let userRequest = UserRequest()
        
        userRequest.login(appToken: self.appToken, entityToken: self.entityToken, user: user, password: password, completion: {(userToken, userInfo, loginResult) in
            
            if userToken != nil, userInfo != nil{
                
                let userId = userInfo!["id"] as? Int
                
                if self.pushToken != nil && userId != nil {
                    userRequest.registerDevice(appToken: self.appToken, userToken: userToken!, userId: String(userId!), pushToken: self.pushToken!, completion: {(registerResult) -> () in
                        
                        if registerResult == .correct{
                            print("DEVICE REGISTERED WITH TOKEN: " + self.pushToken!)
                        }
                        else{
                            print("ERROR DEVICE REGISTER")
                        }
                    })
                }
                
                let userModel = UserModel(userToken: userToken!, info: userInfo!, mantenerSesion: self.keepSession)
                
                userModel.anonimo = self.isAnonymous
                
                DispatchQueue.main.async {
                    DataBaseManager.shared.deleteObjects(type: UserModel.self)
                    DataBaseManager.shared.save(object: userModel)
                    
                    let contentOwnersModule = HermesContentOwners()
                    let entitiesModule = HermesEntities()
                    let formsModule = HermesForms()
                    let appointmentsModule = HermesAppointments()
                    let resourcesModule = HermesResources()
                    let cmsModule = HermesCMS()
                    let menuModule = HermesMenu()
                    
                    menuModule.update(completion: {(menuUpdateResult) -> () in
                        contentOwnersModule.update(completion: {(updateResult) -> () in
                            entitiesModule.update(completion: {(updateEntitiesResult) -> () in
                                formsModule.update(completion: {(updateFormsResult) -> () in
                                    appointmentsModule.update(completion: {(updateAppointmentsResult) -> () in
                                        resourcesModule.update(completion: {(updateResourceResult) -> () in
                                            cmsModule.update(completion: {(updateCmsResult) -> () in
                                                DispatchQueue.main.async {
                                                    completion(loginResult)
                                                }
                                            })
                                            
                                        })
                                    })
                                })
                                
                                
                            })
                            
                        })
                    })
                    
                }
                
               
            }
            else{
                DispatchQueue.main.async {
                    completion(loginResult)
                }
            }
            
            
        })
    
    }
    
    public func loginWithPost(completion: (@escaping(_ loginResult: LoginResult) -> ())){
        
        guard let user = self.user else {
            completion(.error)
            return
        }
        
        guard let password = self.password else{
            completion(.error)
            return
        }
        
        let userRequest = UserRequest()
        
        userRequest.loginWithPost(appToken: self.appToken, entityToken: self.entityToken, user: user, password: password, completion: {(userToken, userInfo, loginResult) in
            
            if userToken != nil, userInfo != nil{
                
                let userId = userInfo!["id"] as? Int
                
                if self.pushToken != nil && userId != nil {
                    userRequest.registerDevice(appToken: self.appToken, userToken: userToken!, userId: String(userId!), pushToken: self.pushToken!, completion: {(registerResult) -> () in
                        
                        if registerResult == .correct{
                            print("DEVICE REGISTERED WITH TOKEN: " + self.pushToken!)
                        }
                        else{
                            print("ERROR DEVICE REGISTER")
                        }
                    })
                }
                
                let userModel = UserModel(userToken: userToken!, info: userInfo!, mantenerSesion: self.keepSession)
                
                userModel.anonimo = self.isAnonymous
                
                DispatchQueue.main.async {
                    DataBaseManager.shared.deleteObjects(type: UserModel.self)
                    DataBaseManager.shared.save(object: userModel)
                    
                    let contentOwnersModule = HermesContentOwners()
                    let entitiesModule = HermesEntities()
                    let formsModule = HermesForms()
                    let appointmentsModule = HermesAppointments()
                    let resourcesModule = HermesResources()
                    let cmsModule = HermesCMS()
                    let menuModule = HermesMenu()
                    
                    menuModule.update(completion: {(menuUpdateResult) -> () in
                        contentOwnersModule.update(completion: {(updateResult) -> () in
                            entitiesModule.update(completion: {(updateEntitiesResult) -> () in
                                formsModule.update(completion: {(updateFormsResult) -> () in
                                    appointmentsModule.update(completion: {(updateAppointmentsResult) -> () in
                                        resourcesModule.update(completion: {(updateResourceResult) -> () in
                                            cmsModule.update(completion: {(updateCmsResult) -> () in
                                                DispatchQueue.main.async {
                                                    completion(loginResult)
                                                }
                                            })
                                            
                                        })
                                    })
                                })
                                
                                
                            })
                            
                        })
                    })
                    
                }
                
               
            }
            else{
                DispatchQueue.main.async {
                    completion(loginResult)
                }
            }
            
            
        })
    
    }
    
    public func registerWithContentOwner(completion: (@escaping(_ registerResult: LoginResult) -> ())){
        
        guard let user = self.user else {
            completion(.error)
            return
        }
        
        guard let password = self.password else{
            completion(.error)
            return
        }
        
        guard let name = self.name else{
            completion(.error)
            return
        }
        
        guard let surname = self.surname else{
            completion(.error)
            return
        }
        
        guard let pushToken = self.pushToken else{
            completion(.error)
            return
        }
        
        let userRequest = UserRequest()
        
        userRequest.registerWithContentOwner(appToken: self.appToken, appId: self.appId, entityToken: self.entityToken, entityId: self.entityId, user: user, name: name, surname: surname, password: password, pushToken: pushToken, rol: self.rol, jsonEmpleoDemandante: self.jsonEmpleoDemandante, jsonEmpleoEmpresa: self.jsonEmpleoEmpresa, completion: {(requestResult) -> () in
            
            if requestResult == .correct{
                self.login(completion: {(loginResult) -> () in
                    
                    DispatchQueue.main.async {
                        completion(loginResult)
                    }
                    
                })
            }
            else{
                DispatchQueue.main.async {
                    completion(.error)
                }
                
            }
        })
        
    }
    
    public func register(completion: (@escaping(_ registerResult: LoginResult) -> ())){
        
        guard let user = self.user else {
            completion(.error)
            return
        }
        
        guard let password = self.password else{
            completion(.error)
            return
        }
        
        guard let name = self.name else{
            completion(.error)
            return
        }
        
        guard let surname = self.surname else{
            completion(.error)
            return
        }
        

        let userRequest = UserRequest()
        
        userRequest.register(appToken: self.appToken, appId: self.appId, entityToken: self.entityToken, user: user, name: name, surname: surname, password: password, completion: {(requestResult) -> () in
            
            if requestResult == .correct{
                self.login(completion: {(loginResult) -> () in
                    
                    
                    DispatchQueue.main.async {
                        completion(loginResult)
                    }
                    
                })
            }
            else{
                DispatchQueue.main.async {
                    completion(.error)
                }
                
            }
        })
        
    }
    
    public func resetPassword(email: String, completion: (@escaping(_ sendResult: SendResult) -> ())){
        
        let userRequest = UserRequest()
        userRequest.resetPassword(entityId: self.entityId, email: email, completion: {(requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    completion(.correct)
                }
                else{
                    completion(.error)
                }
            }
        })

    
    }
    
    public func changePassword(newPassword: String, completion: (@escaping(_ sendResult: SendResult) -> ())){
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        guard let user = users.first else {
            completion(.error)
            return
        }
        
 
        let userRequest = UserRequest()
        
        userRequest.changePassword(appToken: self.appToken, userToken: user.userToken, userId: user.id, newPassword: newPassword,  completion: {(requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    completion(.correct)
                }
                else{
                    completion(.error)
                }
            }
            
        })
        
        
    }
    
    public func getUser() -> User?{
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        guard let user = users.first else {
            return nil
        }
        
        return User(model: user)
        
    }
    
    public func isSessionStarted() -> Bool{
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        guard let user = users.first else {
            return false
        }
        
        if user.anonimo == true{
            return true
        }
        
        return user.mantenerSesion
    }
    
    public func logout(){
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            let userRequest = UserRequest()
            
            userRequest.unregisterDevice(userToken: user.userToken, completion: {(unregisterResult) -> () in
                
                if unregisterResult == .correct{
                    print("DEVICE UNREGISTERED")
                }
                else{
                    print("ERROR UNREGISTER DEVICE")
                }
            })
        }
        
        
        DataBaseManager.shared.deleteObjects(type: UserModel.self)
        DataBaseManager.shared.deleteObjects(type: MessageModel.self)
        DataBaseManager.shared.deleteObjects(type: EntityModel.self)
        DataBaseManager.shared.deleteObjects(type: ContentOwnerModel.self)
        DataBaseManager.shared.deleteObjects(type: EventModel.self)
        DataBaseManager.shared.deleteObjects(type: NewsItemModel.self)
        DataBaseManager.shared.deleteObjects(type: NewsCategoryModel.self)
        DataBaseManager.shared.deleteObjects(type: FileModel.self)
        DataBaseManager.shared.deleteObjects(type: IncidenceModel.self)
        DataBaseManager.shared.deleteObjects(type: IncidenceProfileModel.self)
        DataBaseManager.shared.deleteObjects(type: AppointmentModel.self)
        DataBaseManager.shared.deleteObjects(type: DirectoryItemModel.self)
        DataBaseManager.shared.deleteObjects(type: FormModel.self)
        DataBaseManager.shared.deleteObjects(type: FormRecipientModel.self)
        DataBaseManager.shared.deleteObjects(type: FormFieldModel.self)
        DataBaseManager.shared.deleteObjects(type: SentFormModel.self)
        DataBaseManager.shared.deleteObjects(type: ReceivedFormModel.self)
        DataBaseManager.shared.deleteObjects(type: AllContentOwnerModel.self)
        
        UserDefaults.standard.set(nil, forKey: "readEvents")
        UserDefaults.standard.set(nil, forKey: "readNews")
        UserDefaults.standard.set(nil, forKey: "readReceivedForms")
        UserDefaults.standard.set(nil, forKey: "readReceivedForms")
        UserDefaults.standard.set(nil, forKey: "readDocumentation")
        UserDefaults.standard.set(nil, forKey: "readOffers")
        UserDefaults.standard.set(nil, forKey: "favoriteOffers")
        UserDefaults.standard.set(nil, forKey: "myNetworkFavorites")
        

        
    }
    
    public func disable(completion: ((UpdateResult) -> ())?){
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            let userRequest = UserRequest()
            
            userRequest.disableUser(appToken: self.appToken, userToken: user.userToken, completion: {(result) -> () in
                
                DispatchQueue.main.async {
                    if result == .correct{
                        if completion != nil{
                            completion!(.correct)
                        }
                    }
                    else{
                        if completion != nil{
                            completion!(.error)
                        }
                        
                    }
                }
                
            })
        }
        
    }
}
