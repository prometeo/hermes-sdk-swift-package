//
//  User.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 30/10/2020.
//

import Foundation

public class User {
        
    internal init(model: UserModel) {
        self.userToken = model.userToken
        self.nombreUsuario = model.nombre + " " + model.apellido
        self.nombre = model.nombre
        self.apellido = model.apellido
        self.alias = model.alias
        self.ciudad = model.ciudad
        self.codigoPostal = model.codigoPostal
        self.telefono = model.telefono
        self.provincia = model.provincia
        self.correo = model.correo
        self.esAnonimo = model.anonimo
        self.estaAsociado = model.estaAsociado()
    }
    
    public var userToken : String
    public var nombreUsuario : String
    public var nombre : String
    public var apellido : String
    public var alias : String
    public var ciudad : String
    public var codigoPostal : String
    public var telefono : String
    public var provincia : String
    public var correo : String
    public var esAnonimo : Bool
    public var estaAsociado : Bool
}
