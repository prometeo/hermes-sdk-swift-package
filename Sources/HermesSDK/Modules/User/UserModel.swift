//
//  User.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 30/10/2020.
//

import Foundation
import RealmSwift

class UserModel: Object {
    
    @objc dynamic var id : Int = -1
    @objc dynamic var userToken : String = ""
    @objc dynamic var nombre : String = ""
    @objc dynamic var apellido : String  = ""
    @objc dynamic var alias : String  = ""
    @objc dynamic var ciudad : String  = ""
    @objc dynamic var codigoPostal : String  = ""
    @objc dynamic var telefono : String  = ""
    @objc dynamic var provincia : String  = ""
    @objc dynamic var correo : String  = ""
    @objc dynamic var mantenerSesion : Bool = false
    @objc dynamic var anonimo : Bool = false
    
    override static func primaryKey() -> String? {
        return "userToken"
    }
    
    override required init() {
        
    }
    
    init(userToken: String, info: [String : Any], mantenerSesion: Bool) {
        
        self.id = info["id"] as? Int ?? -1
        self.userToken = userToken
        self.nombre = info["nombre"] as? String ?? ""
        self.apellido = info["apellido"] as? String ?? ""
        self.ciudad = info["ciudad"] as? String ?? ""
        self.codigoPostal = info["codigo_postal"] as? String ?? ""
        self.telefono = info["telefono"] as? String ?? ""
        self.provincia = info["provincia"] as? String ?? ""
        self.correo = info["correo"] as? String ?? ""
        self.mantenerSesion = mantenerSesion
        
        let nombreCompleto = self.nombre + " " + self.apellido
        self.alias = nombreCompleto.getAcronyms()
    }
    
    func estaAsociado() -> Bool{
        
        let contentOwners = DataBaseManager.shared.getObjects(type: ContentOwnerModel.self)
        
        return (contentOwners.count > 0)
    }
    
}
