//
//  HermesChat.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 02/12/2020.
//

import Foundation
import UIKit

public class HermesChat : HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    private var entityId : String!
    private var entityToken : String!
    
    public var chatToEntity : Bool = false
    public var chatToPC : Bool = false
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        self.entityId = ENTITY_ID
        self.entityToken = ENTITY_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
        self.chatToEntity = UserDefaults.standard.bool(forKey: "chatToEntity")
        self.chatToPC = UserDefaults.standard.bool(forKey: "chatToPC")
        
    }
    
    public func get() -> [HermesObject] {
        let chats = DataBaseManager.shared.getObjects(type: ChatModel.self).map{Chat(model: $0)}
        
        return chats
    }
    
    public func update(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = ChatRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            request.downloadEntity(appToken: self.appToken, userToken: userToken, entityId: self.entityId, completion: {(resultsEntity, requestEntityResult) -> () in
                
                self.updateDestinationContentOwners(completion: {(contentOwnersResult) -> () in
                    DispatchQueue.main.async {
                        if requestResult == .correct{
                            
                            DispatchQueue.main.async {
                                
                                if let entity = resultsEntity.first{
                                    self.chatToEntity = entity["permitir_iniciar_chat_pc_a_entidad"] as? Bool ?? false
                                    self.chatToPC = entity["permitir_iniciar_chat_entre_pc"] as? Bool ?? false
                                    
                                    UserDefaults.standard.set(self.chatToEntity, forKey: "chatToEntity")
                                    UserDefaults.standard.set(self.chatToPC, forKey: "chatToPC")
                                }
                                
                                DataBaseManager.shared.deleteObjects(type: ChatModel.self)
                                
                                var chats = [ChatModel]()
                                
                                for element in results{
                                    let chat = ChatModel(info: element)
                                    
                                    chats.append(chat)
                                    
                                    self.updateMessages(chat: Chat(model: chat), completion: nil)
                                }
                                
                                DataBaseManager.shared.save(objects: chats)
                                
                                if completion != nil{
                                    completion!(.correct)
                                }

                            }
                            
                        }
                        else{
                            if completion != nil{
                                completion!(.error)
                            }
                        }
                    }
                })
                
                
            })
            
            
        })
 
    
    }
    
    public func updateOnlyChats(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = ChatRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DataBaseManager.shared.deleteObjects(type: ChatModel.self)
                    
                    var chats = [ChatModel]()
                    
                    for element in results{
                        let chat = ChatModel(info: element)
                        
                        chats.append(chat)
                        
                        self.updateMessages(chat: Chat(model: chat), completion: nil)
                    }
                    
                    DataBaseManager.shared.save(objects: chats)
                    
                    if completion != nil{
                        completion!(.correct)
                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
            
        })
 
    
    }
    
    public func create(name: String, participants: [String], creator: String, completion: ((Chat?, UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(nil, .missingUserToken)
                }
                
            }
            return
        }
                
        let request = ChatRequest()
        
        request.create(appToken: self.appToken, userToken: userToken, name: name, participants: participants, creator: creator, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        let chatModel = ChatModel(info: results)
                        
                        let chat = Chat(model: chatModel)
                        
                        if completion != nil{
                            completion!(chat, .correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(nil, .error)
                    }
                }
            }
            
        })
 
    
    }
    
    public func updateMessages(chat: Chat, completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = ChatRequest()
        
        request.downloadMessages(appToken: self.appToken, userToken: userToken, idChat: String(chat.id), completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                                                
                        var messages = [ChatMessageModel]()
                        
                        for element in results{
                            let message = ChatMessageModel(info: element)
                            
                            messages.append(message)
                        }
                        
                        DataBaseManager.shared.save(objects: messages)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
    public func getMessages(chat: Chat) -> [HermesObject] {
        
        let messages = DataBaseManager.shared.getObjects(type: ChatMessageModel.self).sorted{$0.fecha < $1.fecha}.filter{$0.idChat == chat.id}.map{ChatMessage(model: $0)}
        
        return messages
    }
    
    public func sendMessage(text: String, chat: Chat, idContentOwner: String, completion: ((ChatMessage?, RequestResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(nil, .error)
                }
                
            }
            return
        }
                
        let request = ChatRequest()
        
        
        request.sendMessage(appToken: self.appToken, userToken: userToken, idChat: String(chat.id), idContentOwner: idContentOwner, message: text, completion: {(result, requestResult) -> () in
            
            DispatchQueue.main.async {
                
                let messageModel = ChatMessageModel(info: result)
                let newMessage = ChatMessage(model: messageModel)
                
                if completion != nil{
                    
                    DispatchQueue.main.async {
                        completion!(newMessage, requestResult)
                    }
                    
                }
            }
            
        })
 
    
    }
    
    public func sendMessageWithImage(image: UIImage, chat: Chat, idContentOwner: String, completion: ((ChatMessage?, RequestResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(nil, .error)
                }
                
            }
            return
        }
                
        let request = ChatRequest()
        
        
        request.sendMessageWithImage(appToken: self.appToken, userToken: userToken, idChat: String(chat.id), idContentOwner: idContentOwner, image: image, completion: {(result, requestResult) -> () in
            
            DispatchQueue.main.async {
                
                let messageModel = ChatMessageModel(info: result)
                let newMessage = ChatMessage(model: messageModel)
                
                if completion != nil{
                    
                    DispatchQueue.main.async {
                        completion!(newMessage, requestResult)
                    }
                    
                }
            }
            
        })
 
    
    }
    
    public func updateDestinationContentOwners(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = ChatRequest()
        
        request.downloadContentOwners(appToken: self.appToken, entityToken: self.entityToken, maxResults: 100, firstResult: 0, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                                                
                        DataBaseManager.shared.deleteObjects(type: DestinationContentOwnerModel.self)
                        
                        var destinations = [DestinationContentOwnerModel]()
                        
                        for element in results{
                            let destination = DestinationContentOwnerModel(info: element)
                            
                            destinations.append(destination)
                            
                        }
                        
                        DataBaseManager.shared.save(objects: destinations)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
    public func getDestinationContentOwners() -> [DestinationContentOwner] {
        let chats = DataBaseManager.shared.getObjects(type: DestinationContentOwnerModel.self).map{DestinationContentOwner(model: $0)}
        
        return chats
    }
    
    public func updateDestinationGroups(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = ChatRequest()
        
        request.downloadGroups(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                                                
                        DataBaseManager.shared.deleteObjects(type: DestinationGroupModel.self)
                        
                        var destinations = [DestinationGroupModel]()
                        
                        for element in results{
                            let destination = DestinationGroupModel(info: element)
                            
                            destinations.append(destination)
                            
                        }
                        
                        DataBaseManager.shared.save(objects: destinations)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
    public func getDestinationGroups() -> [DestinationGroup] {
        let chats = DataBaseManager.shared.getObjects(type: DestinationGroupModel.self).map{DestinationGroup(model: $0)}
        
        return chats
    }
    
    public func updateDestinationOrgans(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = ChatRequest()
        
        request.downloadOrgans(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                                                
                        DataBaseManager.shared.deleteObjects(type: DestinationOrganModel.self)
                        
                        var destinations = [DestinationOrganModel]()
                        
                        for element in results{
                            let destination = DestinationOrganModel(info: element)
                            
                            destinations.append(destination)
                            
                        }
                        
                        DataBaseManager.shared.save(objects: destinations)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
    public func getDestinationOrgans() -> [DestinationOrgan] {
        let chats = DataBaseManager.shared.getObjects(type: DestinationOrganModel.self).map{DestinationOrgan(model: $0)}
        
        return chats
    }
    
    public func filter(text: String?, sender: String?, startDate: Date?, endDate: Date?) -> [Chat] {
        
        let chats = DataBaseManager.shared.getObjects(type: ChatModel.self).map{Chat(model: $0)}
            .filter{
                
                guard let message = $0.ultimoMensaje else {return false}
                
                var textResult = true
                var senderResult = true
                var startDateResult = true
                var endDateResult = true
                
                if text != nil{
                    if !message.texto.uppercased().contains(text!.uppercased()){
                        textResult = false
                    }
                }
                
                if sender != nil{
                    if !message.nombreEmisor.uppercased().contains(sender!.uppercased()){
                        senderResult = false
                    }
                }
                
                if startDate != nil{
                    
                    if let date = Formatter.stringToDate(string: message.fecha, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
                        if !(date >= startDate!){
                            startDateResult = false
                        }
                    }
                    else{
                        startDateResult = false
                    }
                    
                }
                
                if endDate != nil{
                    
                    if let date = Formatter.stringToDate(string: message.fecha, format: "yyyy-MM-dd'T'HH:mm:sszzzz"){
                        if !(date <= endDate!){
                            endDateResult = false
                        }
                    }
                    else{
                        endDateResult = false
                    }
                    
                }
                
                let result = textResult && senderResult && startDateResult && endDateResult
                
                return result
            }
            .sorted{
                guard let message1 = $0.ultimoMensaje else {return false}
                guard let message2 = $1.ultimoMensaje else {return false}
                
                return message1.fecha > message2.fecha
            }
        
        return chats
        
    }
    
}
