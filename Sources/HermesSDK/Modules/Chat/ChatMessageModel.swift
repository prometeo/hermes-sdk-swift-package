//
//  ChatMessageModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 02/12/2020.
//

import Foundation
import RealmSwift

class ChatMessageModel: Object {
    
    @objc dynamic var id : Int = 0
    @objc dynamic var idChat : Int = -1
    @objc dynamic var fecha : String  = ""
    @objc dynamic var emisor : String  = ""
    @objc dynamic var texto : String  = ""
    @objc dynamic var nombreEmisor : String  = ""
    @objc dynamic var leido : Bool = false
    @objc dynamic var fichero : String  = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.fecha = info["fecha"] as? String ?? ""
        self.emisor = info["emisor"] as? String ?? ""
        self.texto = info["texto"] as? String ?? ""
        self.nombreEmisor = info["_emisorName"] as? String ?? ""
        
        if let chat = info["chat"] as? [String : Any]{
            self.idChat = chat["id"] as? Int ?? -1
        }
        
        self.leido = true
        
        if let participante_has_mensajes = info["participante_has_mensajes"] as? [[String : Any]]{
            for partipante in participante_has_mensajes{
                let fecha_leido = partipante["fecha_leido"] as? String
                
                if fecha_leido == nil{
                    self.leido = false
                    break
                }
            }
        }
        
        self.fichero = info["fichero_name"] as? String ?? ""
        
        if self.fichero.hasPrefix("http") == false && self.fichero != ""{
            self.fichero = (BASE_URL + self.fichero).replacingOccurrences(of: "//", with: "/")
        }
        
    }
    
    
}
