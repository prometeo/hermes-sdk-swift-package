//
//  DestinationContentOwner.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 1/7/22.
//

import Foundation
import UIKit

public class DestinationContentOwner : HermesObject {
        
    internal init(model: DestinationContentOwnerModel) {
        self.id = "PropietarioContenido:" + String(model.id)
        self.nombre = model.nombre
        self.imagen = model.imagen
    }
    
    public var id : String
    public var nombre : String
    public var imagen : String

}
