//
//  ChatRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 02/12/2020.
//

import Foundation
import UIKit
import DataCompression

class ChatRequest{
    
    func download(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = CHATS_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
                
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            
            if result.contains("OBJECT_NOT_EXIST"){
                completion(results, .correct)
                
                return
            }
            
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            return
            

        })
        
    }
    
    func create(appToken: String, userToken: String, name: String, participants: [String], creator: String, completion: (@escaping([String : Any], RequestResult) -> ())){
        
        let url = CREATE_CHAT_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "titulo", value: name))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "creado_por", value: creator))
        
        for participant in participants{
            bodyCompontents.queryItems!.append(URLQueryItem(name: "participante[][participante]", value: participant))
        }
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [String : Any]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            return
            

        })
        
    }
    
    func downloadMessages(appToken: String, userToken: String, idChat: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = CHAT_MESSAGES_ENDPOINT + idChat
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            return
            

        })
        
    }
    
    func sendMessage(appToken: String, userToken: String, idChat: String, idContentOwner: String, message: String, completion: (@escaping([String : Any], RequestResult) -> ())){
        
        let url = SEND_CHAT_MESSAGE_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "texto", value: message))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "emisor", value: "PropietarioContenido:" + idContentOwner))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "chat[id]", value: idChat))

        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [String : Any]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            results = jsonData
   
            completion(results, .correct)
            
            return
            

        })
        
    }
    
    func sendMessageWithImage(appToken: String, userToken: String, idChat: String, idContentOwner: String, image: UIImage, completion: (@escaping([String : Any], RequestResult) -> ())){
        
        let url = SEND_CHAT_MESSAGE_ENDPOINT
        
        let request = Request(endpoint: url, method: "POST")
        
        var body = Data()
        
        let boundary = "------VohpleBoundary4sadfasdfeafe4w5w45435hwttre"
        let contentType = "multipart/form-data; boundary=" + boundary
        let separator = "--" + boundary + "\r\n"
        let newLine = "\r\n"
        
        let paramNameEmisor = "Content-Disposition: form-data; name=\"emisor\"\r\n\r\n"
        let paramNameChatId = "Content-Disposition: form-data; name=\"chat[id]\"\r\n\r\n"
        let paramNameFicheroName = "Content-Disposition: form-data; name=\"ficheroName\"\r\n\r\n"
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameEmisor.data(using: .utf8)!)
        body.append(("PropietarioContenido:" + idContentOwner).data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameChatId.data(using: .utf8)!)
        body.append(idChat.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        let imageName = Formatter.randomString()
        guard let imageData = image.compress() else {completion([String : Any](), .error); return}
        
        guard let compressedData = imageData.gzip() else {completion([String : Any](), .error); return}
        
        let base64 = imageName + ".jpg" + "|" + compressedData.base64EncodedString()
       
        body.append(separator.data(using: .utf8)!)
        body.append(paramNameFicheroName.data(using: .utf8)!)
        body.append(base64.data(using: .utf8)!)
        body.append(newLine.data(using: .utf8)!)
        
        /*let typeImage = "Content-Type:image/jpeg\r\n\r\n"
        
        guard let imageData = image.compress() else {completion([String : Any](), .error); return}
        
        let imageName = Formatter.randomString()
        
        body.append(separator.data(using: .utf8)!)
        
        let imageField = "Content-Disposition: form-data; name=\"ficheroFile\"; filename=\"" + imageName + ".jpg\"\r\n"
        
        body.append(imageField.data(using: .utf8)!)
        body.append(typeImage.data(using: .utf8)!)
        body.append(imageData)
        body.append(newLine.data(using: .utf8)!)*/
        
        let end = "--" + boundary + "--\r\n"
        
        body.append(end.data(using: .utf8)!)
        
        let headers : [String : String] = ["Content-Type" : contentType, "appToken" : appToken, "userToken" : userToken]
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [String : Any]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            results = jsonData
   
            completion(results, .correct)
            
            return
            

        })
        
    }
    
    private var totalResults = [[String : Any]]()
    
    func downloadContentOwners(appToken: String, entityToken: String, maxResults: Int, firstResult: Int, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        if firstResult == 0{
            self.totalResults = [[String : Any]]()
        }
        
        self.getPage(appToken: appToken, entityToken: entityToken, maxResults: maxResults, firstResult: firstResult, completion: {(elements) -> () in
            
            if elements.count == 0{
                completion(self.totalResults, .correct)
            }
            else{
                self.totalResults.append(contentsOf: elements)
                                
                self.downloadContentOwners(appToken: appToken, entityToken: entityToken, maxResults: maxResults, firstResult: firstResult + maxResults, completion: completion)
            }
            
        })
        
    }
    
    private func getPage(appToken: String, entityToken: String, maxResults: Int, firstResult: Int, completion: (@escaping([[String : Any]]) -> ())){
        
        var url = ALL_CONTENT_OWNERS_ENDPOINT + "?&$maxResults=" + String(maxResults) + "&$firstResult=" + String(firstResult)
        

        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "entidadToken" : entityToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results); return}
            guard responseCode == .success else {completion(results); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results); return}
            
            results = jsonData
            
            completion(results)
            
            
        })
        
    }
    
    
    func downloadGroups(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = CHAT_GROUPS_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
                
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            return
            

        })
        
    }
    
    func downloadOrgans(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = CHAT_ORGANS_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
                
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            return
            

        })
        
    }
    
    func downloadEntity(appToken: String, userToken: String, entityId: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = CHAT_ENTITY_ENDPOINT + entityId
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
                
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            
            return
            

        })
        
    }
}
