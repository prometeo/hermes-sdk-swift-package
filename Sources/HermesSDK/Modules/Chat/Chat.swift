//
//  Chat.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 02/12/2020.
//

import Foundation
import UIKit

public class Chat : HermesObject {
        
    internal init(model: ChatModel) {
        self.id = model.id
        self.titulo = model.titulo
        self.fecha = model.fecha
        self.banner = model.banner
        self.idCreador = model.idCreador
    }
    
    public var id : Int
    public var titulo : String
    public var fecha : String
    public var banner : String
    public var idCreador : String
    
    public var ultimoMensaje : ChatMessage? {
        get{
            guard let lastMessage = DataBaseManager.shared.getObjects(type: ChatMessageModel.self).filter({$0.idChat == self.id}).sorted(by: {$0.fecha > $1.fecha}).first else {return nil}
            
            return ChatMessage(model: lastMessage)
        }
    }
    
    public func buscarMensaje(id: Int) -> ChatMessage?{
        
        guard let message = DataBaseManager.shared.getObjects(type: ChatMessageModel.self).filter({$0.idChat == self.id && $0.id == id}).first else {return nil}
        
        return ChatMessage(model: message)
    }
    
    public func markMessagesAsRead() {
        let messages = DataBaseManager.shared.getObjects(type: ChatMessageModel.self).filter({$0.idChat == self.id})
        
        for message in messages{
            let messageId = "ChatMessage:" + String(message.id)
            UserDefaults.standard.set(true, forKey: messageId)
        }
    }
    
    public var hasNewMessages : Bool{
        get {
            
            let messages = DataBaseManager.shared.getObjects(type: ChatMessageModel.self).filter({$0.idChat == self.id}).map{ChatMessage(model: $0)}
            
            for message in messages {
                if message.isRead == false{
                    return true
                }
            }
            
            return false
        }
    }
    
    public func fecha(formato: String) -> String{
        
        var resultado = ""
        
        if let fechaDate = Formatter.stringToDate(string: self.fecha, format: "yyyy-MM-dd'T'HH:mm:ssXXXXX"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
        
        return resultado
    }
    
    public var creador : String {
        
        if let bannerJson = try? JSONSerialization.jsonObject(with: (self.banner.data(using: .utf8))!, options:[]) as? [String:Any]{
            
            return bannerJson["creadoPor"] as? String ?? ""
            
        }
        
        return ""
    }
    
    public var imagenCreador : String {
        
        var result = ""
        
        if self.idCreador.contains("PropietarioContenido"){
            if let contentOwner = DataBaseManager.shared.getObjects(type: DestinationContentOwnerModel.self).filter({self.idCreador.contains(String($0.id))}).first{
                
                result = contentOwner.imagen
            }
        }
        
        return result
    }
}
