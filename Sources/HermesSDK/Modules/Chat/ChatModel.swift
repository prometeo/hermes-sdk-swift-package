//
//  ChatModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 02/12/2020.
//

import Foundation
import RealmSwift

class ChatModel: Object {
    
    @objc dynamic var id : Int = 0
    @objc dynamic var titulo : String = ""
    @objc dynamic var fecha : String = ""
    @objc dynamic var banner : String = ""
    @objc dynamic var idCreador : String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.titulo = info["titulo"] as? String ?? ""
        self.fecha = info["created_at"] as? String ?? ""
        self.banner = info["_banner"] as? String ?? ""
        self.idCreador = info["creado_por"] as? String ?? ""
    }
    
    
}
