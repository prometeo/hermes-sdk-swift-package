//
//  DestinationGroup.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 1/7/22.
//

import Foundation
import UIKit

public class DestinationGroup : HermesObject {
        
    internal init(model: DestinationGroupModel) {
        self.id = "GrupoPacientes:" + String(model.id)
        self.nombre = model.nombre
    }
    
    public var id : String
    public var nombre : String

}
