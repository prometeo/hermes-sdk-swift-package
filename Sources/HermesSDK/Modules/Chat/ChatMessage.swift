//
//  ChatMessage.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 02/12/2020.
//

import Foundation
import UIKit

public class ChatMessage : HermesObject {
        
    internal init(model: ChatMessageModel) {
        self.id = model.id
        self.idChat = model.idChat
        self.fecha = model.fecha
        self.emisor = model.emisor
        self.nombreEmisor = model.nombreEmisor
        self.texto = model.texto
        self.leido = model.leido
        self.fichero = model.fichero
    }
    
    public var id : Int
    public var idChat : Int
    public var fecha : String
    public var emisor : String
    public var nombreEmisor : String
    public var texto : String
    public var leido : Bool
    public var fichero : String
    
    public func fecha(formato: String) -> String{
        
        var resultado = ""
        
        if let fechaDate = Formatter.stringToDate(string: self.fecha, format: "yyyy-MM-dd'T'HH:mm:ssXXXXX"){
            resultado = Formatter.dateToString(date: fechaDate, format: formato)
        }
        
        return resultado
    }
    
    public var isRead : Bool {
        get{
            let messageId = "ChatMessage:" + String(self.id)
            
            let read = UserDefaults.standard.bool(forKey: messageId)
            
            return read
        }
    }
    
    public var imagenEmisor : String {
        
        var result = ""
        
        if self.emisor.contains("PropietarioContenido"){
            if let contentOwner = DataBaseManager.shared.getObjects(type: DestinationContentOwnerModel.self).filter({self.emisor.contains(String($0.id))}).first{
                
                result = contentOwner.imagen
            }
        }
        
        return result
    }
    
}
