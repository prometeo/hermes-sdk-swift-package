//
//  DestinationContentOwnerModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 1/7/22.
//

import Foundation
import RealmSwift

class DestinationContentOwnerModel: Object {
    
    @objc dynamic var id : Int = 0
    @objc dynamic var nombre : String = ""
    @objc dynamic var imagen : String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.nombre = info["nombre"] as? String ?? ""
    
        if let apellidos = info["apellidos"] as? String{
            self.nombre += " "
            self.nombre += apellidos
        }
        
        self.imagen = info["image_route"] as? String ?? ""
    }
    
    
}
