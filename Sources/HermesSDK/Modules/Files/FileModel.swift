//
//  FileModel.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 09/11/2020.
//

import Foundation
import RealmSwift

class FileModel: Object {
    
    @objc dynamic var id : Int  = 0
    @objc dynamic var nombre : String = ""
    @objc dynamic var ruta : String = ""
    @objc dynamic var esDirectorio : Bool = false
    @objc dynamic var nombrePadre : String = ""
    @objc dynamic var idPadre : Int = 0

    override static func primaryKey() -> String? {
        return "id"
    }
    
    override required init() {
        
    }
    
    init(info: [String : Any]) {
        
        self.id = info["id"] as? Int ?? 0
        self.nombre = info["nombre"] as? String ?? ""
        self.ruta = info["ruta"] as? String ?? ""
        self.esDirectorio = info["is_dir"] as? Bool ?? false
        self.nombrePadre = info["nombre_padre"] as? String ?? ""
        self.idPadre = info["id_padre"] as? Int ?? 0
 
    }
    
    
}
