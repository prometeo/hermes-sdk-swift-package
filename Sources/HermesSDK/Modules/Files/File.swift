//
//  File.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 09/11/2020.
//

import Foundation
import UIKit

public class File : HermesObject {
        
    internal init(model: FileModel) {
        self.id = model.id
        
        let nombreTroceado = model.nombre.components(separatedBy: "/")
        
        self.nombre = nombreTroceado.last ?? model.nombre
        
        self.nombreCompleto = model.nombre
        
        self.ruta = model.ruta
        self.esDirectorio = model.esDirectorio
        self.nombrePadre = model.nombrePadre
        self.idPadre = model.idPadre
    }
    
    public var id : Int
    public var nombre : String
    public var ruta : String
    public var esDirectorio : Bool
    public var nombrePadre : String
    public var idPadre : Int
    public var nombreCompleto : String
    
    public func download(completion: (@escaping(String?, Data?, DownloadResult) -> ())){
        
        let hermesFiles = HermesFiles()
        
        hermesFiles.download(fileName: self.nombreCompleto, completion: {(mime, result, resultCode) -> () in
            
            DispatchQueue.main.async {
                completion(mime, result, resultCode)
            }
        })
        
    }
}
