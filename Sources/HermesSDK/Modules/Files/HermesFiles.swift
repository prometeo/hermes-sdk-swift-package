//
//  HermesFiles.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 09/11/2020.
//

import Foundation

public class HermesFiles: HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func getFilesOf(parent: Int) -> [File]{
        let files = DataBaseManager.shared.getObjects(type: FileModel.self).filter{$0.idPadre == parent}.map{File(model: $0)}
        
        return files
    }
    
    public func get() -> [HermesObject] {
        let files = DataBaseManager.shared.getObjects(type: FileModel.self).map{File(model: $0)}
        
        return files
    }
    
    public func update(completion: ((UpdateResult) -> ())?){
        
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = FilesRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        DataBaseManager.shared.deleteObjects(type: FileModel.self)
                        
                        var files = [FileModel]()
                        
                        for element in results{
                            let file = FileModel(info: element)
                            
                            files.append(file)
                        }
                        
                        DataBaseManager.shared.save(objects: files)
                        
                        if completion != nil{
                            completion!(.correct)
                        }

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
 
    
    }
    
    func download(fileName: String, completion: (@escaping(String?, Data?, DownloadResult) -> ())){
        
        
        guard let userToken = self.userToken else {
            completion(nil, nil, .missingUserToken)
            return
        }
                
        let request = FilesRequest()
        
        request.get(fileName: fileName, appToken: self.appToken, userToken: userToken, completion: {(mime, result, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        completion(mime, result, .correct)

                    }
                    
                }
                else{
                    completion(nil, nil, .error)
                }
            }
            
        })
 
    
    }
}
