//
//  ContentOwnersRequest.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 06/11/2020.
//

import Foundation

class ContentOwnersRequest{
    
    func download(appToken: String, userToken: String, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = CONTENT_OWNERS_ENDPOINT
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded"]
        
        let request = Request(endpoint: url, method: "POST")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "userToken", value: userToken))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            guard let str = jsonData["str"] as? String else {completion(results, .error); return}
            
            if str == "OK"{
                
                guard let info = jsonData["info"] as? [[String : Any]] else {completion(results, .error); return}
                
                results = info
                
                completion(results, .correct)
                return
            }
            else{
                completion(results, .error)
                return
            }
            

        })
        
    }
    
    func associate(userToken: String, contentOwnerToken: String, entityId: String, completion: (@escaping(AssociateResult) -> ())){
        
        let url = ASSOCIATE_CONTENT_OWNER_ENDPOINT.replacingOccurrences(of: "$0", with: contentOwnerToken)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded"]
        
        let request = Request(endpoint: url, method: "POST")
        
  
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "userToken", value: userToken))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "idEntidad", value: entityId))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.incorrect); return}
            guard responseCode == .success else {completion(.incorrect); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(.incorrect); return}
            
            guard let str = jsonData["str"] as? String else {completion(.incorrect); return}
            
            if str == "OK"{
                
                completion(.correct)
                return
            }
            else if str == "PASSWORD_REQUESTED"{
                
                completion(.needsValidation)
                return
            }
            else{
                completion(.incorrect)
                return
            }
            

        })
        
    }
    
    func validate(userToken: String, contentOwnerToken: String, entityId: String, password: String, completion: (@escaping(RequestResult) -> ())){
        
        let url = VALIDATE_CONTENT_OWNER_ENDPOINT.replacingOccurrences(of: "$0", with: contentOwnerToken)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded"]
        
        let request = Request(endpoint: url, method: "POST")
        
  
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "userToken", value: userToken))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "idEntidad", value: entityId))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "password", value: password))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(.error); return}
            
            guard let str = jsonData["str"] as? String else {completion(.error); return}
            
            if str == "OK"{
                
                completion(.correct)
                return
            }
            else{
                completion(.error)
                return
            }
            

        })
        
    }
    
    func deassociate(userToken: String, contentOwnerToken: String, completion: (@escaping(RequestResult) -> ())){
        
        let url = DEASSOCIATE_CONTENT_OWNER_ENDPOINT.replacingOccurrences(of: "$0", with: contentOwnerToken)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded"]
        
        let request = Request(endpoint: url, method: "POST")
        
  
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "userToken", value: userToken))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(.error); return}
            
            guard let str = jsonData["str"] as? String else {completion(.error); return}
            
            if str == "OK"{
                
                completion(.correct)
                return
            }
            else{
                completion(.error)
                return
            }
            

        })
        
    }
    
    func askSMSCode(userToken: String, appToken: String, telephone: String, completion: (@escaping(RequestResult) -> ())){
        
        let url = ASK_SMS_CODE_ENDPOINT.replacingOccurrences(of: "$0", with: telephone)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "userToken" : userToken, "appToken" : appToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(.error); return}
            
            guard let str = jsonData["str"] as? String else {completion(.error); return}
            
            if str == "OK"{
                
                completion(.correct)
                return
            }
            else{
                completion(.error)
                return
            }
            

        })
        
    }
    
    func createContentOwner(userToken: String, appToken: String, dni: String, nombre: String, apellidos: String, idExterno: String, rol: String?, completion: (@escaping([String : Any]?, RequestResult) -> ())){
        
        let url = CREATE_CONTENT_OWNER
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "userToken" : userToken, "appToken" : appToken]
        
        let nombreCompleto = nombre + " " + apellidos
        
        let alias = nombreCompleto.getAcronyms()
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "alias", value: alias))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "nombre", value: nombre))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "apellidos", value: apellidos))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "dni", value: dni))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "identificador_externo", value: idExterno))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "recibir_notificaciones_push", value: String(true)))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "activo", value: String(true)))
        
        if rol != nil{
            bodyCompontents.queryItems!.append(URLQueryItem(name: "roles[0]", value: rol!))
        }
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        let request = Request(endpoint: url, method: "POST")
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results : [String : Any]? = nil
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            

        })
        
    }
    
    func searchContentOwnerByDni(userToken: String, appToken: String, dni: String, completion: (@escaping([String : Any]?, RequestResult) -> ())){
        
        let url = SEARCH_CONTENT_OWNER_BY_DNI + dni
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "userToken" : userToken, "appToken" : appToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results : [String : Any]? = nil
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            guard let contentOwnerJson = jsonData.first else {completion(results, .error); return}
            
            results = contentOwnerJson
            
            completion(results, .correct)
            

        })
        
    }
    
    func getContentOwner(appToken: String, userToken: String, id: Int, completion: (@escaping([[String : Any]], RequestResult) -> ())){
        
        let url = GET_CONTENT_OWNER_ENDPOINT + String(id)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [[String : Any]]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            

        })
        
    }
    
    func requestTokenByNif(entityToken: String, appToken: String, nif: String, completion: (@escaping(RequestResult) -> ())){
        
        let url = REQUEST_TOKEN_BY_NIF + nif
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "entidadToken" : entityToken, "appToken" : appToken]
        
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            
            guard error == nil else {completion(.error); return}
            guard responseCode == .success else {completion(.error); return}
            
            completion(.correct)
            

        })
        
    }
    
    func saveCV(appToken: String, userToken: String, idContentOwner: Int, cv: String, completion: (@escaping([String : Any], RequestResult) -> ())){
        
        let url = SAVE_CV_ENDPOINT + String(idContentOwner)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "PATCH")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "json_empleo_curriculum", value: cv))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [String : Any]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            

        })
        
    }
    
    func saveExtraData(appToken: String, userToken: String, idContentOwner: Int, jsonEmpleoEmpresa: String, jsonEmpleoDemandante: String, completion: (@escaping([String : Any], RequestResult) -> ())){
        
        let url = SAVE_CONTENT_OWNER_EXTRA_DATA_ENDPOINT + String(idContentOwner)
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "appToken" : appToken, "userToken" : userToken]
        
        let request = Request(endpoint: url, method: "PATCH")
        
        var bodyCompontents = URLComponents()
        bodyCompontents.queryItems = [URLQueryItem]()
        bodyCompontents.queryItems!.append(URLQueryItem(name: "json_empleo_empresa", value: jsonEmpleoEmpresa))
        bodyCompontents.queryItems!.append(URLQueryItem(name: "json_empleo_demandante", value: jsonEmpleoDemandante))
        
        let body = bodyCompontents.query?.data(using: .utf8)
        
        request.send(headers: headers, body: body, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results = [String : Any]()
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [String:Any] else {completion(results, .error); return}
            
            results = jsonData
            
            completion(results, .correct)
            

        })
        
    }
    
    func searchContentOwnerByToken(userToken: String, appToken: String, token: String, completion: (@escaping([String : Any]?, RequestResult) -> ())){
        
        let url = SEARCH_CONTENT_OWNER_BY_TOKEN + token
        
        let headers : [String : String] = ["Content-Type" : "application/x-www-form-urlencoded", "userToken" : userToken, "appToken" : appToken]
        
        let request = Request(endpoint: url, method: "GET")
        
        request.send(headers: headers, body: nil, completion: {(result, statusCode, error) -> () in
            
            let responseCode = ResponseCode(rawValue: statusCode) ?? .unknown
            var results : [String : Any]? = nil
            
            guard error == nil else {completion(results, .error); return}
            guard responseCode == .success else {completion(results, .error); return}
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: (result.data(using: .utf8))!, options:[]) as? [[String:Any]] else {completion(results, .error); return}
            
            guard let contentOwnerJson = jsonData.first else {completion(results, .error); return}
            
            results = contentOwnerJson
            
            completion(results, .correct)
            

        })
        
    }
}
