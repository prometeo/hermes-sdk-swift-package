//
//  HermesContentOwners.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 06/11/2020.
//

import Foundation

public class HermesContentOwners : HermesModule {
    
    private var userToken : String?
    private var appToken : String!
    private var entityId : String!
    private var entityToken : String!
    
    public init() {
        self.userToken = nil

        self.appToken = APP_TOKEN
        self.entityId = ENTITY_ID
        self.entityToken = ENTITY_TOKEN
        
        let users = DataBaseManager.shared.getObjects(type: UserModel.self)
        
        if let user = users.first {
            self.userToken = user.userToken
        }
        
    }
    
    public func get() -> [HermesObject] {
        
        let contentOwners = DataBaseManager.shared.getObjects(type: ContentOwnerModel.self).map{ContentOwner(model: $0)}
        
        return contentOwners
    }
    
    private var pendingContentOwners = 0
    
    public func update(completion: ((UpdateResult) -> ())?) {
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.missingUserToken)
                }
                
            }
            return
        }
                
        let request = ContentOwnersRequest()
        
        request.download(appToken: self.appToken, userToken: userToken, completion: {(results, requestResult) -> () in
            
            DispatchQueue.main.async {
                if requestResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        self.pendingContentOwners = results.count
                        
                        DataBaseManager.shared.deleteObjects(type: ContentOwnerModel.self)
                                                
                        for element in results{
                            
                            if let id = element["id"] as? Int{
                                request.getContentOwner(appToken: self.appToken, userToken: userToken, id: id, completion: {(contentOwners, getResult) -> () in
                                    
                                    DispatchQueue.main.async {
                                        if let info = contentOwners.first{
                                            let contentOwner = ContentOwnerModel(info: info)
                                            
                                            DataBaseManager.shared.save(object: contentOwner)
                                        }
                                        
                                        self.pendingContentOwners -= 1
                                        
                                        if self.pendingContentOwners == 0{
                                            if completion != nil{
                                                completion!(.correct)
                                            }
                                        }
                                    }
                                    
                                })
                            }
                            else{
                                if self.pendingContentOwners == 0{
                                    if completion != nil{
                                        completion!(.correct)
                                    }
                                }
                            }
                            
                        }
                        
                        if self.pendingContentOwners == 0{
                            if completion != nil{
                                completion!(.correct)
                            }
                        }
                        

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
            
        })
    }
    
    public func asociate(code: String, completion: ((AssociateResult) -> ())?) {
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.incorrect)
                }
                
            }
            return
        }
                
        let request = ContentOwnersRequest()
        
        request.associate(userToken: userToken, contentOwnerToken: code, entityId: self.entityId, completion: {(associateResult) -> () in
            
            DispatchQueue.main.async {
                if associateResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        let contentOwnersModule = HermesContentOwners()
                        
                        contentOwnersModule.update(completion: {(updateResult) -> () in
                            DispatchQueue.main.async {
                                if completion != nil{
                                    completion!(.correct)
                                }
                            }
                        })
                        
                        

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(associateResult)
                    }
                }
            }
            
        })
    }
    
    public func deasociate(code: String, completion: ((AssociateResult) -> ())?) {
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.incorrect)
                }
                
            }
            return
        }
                
        let request = ContentOwnersRequest()
        
        request.deassociate(userToken: userToken, contentOwnerToken: code, completion: {(associateResult) -> () in
            
            DispatchQueue.main.async {
                if associateResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        let contentOwnersModule = HermesContentOwners()
                        
                        contentOwnersModule.update(completion: {(updateResult) -> () in
                            DispatchQueue.main.async {
                                if completion != nil{
                                    completion!(.correct)
                                }
                            }
                        })
                        
                        

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.incorrect)
                    }
                }
            }
            
        })
    }
    
    public func validate(code: String, password: String, completion: ((AssociateResult) -> ())?) {
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.incorrect)
                }
                
            }
            return
        }
                
        let request = ContentOwnersRequest()
        
        request.validate(userToken: userToken, contentOwnerToken: code, entityId: self.entityId, password: password, completion: {(associateResult) -> () in
            
            DispatchQueue.main.async {
                if associateResult == .correct{
                    
                    DispatchQueue.main.async {
                        
                        let contentOwnersModule = HermesContentOwners()
                        
                        contentOwnersModule.update(completion: {(updateResult) -> () in
                            DispatchQueue.main.async {
                                if completion != nil{
                                    completion!(.correct)
                                }
                            }
                        })
                        
                        

                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.incorrect)
                    }
                }
            }
            
        })
    }
    
    public func askSMSCode(telephone: String, completion: ((AssociateResult) -> ())?) {
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.incorrect)
                }
                
            }
            return
        }
                
        let request = ContentOwnersRequest()
        
        request.askSMSCode(userToken: userToken, appToken: self.appToken, telephone: telephone, completion: {(associateResult) -> () in
            
            DispatchQueue.main.async {
                if associateResult == .correct{
                    
                    if completion != nil{
                        completion!(.correct)
                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.incorrect)
                    }
                }
            }
            
        })
    }
    
    public func createContentOwner(dni: String, nombre: String, apellidos: String, idExterno: String, rol: String? = nil, completion: ((ContentOwner?, RequestResult) -> ())?) {
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(nil, .error)
                }
                
            }
            return
        }
                
        let request = ContentOwnersRequest()
        
        request.createContentOwner(userToken: userToken, appToken: self.appToken, dni: dni, nombre: nombre, apellidos: apellidos, idExterno: idExterno, rol: rol, completion: {(contentOwnerInfo, requestResult) in
            
            DispatchQueue.main.async {
                if requestResult == .correct && contentOwnerInfo != nil{
                    
                    let contentOwnerModel = ContentOwnerModel(info: contentOwnerInfo!)
                    let contentOwner = ContentOwner(model: contentOwnerModel)
                    
                    if completion != nil{
                        completion!(contentOwner, .correct)
                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(nil, .error)
                    }
                }
            }
            
        })
    }
    
    public func searchContentOwnerByDni(dni: String, completion: ((ContentOwner?, RequestResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(nil, .error)
                }
                
            }
            return
        }
                
        let request = ContentOwnersRequest()
        
        request.searchContentOwnerByDni(userToken: userToken, appToken: self.appToken, dni: dni, completion: {(contentOwnerInfo, requestResult) in
            
            DispatchQueue.main.async {
                if requestResult == .correct && contentOwnerInfo != nil{
                    
                    let contentOwnerModel = ContentOwnerModel(info: contentOwnerInfo!)
                    let contentOwner = ContentOwner(model: contentOwnerModel)
                    
                    if completion != nil{
                        completion!(contentOwner, .correct)
                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(nil, .error)
                    }
                }
            }
        })
    }
    
    public func requestTokenByNif(nif: String, completion: ((RequestResult) -> ())?){
        
        let request = ContentOwnersRequest()
        
        request.requestTokenByNif(entityToken: self.entityToken, appToken: self.appToken, nif: nif, completion: {(requestResult) in
            
            DispatchQueue.main.async {
                if requestResult == .correct {
                    
                    if completion != nil{
                        completion!(.correct)
                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
        })
    }
    
    public func getContentOwnerById(id: Int, completion: ((ContentOwner?, RequestResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(nil, .error)
                }
                
            }
            return
        }
                
        let request = ContentOwnersRequest()
        request.getContentOwner(appToken: self.appToken, userToken: userToken, id: id, completion: {(contentOwnerInfo, requestResult) in
            
            DispatchQueue.main.async {
                if requestResult == .correct {
                    
                    if let info = contentOwnerInfo.first{
                        let contentOwnerModel = ContentOwnerModel(info: info)
                        let contentOwner = ContentOwner(model: contentOwnerModel)
                        
                        if completion != nil{
                            completion!(contentOwner, .correct)
                        }
                    }
                    else{
                        if completion != nil{
                            completion!(nil, .error)
                        }
                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(nil, .error)
                    }
                }
            }
        })
    }
    
    public func saveCV(id: Int, cv: String, completion: ((RequestResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.error)
                }
                
            }
            return
        }
                
        
        let request = ContentOwnersRequest()
        request.saveCV(appToken: self.appToken, userToken: userToken, idContentOwner: id, cv: cv, completion: {(contentOwnerInfo, requestResult) in
            
            DispatchQueue.main.async {
                if requestResult == .correct {
                    if completion != nil{
                        completion!(.correct)
                    }
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
        })
    }
    
    public func saveExtraData(id: Int, jsonEmpleoEmpresa: String, jsonEmpleoDemandante: String, completion: ((RequestResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(.error)
                }
                
            }
            return
        }
                
        
        let request = ContentOwnersRequest()
        request.saveExtraData(appToken: self.appToken, userToken: userToken, idContentOwner: id, jsonEmpleoEmpresa: jsonEmpleoEmpresa, jsonEmpleoDemandante: jsonEmpleoDemandante, completion: {(contentOwnerInfo, requestResult) in
            
            DispatchQueue.main.async {
                if requestResult == .correct {
                    if completion != nil{
                        completion!(.correct)
                    }
                }
                else{
                    if completion != nil{
                        completion!(.error)
                    }
                }
            }
        })
    }
    
    public func searchContentOwnerByToken(token: String, completion: ((ContentOwner?, RequestResult) -> ())?){
        
        guard let userToken = self.userToken else {
            if completion != nil{
                
                DispatchQueue.main.async {
                    completion!(nil, .error)
                }
                
            }
            return
        }
                
        let request = ContentOwnersRequest()
        
        request.searchContentOwnerByToken(userToken: userToken, appToken: self.appToken, token: token, completion: {(contentOwnerInfo, requestResult) in
            
            DispatchQueue.main.async {
                if requestResult == .correct && contentOwnerInfo != nil{
                    
                    let contentOwnerModel = ContentOwnerModel(info: contentOwnerInfo!)
                    let contentOwner = ContentOwner(model: contentOwnerModel)
                    
                    if completion != nil{
                        completion!(contentOwner, .correct)
                    }
                    
                }
                else{
                    if completion != nil{
                        completion!(nil, .error)
                    }
                }
            }
        })
    }
}
