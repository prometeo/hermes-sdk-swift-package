//
//  DataBaseManager.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 30/10/2020.
//

import Foundation
import RealmSwift

internal class DataBaseManager {
    
    static let shared = DataBaseManager()
    
    let realm : Realm!
    
    init() {
        
        Realm.Configuration.defaultConfiguration = Realm.Configuration(
            schemaVersion: 39,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 39) {
                    migration.enumerateObjects(ofType: EventModel.className()) { oldObject, newObject in
                        
                        newObject!["descripcionCorta"] = ""
                        newObject!["idFormularioInscripcion"] = -1
                        newObject!["completo"] = false
                        newObject!["publico"] = false
                        newObject!["inscrito"] = false
                        newObject!["local"] = false
                    }
                    
                    migration.enumerateObjects(ofType: IncidenceModel.className()) { oldObject, newObject in
                        
                        newObject!["obervaciones"] = ""

                    }
                    
                    migration.enumerateObjects(ofType: SentFormModel.className()) { oldObject, newObject in
                        
                        newObject!["observaciones"] = ""
                        newObject!["comentario1"] = ""
                        newObject!["comentario2"] = ""
                        newObject!["comentario3"] = ""

                    }
                    
                    migration.enumerateObjects(ofType: ReceivedFormModel.className()) { oldObject, newObject in
                        
                        newObject!["observaciones"] = ""
                        newObject!["comentario1"] = ""
                        newObject!["comentario2"] = ""
                        newObject!["comentario3"] = ""

                    }
                    
                    migration.enumerateObjects(ofType: DirectoryItemModel.className()) { oldObject, newObject in
                        
                        newObject!["orden"] = 0

                    }
                    
                    migration.enumerateObjects(ofType: ContentOwnerModel.className()) { oldObject, newObject in
                        
                        newObject!["dni"] = ""
                        newObject!["direccion"] = ""
                        newObject!["idExterno"] = ""
                        newObject!["email"] = ""
                        newObject!["otrosDatos"] = ""
                        newObject!["roles"] = ""
                        newObject!["jsonEmpleoEmpresa"] = ""
                        newObject!["jsonEmpleoDemandante"] = ""
                        newObject!["jsonEmpleoCurriculum"] = ""
                    }
                    
                    migration.enumerateObjects(ofType: CMSItemModel.className()) { oldObject, newObject in
                        
                        newObject!["slug"] = ""

                    }
                    
                    migration.enumerateObjects(ofType: MenuItemModel.className()) { oldObject, newObject in
                        
                        newObject!["tipos"] = ""

                    }
                    
                    migration.enumerateObjects(ofType: MenuItemModel.className()) { oldObject, newObject in
                        
                        newObject!["id"] = Int.random(in: 1..<1000000) * -1

                    }
                    
                    migration.enumerateObjects(ofType: ResourceModel.className()) { oldObject, newObject in
                        
                        newObject!["tipoReserva"] = 1
                        newObject!["categorias"] = [ResourceCategory]()

                    }
                    
                    migration.enumerateObjects(ofType: ChatMessageModel.className()) { oldObject, newObject in
                        
                        newObject!["nombreEmisor"] = ""
                        newObject!["leido"] = false
                        newObject!["fichero"] = ""
                    }
                    
                    migration.enumerateObjects(ofType: MenuBadgesModel.className()) { oldObject, newObject in
                        
                        newObject!["chats"] = 0
                    }
                    
                    migration.enumerateObjects(ofType: ReceivedFormModel.className()) { oldObject, newObject in
                        
                        newObject!["fechaEstado1"] = ""
                        newObject!["fechaEstado2"] = ""
                        newObject!["fechaEstado3"] = ""
                    }
                    
                    migration.enumerateObjects(ofType: SentFormModel.className()) { oldObject, newObject in
                        
                        newObject!["fechaEstado1"] = ""
                        newObject!["fechaEstado2"] = ""
                        newObject!["fechaEstado3"] = ""
                    }
                    
                    migration.enumerateObjects(ofType: OrganizationItemModel.className()) { oldObject, newObject in
                        
                        newObject!["miembros"] = List<OrganizationMemberModel>()

                    }
                    
                    migration.enumerateObjects(ofType: OrganizationMemberModel.className()) { oldObject, newObject in
                        
                        newObject!["id"] = Int.random(in: -10000 ..< -1)

                    }
                    
                    migration.enumerateObjects(ofType: ChatModel.className()) { oldObject, newObject in
                        
                        newObject!["fecha"] = ""
                        newObject!["banner"] = ""
                        newObject!["idCreador"] = ""

                    }
                    
                    migration.enumerateObjects(ofType: DestinationContentOwnerModel.className()) { oldObject, newObject in
                        
                        newObject!["imagen"] = ""

                    }
                    
                    migration.enumerateObjects(ofType: ExpenseModel.className()) { oldObject, newObject in
                        
                        newObject!["proyecto"] = ""
                        newObject!["destino"] = ""
                        newObject!["motivo"] = ""
                        newObject!["proyectoId"] = -1
                        newObject!["items"] = [ExpenseItemModel]()
                    }
                    
                    migration.enumerateObjects(ofType: FormModel.className()) { oldObject, newObject in
                        
                        newObject!["fechaAlta"] = ""
                        newObject!["fechaBaja"] = ""
                        newObject!["categorias"] = [Int]()
                    }
                    
                    migration.enumerateObjects(ofType: UserModel.className()) { oldObject, newObject in
                        
                        newObject!["id"] = -1

                    }
                    
                    migration.enumerateObjects(ofType: OfferModel.className()) { oldObject, newObject in
                        
                        newObject!["idPropietarioContenido"] = -1

                    }
                    
                    migration.enumerateObjects(ofType: VacationDayModel.className()) { oldObject, newObject in
                        
                        newObject!["idPropietarioContenido"] = -1

                    }
                    
                    migration.enumerateObjects(ofType: NewsItemModel.className()) { oldObject, newObject in
                        
                        newObject!["activa"] = true

                    }
                    
                    migration.enumerateObjects(ofType: MessageModel.className()) { oldObject, newObject in
                        
                        newObject!["adjuntos"] = [String]()

                    }
                    
                }
            })
        
        self.realm = try! Realm()
        
    }
    
    
    func save<T: Object>(object: T){
        
        try! realm.write{
            self.realm.add(object, update: .all)
        }

    }
    
    func save<T: Object>(objects: [T]){
        try! realm.write{
            self.realm.add(objects, update: .all)
        }
    }

    func getObjects<T: Object>(type: T.Type) -> [T]{
        
        return Array(self.realm.objects(type))
    }
    
    func delete<T: Object>(object: T){

        try! realm.write{
            self.realm.delete(object)
        }

    }
    
    func deleteObjects<T: Object>(type: T.Type){
        
        let objects = self.getObjects(type: type)
        
        try! realm.write{
            self.realm.delete(objects)
        }

    }
    
    func startUpdate(){
        self.realm.beginWrite()
        
    }
    
    func endUpdate(){
        try! self.realm.commitWrite()
    }
    
    func update<T: Object>(object: T){
        try! realm.write{
            self.realm.add(object, update: .modified)
        }
    }
    
    func numberOfObjects<T: Object>(type: T.Type) -> Int{
        
        return Array(self.realm.objects(type)).count
    }
    
    func filter<T: Object>(type: T.Type, identifiers: List<Int>) -> [T]{
        
        var predicate = "id IN {"
        
        predicate += identifiers.map{String($0)}.joined(separator: ", ")
        predicate += "}"
        
        return Array(self.realm.objects(type).filter(predicate))
    }
    
    func filter<T: Object>(type: T.Type, field: String, value: String) -> [T]{
        
        let predicate = field + " = " + value
        
        return Array(self.realm.objects(type).filter(predicate))
    }
}
