//
//  HermesModuleProtocol.swift
//  HermesSDK
//
//  Created by Juan Luis Jimenez Garcia on 05/11/2020.
//

import Foundation

public protocol HermesModule {
    
    func get() -> [HermesObject]
    func update(completion: ((UpdateResult) -> ())?)
}
