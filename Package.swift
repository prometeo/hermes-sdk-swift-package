// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "HermesSDK",
    platforms: [
        .iOS(.v12)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "HermesSDK",
            targets: ["HermesSDK"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(name: "Realm", url: "https://github.com/realm/realm-cocoa.git", "10.19.0"..<"10.19.0"),
        .package(name: "DataCompression", url: "https://github.com/mw99/DataCompression.git", "3.8.0"..<"3.8.0")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "HermesSDK",
            dependencies: [
                .product(name: "RealmSwift", package: "Realm"),
                .product(name: "DataCompression", package: "DataCompression")]),
    ]
)
